package entities;

import entities.IMurderable.IExitable;
import utils.Sounds;
import flixel.util.FlxTimer;

import flixel.FlxG;
import flixel.FlxSprite;
import flixel.group.FlxGroup;

/**
 * ...
 * @author TJ
 */
class Beam extends FlxSprite
{
	inline static var MINSPEED = 25;
	inline static var MAXSPEED = 50;
	
	inline static var WARNING_TIME = 1.5;
	inline static var SCALE = 1;//Work in Progress

	var topEmitter:BeamEmitter;
	var bottomEmitter:BeamEmitter;
	
	@:keep
	public function new()
	{
		super();
		
		loadGraphic("assets/images/enemies/beam.png", true, 8, 8);
		
		animation.add("intro", [0, 1], 15);
		animation.add("idle", [2, 3], 15);
		
		origin.set(0, 0);
		offset.set(0, -frameWidth * SCALE);
	}
	
	public function init(emitters: FlxTypedGroup<BeamEmitter>):Void
	{
		Sounds.play(BEAM);
		animation.play("intro");
		solid = false;
		moves = false;
		angle = 0;
		scale.set(SCALE, SCALE);
		width = frameWidth * SCALE;
		height = frameHeight * SCALE;

		topEmitter = emitters.recycle(BeamEmitter);
		bottomEmitter = emitters.recycle(BeamEmitter);

		switch (FlxG.random.int(0, 3))
		{
			case 0: spawnLeftToRight();
			case 1: spawnRightToLeft();
			case 2: spawnTopToBottom();
			case 3: spawnBottomToTop();
		}
		
		new FlxTimer().start(WARNING_TIME).onComplete = function (_) {
			animation.finishCallback = null;
			topEmitter.moves = true;
			bottomEmitter.moves = true;
			topEmitter.animation.play("life");
			bottomEmitter.animation.play("life");
			moves = true;
			solid = true;
			animation.play("idle");
		};
	}
	
	function spawnLeftToRight()
	{
		reset(0, -8);
		var moveSpeed = getSpeed();

		height = FlxG.height;
		scale.y = height / frameHeight / scale.y;
		velocity.set(moveSpeed, 0);

		topEmitter.spawn(x+4, y+12, 0, moveSpeed, 0);
		bottomEmitter.spawn(x+4, y+FlxG.height+4, 180, moveSpeed, 0);
	}

	function spawnRightToLeft()
	{
		reset(FlxG.width - width, -8);
		var moveSpeed = -getSpeed();

		height = FlxG.height;
		scale.y = height / frameHeight / scale.y;
		velocity.set(moveSpeed, 0);

		topEmitter.spawn(x+4, y+12, 0, moveSpeed, 0);
		bottomEmitter.spawn(x+4, y+FlxG.height+4, 180, moveSpeed, 0);
	}

	function spawnTopToBottom()
	{
		reset(0, 0);
		var moveSpeed = getSpeed();
		angle = -90;
		width = FlxG.width;
		scale.y = width / frameHeight / scale.y;
		velocity.set(0, moveSpeed);

		topEmitter.spawn(x+4, y+4, 270, 0, moveSpeed);
		bottomEmitter.spawn(x+FlxG.width-4, y+4, 90, 0, moveSpeed);
	}

	function spawnBottomToTop()
	{
		reset(0, FlxG.height - height);
		var moveSpeed = -getSpeed();

		angle = -90;
		width = FlxG.width;
		scale.y = width / frameHeight / scale.y;
		velocity.set(0, moveSpeed);

		topEmitter.spawn(x+4, y+4, 270, 0, moveSpeed);
		bottomEmitter.spawn(x+FlxG.width-4, y+4, 90, 0, moveSpeed);
	}

	override public function update(elapsed:Float):Void
	{
		if (!alive)
		{
			exists = false;	
		}
		else if (velocity.x > 0 && x > FlxG.width)
		{
			kill();
		}
		else if (velocity.x < 0 && x < -frameWidth * SCALE)
		{
			kill();
		}
		else if (velocity.y < 0 && y < frameHeight * SCALE)
		{
			kill();
		}
		else if (velocity.y > 0 && y > FlxG.height)
		{
			kill();
		}
		super.update(elapsed);
	}
	
	public function exit(duration:Float = 0.25):Void
	{
		animation.play("intro");
		new FlxTimer().start(duration, (_)->kill());
	}

	override public function kill():Void
	{
		topEmitter.kill();
		bottomEmitter.kill();
		super.kill();
	}
	
	inline function getSpeed():Float
	{
		return FlxG.random.int(MINSPEED, MAXSPEED);
	}
}