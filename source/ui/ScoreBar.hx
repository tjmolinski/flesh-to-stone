package ui;

import data.Levels;
import states.PlayState.LevelType;
import utils.Sounds;

import flixel.FlxBasic;
import flixel.FlxG;
import flixel.FlxSprite;
import flixel.effects.FlxFlicker;
import flixel.group.FlxGroup;
import flixel.group.FlxGroup.FlxTypedGroup;
import flixel.math.FlxPoint;
import flixel.math.FlxRect;
import flixel.tweens.FlxEase;
import flixel.tweens.FlxTween;
import flixel.util.FlxColor;
import flixel.util.FlxTimer;
import flixel.addons.display.FlxSliceSprite;

class ScoreBar extends ScoreBarStatic
{
	public inline static var USE_PELLETS_BAR = true;
	
	var maxPellets:Int;
	var pelletsGroup:FlxTypedGroup<Pellet>;
	var pelletsGathered = 0;
	var pelletsHighlighted = 0;
	var animPellet:Pellet;
	
	public function new(level:LevelType, xMargin = 4, y = ScoreBarStatic.DEFAULT_Y)
	{
		super(level, xMargin, y);
		isStatic = false;
		
		if (USE_PELLETS_BAR)
		{
			addAt(0, animPellet = Pellet.createEmpty());
			animPellet.highlight();
			animPellet.x = bar.x;
			animPellet.y = bar.y;
			addAt(0, pelletsGroup = new FlxTypedGroup<Pellet>());
		}
	}
	
	inline function addBehind(child:FlxBasic, relative:FlxBasic):FlxBasic
	{
		return addAt(members.indexOf(relative), child);
	}
	
	inline function addAt(pos:Int, child:FlxBasic):FlxBasic
	{
		add(child);
		members.remove(child);
		members.insert(pos, child);
		return child;
	}
	
	public function startIntro(duration:Float, ?onComplete:()->Void):Void
	{
		var options:TweenOptions = { ease:FlxEase.backOut };
		if (onComplete != null)
			options.onComplete = (_)->onComplete();
		
		FlxTween.num
			( -bar.y - bar.height, bar.y
			, duration
			, options
			, (y) -> bar.y = frame.y = back.y = y
			);
	}
	
	function resetEndlessBar(?onComplete:()->Void):Void
	{
		resetBar(0.25,
			function () 
			{
				var isTextIntro = multiplierText == null;
				if (isTextIntro)
				{
					multiplierText = new MultiplierText(bar.x + MultiplierText.BAR_OFFSET_X, bar.y);
					multiplierText.tweenDown(bar, 0.4);
					add(multiplierText);
				}
				
				multiplierText.text = "X" + multiplier;
				multiplier++;
				FlxFlicker.flicker(multiplierText, 0.5 + (isTextIntro ? 0.25 : 0));
				new FlxTimer().start(0.25,
					function(_)
					{
						multiplierText.text = "X" + multiplier;
						
						score = 0;
						
						if (onComplete != null)
							onComplete();
					}
				);
			}
		);
		
	}
	 
	public function addPoint(?onComplete:()->Void):Void
	{
		var extendedAnim = false;
		if (endless && score == maxScore - 1)
		{
			onComplete = resetEndlessBar.bind(onComplete);
			extendedAnim = true;
		}
		
		pointAnim.x = 1 + bar.x + Std.int(score * (bar.width - 2) / maxScore);
		if (USE_PELLETS_BAR)
		{
			var left = pelletsGroup.getFirstAlive();
			var right = pelletsGroup.members[pelletsGroup.countLiving() - 1];
			animPellet.x = left.x;
			animPellet.y = left.y;
			animPellet.width = left.width;
			animPellet.height = left.height;
			left.kill();
			// replace with anim
			left = animPellet;
			members.remove(left);
			members.unshift(left);
			left.revive();
			var leftStart = left.x;
			var rightStart = right.x;
			var end = pointAnim.x;
			var leftWidthStart = left.width;
			var rightWidthStart = right.width;
			var endWidth = pointAnim.width + 2;
			
			inline function lerp(start:Float, end:Float, mixer:Float):Int
			{
				return Math.round(start + (end - start) * mixer);
			}
			
			function onTweenUpdate(value:Float):Void
			{
				// Movbe left and right inward towards the next score slot
				if (right.alive)
				{
					left.x  = lerp(leftStart , end, value);
					right.x = lerp(rightStart, end, value);
					if (leftStart + leftWidthStart <= end + endWidth)
						left.width = lerp(leftWidthStart, endWidth, value);
					right.width = lerp(rightWidthStart, endWidth, value);
					
					if (right.x < left.x + left.width)
					{
						killPellets();
						left.width = right.x + right.width - left.x;
					}
				}
				else if (!right.alive)
				{
					left.x  = lerp(leftStart , end, value);
					left.width
						= lerp(rightWidthStart, endWidth, value)
					 	+ lerp(rightStart, end, value)
						- left.x;
				}
				
				
				var i = pelletsGroup.members.length;
				while (i-- > 0)
				{
					var pellet = pelletsGroup.members[i];
					
					if (pellet.on && pellet.alive && pellet != left && pellet != right)
					{
						if (pellet.x < end + endWidth)
						{
							if (left.x > pellet.x + pellet.width)
							{
								pellet.kill();
								continue;
							}
							
							if (left.x + left.width > pellet.x + 2)
							{
								pellet.width = pellet.x + pellet.width - left.x - left.width + 2;
								pellet.x = left.x + left.width - 2;
							}
						}
						
						if (right.alive && pellet.x + pellet.width > end + endWidth)
						{
							if (right.x < pellet.x)
							{
								pellet.kill();
								continue;
							}
							
							if (right.x < pellet.x + pellet.width - 2)
								pellet.width = right.x - pellet.x + 2;
						}
					}
				}
			}
			
			function onTweenComplete(_):Void
			{
				killPellets();
				
				members.remove(left);
				members.push(left);
				
				if (onComplete != null && !extendedAnim)
					onComplete();
				
				FlxTween.tween(
					left,
					{ y:bar.y, height:bar.height },
					0.25,
					{
						ease:FlxEase.backInOut,
						onComplete: function(_)
						{
							score++;
							left.kill();
							if (extendedAnim && onComplete != null)
							{
								Sounds.delay(0.1, SUCCESS);
								onComplete();
							}
							else
							{
								Sounds.delay(0.1, CHEER);
								FlxFlicker.flicker(pointAnim, 0.5, false);
							}
						}
					}
				);
			}
			FlxTween.num(0, 1, 0.25, { onComplete: onTweenComplete, ease:FlxEase.quadIn }, onTweenUpdate);
			// play random sound pair
			var ran = FlxG.random.int(1, Sounds.getCount(SCORE_SMOOSH)) - 1;
			Sounds.delay(0.25 - Sounds.getDuration(SCORE_SMOOSH, ran) + 0.05, SCORE_SMOOSH, ran);
			Sounds.delay(0.4, SCORE_LIFT);
		}
		else 
		{
			score++;
			FlxFlicker.flicker(pointAnim, 0.5, false, onComplete == null ? null : (_)->onComplete());
		}
	}
	
	inline function killPellets():Void
	{
		pelletsGroup.forEachAlive((pellet)->pellet.kill());
	}
	
	public function setMaxPellets(num:Int):Void
	{
		maxPellets = num;
		pelletsHighlighted = 0;
		pelletsGathered = 0;
		if (USE_PELLETS_BAR)
		{
			pelletsGroup.kill();
			pelletsGroup.exists = true;
			pelletsGroup.alive = true;
			
			if (maxPellets == 0)
				return;
			
			var x = 0;
			var fixedWidth = -1;
			if (bar.width / maxPellets < 8)
			{
				fixedWidth = Std.int(bar.width / maxPellets);
				x = Math.round((bar.width - (fixedWidth * maxPellets)) / 2);
			}
			
			// Staggewr spawn, otherwise all the pellets unfilling causes a bad performance spike
			var maxStaggerTime = 0.5;
			function spawnNew(timer:FlxTimer)
			{
				var index = timer.elapsedLoops;
				var pellet = pelletsGroup.recycle(Pellet.createEmpty);
				pellet.reset
					( bar.x + x
					, -pellet.height
					, fixedWidth > 0 ? fixedWidth : Std.int(index * bar.width / maxPellets) - x
					, pelletsHighlighted > index
					);
				x += Std.int(pellet.width);
				FlxTween.tween(pellet, { y:bar.y + 9 }, 0.25, { ease:FlxEase.backOut });
			}
			new FlxTimer().start(maxStaggerTime / maxPellets, spawnNew, maxPellets);
		}
	}
	
	public function setDisplayNumPellets():Void
	{
		if (USE_PELLETS_BAR)
		{
			getFirstUnhilighted(true).highlight();
		}
		else
		{
			pelletsHighlighted++;
			var width = getBarDividerX(score);
			width = width + Std.int((getBarDividerX(score + 1) - width) * pelletsHighlighted / maxPellets);
			bar.scale.x = width / bar.width;
		}
	}
	
	public function getFirstUnhilighted(iterate = false):Pellet
	{
		return pelletsGroup.members[iterate ? pelletsHighlighted++ : pelletsHighlighted];
	}
	
	public function getFirstUngathered(iterate = false):Pellet
	{
		return pelletsGroup.members[iterate ? pelletsGathered++ : pelletsGathered];
	}
	
	public function getPelletDestination():FlxPoint
	{
		return 
			if (USE_PELLETS_BAR)
				getFirstUngathered(true).getMidpoint();
			else
				FlxPoint.get(bar.x + ((getBarDividerX(score) + getBarDividerX(score + 1)) / 2), bar.y + bar.height / 2);
	}
}

class ScoreBarStatic extends FlxGroup
{
	public inline static var DEFAULT_Y = ScoreBar.USE_PELLETS_BAR ? 1.0 : 4.0;
	
	public var maxScore(default, null):Int;
	public var score(default, set):Int;
	
	var back:FlxSprite;
	var bar:FlxSprite;
	var frame:FlxSprite;
	var pointAnim:FlxSprite;
	var multiplierText:MultiplierText;
	var endless:Bool;
	var multiplier:Int = 0;
	var iconsByPoints = new Map<Int, FlxSprite>();
	var icons:FlxTypedGroup<FlxSprite>;
	var isStatic = true;
	
	public function new(level:LevelType, xMargin = 4, y = DEFAULT_Y)
	{
		super();
		
		this.endless = level.match(Endless|Empty(_));
		var levelNum:Int = -1;
		switch level
		{
			case Endless|Empty(_):
				maxScore = Levels.endlessBarSize;
			case Story(num):
				maxScore = Levels.scoreGoal;
				levelNum = num;
		}
		
		add(back = createBar(xMargin, y, 0xFF144491, "back"));
		back.offset.set(-1, -1);
		add(bar = createBar(xMargin, y, 0xFF5fcde4, "bar"));
		add(pointAnim = createPointAnim());
		icons = createIcons(levelNum);
		if (icons != null) add(icons);
		add(frame = createFrame());
		
		score = 0;
	}
	
	inline function createBar(xMargin:Int, y:Float, color:FlxColor, name:String):FlxSprite
	{
		var sprite = new FlxSprite(xMargin, y);
		sprite.makeGraphic(Std.int(FlxG.width) - xMargin * 2, 8, color, false, 'bar_$name');
		sprite.origin.x = 0;
		return sprite;
	}
	
	inline function createFrame():FlxSprite
	{
		var sprite = new FlxSprite(bar.x, bar.y, FlxG.bitmap.get("bar_frame"));
		if (sprite.graphic == null)
		{
			sprite.makeGraphic(Std.int(bar.width), Std.int(bar.height), 0xFFfaffff, false, "bar_frame");
			var rect = new openfl.geom.Rectangle(0, 1, 0, 6);
			for (i in 0...maxScore)
			{
				rect.left = rect.right + 1;
				rect.right = getBarDividerX(i+1);
				sprite.graphic.bitmap.fillRect(rect, 0);
			}
		}
		return sprite;
	}
	
	inline function createPointAnim():FlxSprite
	{
		var sprite = new FlxSprite(0, bar.y);
		sprite.makeGraphic(1, 1, 0xFFffffff);
		sprite.visible = false;
		sprite.scale.x = Math.ceil((bar.width - 2) / maxScore);
		sprite.scale.y = bar.height;
		sprite.width = Math.abs(sprite.scale.x) * sprite.frameWidth;
		sprite.height = Math.abs(sprite.scale.y) * sprite.frameHeight;
		sprite.origin.set();
		return sprite;
	}
	
	inline function createIcons(levelNum:Int):Null<FlxTypedGroup<FlxSprite>>
	{
		var group:FlxTypedGroup<FlxSprite> = null;
		if(!endless)
		{
			group = new FlxTypedGroup<FlxSprite>();
			
			if (Levels.seenFtue && levelNum < Levels.LEVELS - 1 && !Levels.getIsSeenUnlocked(levelNum + 1))
				group.add(addPointIcon(levelNum, Levels.unlockScoreGoal, "small_key_icon"));
			
			group.add(addPointIcon(levelNum, maxScore, "small_flag_icon"));
		}
		return group;
	}
	
	inline function addPointIcon(level:Int, point:Int, assetKey:String):FlxSprite
	{
		var icon = new FlxSprite('assets/images/ui/${assetKey}.png');
		icon.x = bar.x + Std.int((getBarDividerX(point - 1) + getBarDividerX(point) - icon.width) / 2);
		icon.y = bar.y + Std.int((8 - icon.height) / 2);
		if (Levels.getScore(level) < point)
			icon.colorTransform.color = 0xFF488bd4;
		iconsByPoints[point] = icon;
		return icon;
	}
	
	public function resetForRetry(duration:Float, ?onComplete:()->Void):Void
	{
		resetBar(duration, onComplete);
		
		if(multiplierText != null)
			FlxTween.tween(multiplierText, { y:-multiplierText.height }, duration, { ease:FlxEase.backIn });
	}
	
	function resetBar(duration:Float, ?onComplete:()->Void):Void
	{
		if (bar.scale.x > 0)
		{
			var tweenArgs:TweenOptions = { ease: FlxEase.cubeIn };
			if (onComplete != null)
				tweenArgs.onComplete = (_)->onComplete();
			FlxTween.tween(bar.scale, { x:0 }, duration, tweenArgs);
			// this.score = 0;
		}
	}
	
	inline public function setNoScrollMode():Void
	{
		back.scrollFactor.set();
		bar.scrollFactor.set();
		frame.scrollFactor.set();
		if (!endless)
			icons.forEachAlive((icon)->icon.scrollFactor.set());
		else if (multiplierText != null)
			multiplierText.scrollFactor.set();
	}
	
	inline function getBarDividerX(index:Int):Int
	{
		return 1 + Std.int(index * (bar.width - 2) / maxScore);
	}
	
	function set_score(value:Int):Int
	{
		if (!endless)
		{
			for (i => icon in iconsByPoints)
			{
				if (value >= i)
					icon.setColorTransform();
			}
		}
		else if (isStatic && endless && value >= maxScore)
		{
			multiplier = Math.floor(value / maxScore);
			value = value % maxScore;
			
			multiplierText = MultiplierText.createStatic(bar, multiplier);
			add(multiplierText);
		}
		bar.scale.x = getBarDividerX(value) / bar.width;
		return this.score = value;
	}
	
	public function flickerPoint(point:Int, duration = 1.0, ?onComplete:()->Void):FlxFlicker
	{
		pointAnim.x = bar.x + getBarDividerX(point - 1);
		return FlxFlicker.flicker(pointAnim, duration, false, onComplete == null ? null : (_)->onComplete());
	}
}

@:forward
abstract Pellet(FlxSliceSprite) to FlxSliceSprite
{
	inline static var ON  = "assets/images/ui/UiPellet_filled.png";
	inline static var OFF = "assets/images/ui/UiPellet.png";
	
	static public var rect = FlxRect.get(2, 0, 1, 6);
	static public var sourceRect = FlxRect.get(0, 0, 6, 6);
	
	public var on(get, never):Bool;
	
	public function new (x = 0, y = 0, width = 6)
	{
		this = new FlxSliceSprite(OFF, rect, width, sourceRect.height, sourceRect);
		this.stretchCenter = true;
		reset(x, y, width);
	}
	
	inline public function switchGraphic(on:Bool):Void
	{
		var graphicKey = on ? ON : OFF;
		if (graphicKey != this.graphic.assetsKey)
			this.loadGraphic(graphicKey);
	}
	
	inline public function reset(x = 0.0, y = 0.0, width = 6, on = false):Void
	{
		switchGraphic(on);
		this.x = x;
		this.y = y;
		this.width = width;
		this.height = sourceRect.height;
	}
	
	inline public function highlight():Void
	{
		switchGraphic(true);
	}
	
	inline public function get_on() return this.graphic.assetsKey == ON;
	
	static public function create(x = 0, y = 0, width = 6):Pellet
	{
		return new Pellet(x, y, width);
	}
	
	static public function createEmpty():Pellet { return new Pellet(); }
}

@:forward
abstract MultiplierText(BitmapText) to BitmapText
{
	inline static public var BAR_OFFSET_X = 1;
	inline static public var BAR_OFFSET_Y = 7;
	
	inline public function new (x = 0.0, y = 0.0, value = 0)
	{
		this = new BitmapText(x, y, 'X$value');
	}
	
	inline public function tweenDown(bar:FlxSprite, duration:Float):Void
	{
		FlxTween.tween(this, { y:bar.y + bar.height + BAR_OFFSET_Y }, duration, { ease:FlxEase.circInOut, startDelay:0.15 });
	}
	
	inline static public function createStatic(bar:FlxSprite, value:Int):MultiplierText
	{
		return new MultiplierText(bar.x + BAR_OFFSET_X, bar.y + bar.height + BAR_OFFSET_Y, value);
	}
}