package data;

typedef EndStateAssetData =
{
	prevLevel:states.PlayState.LevelType,
	unlockedLevel:Bool,
	unlockedMedal:Int,
	newBest:Bool,
	score:Int,
	koX:Float,
	koY:Float
};