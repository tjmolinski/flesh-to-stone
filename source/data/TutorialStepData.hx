package data;


import flixel.FlxObject;
import ui.BitmapText;

import flixel.FlxSprite;
import flixel.group.FlxGroup;
import flixel.tweens.FlxEase;
import flixel.tweens.FlxTween;
import flixel.util.FlxTimer;

typedef ObjectProperties = { ?intro:String, ?outro:String, ?animation:Array<Int>, ?delay:Float };
typedef ObjectData = { name:String, type:String, x:Float, y:Float, width:Float, height:Float, properties:ObjectProperties };
typedef TextData = { >ObjectData, text:String };
typedef StepProperties =
{ ?waitAction:String
, ?goalGroupName:String
, ?enemyConfig:Int
, ?stoneEnabled:Bool
, ?enemiesEnabled:Bool
, ?mobileHilite:String
};

class TutorialStepData extends FlxGroup
{
	public var stepNumber:Int;
	public var objects:Map<String, ObjectData>;
	public var goalGroupName (get,never):String; inline function get_goalGroupName () return properties.goalGroupName;
	public var waitAction    (get,never):String; inline function get_waitAction    () return properties.waitAction;
	public var enemyConfig   (get,never):Int   ; inline function get_enemyConfig   () return properties.enemyConfig;
	public var stoneEnabled  (get,never):Bool  ; inline function get_stoneEnabled  () return properties.stoneEnabled;
	public var enemiesEnabled(get,never):Bool  ; inline function get_enemiesEnabled() return properties.enemiesEnabled;
	public var mobileHilite  (get,never):String; inline function get_mobileHilite  () return properties.mobileHilite;
	
	var properties:StepProperties;
	
	public function new 
		( stepNumber:Int
		, objects   :Map<String, ObjectData>
		, properties:StepProperties
		)
	{
		this.stepNumber = stepNumber;
		this.objects = objects;
		this.properties = properties;
		
		super();
	}
	
	public function startIntro(?callback:()->Void):Void
	{
		var delay = 0.0;
		for (objectData in objects)
		{
			var object:FlxSprite;
			switch (objectData.type)
			{
				case "BitmapText":
					object = new TutorialText(cast objectData);
				case "FollowSprite":
					object = new FollowSprite(objectData);
				case type:
					throw 'unrecognized type: $type';
			}
			
			var objDelay = transitionIn(object, objectData.properties.intro, objectData.properties.delay);
			if (delay < objDelay)
				delay = objDelay;
			add(object);
		}
		
		if (callback != null)
		{
			if (delay > 0)
				new FlxTimer().start(delay, (_)->callback());
			else callback();
		}
	}
	
	public function startOutro(?callback:()->Void):Void
	{
		var delay = 0.0;
		for (member in members)
		{
			if (Std.is(member, ITransition))
			{
				var objDelay = transitionOut(cast member, (cast member:ITransition).properties.outro);
				if (delay < objDelay)
					delay = objDelay;
			}
		}
		
		if (callback != null)
		{
			if (delay > 0)
				new FlxTimer().start(delay, (_)->callback());
			else
				callback();
		}
	}
	
	function transitionIn(obj:FlxSprite, intro:String, delay = 0.0):Float
	{
		switch (intro)
		{
			case "fade_up":
				obj.alpha = 0;
				obj.y += 16;
				FlxTween.tween(obj, { alpha:1, y:obj.y - 16 }, 0.5, { ease:FlxEase.sineOut, startDelay: delay });
				return 0.5 + delay;
			
			case "fade_down":
				obj.alpha = 0;
				obj.y -= 16;
				FlxTween.tween(obj, { alpha:1, y:obj.y + 16 }, 0.5, { ease:FlxEase.sineOut, startDelay: delay });
				return 0.5 + delay;
			
			case "scale":
				obj.scale.set();
				FlxTween.tween(obj, { 'scale.x':1, 'scale.y':1 }, 0.25, { ease:FlxEase.sineOut, startDelay: delay });
				return 0.25 + delay;
				
			case "", null, _:
				if (delay > 0)
				{
					obj.visible = false;
					new FlxTimer().start(delay, (_)->{ obj.visible = true; });
				}
				return delay;
		}
	}
	
	function transitionOut(obj:FlxSprite, outro:String):Float
	{
		switch (outro)
		{
			case "fade_up":
				FlxTween.tween(obj, { alpha:0, y:obj.y - 16 }, 0.5, { ease:FlxEase.sineIn });
				return 0.5;
				
			case "fade_down":
				FlxTween.tween(obj, { alpha:0, y:obj.y + 16 }, 0.5, { ease:FlxEase.sineIn });
				return 0.5;
			
			case "scale":
				FlxTween.tween(obj, { 'scale.x':0, 'scale.y':0 }, 0.25, { ease:FlxEase.sineOut });
				return 0.25;
				
			case "", null, _:
				return 0;
		}
	}
	
	static var nameRegExp = ~/Step(\d+)/;
	static public function fromXml(xml:Xml):TutorialStepData
	{
		var name = xml.get("name");
		if (nameRegExp.match(name))
			name = nameRegExp.matched(1);
		
		var properties:StepProperties = {};
		for (property in xml.elementsNamed("properties").next().elements())
		{
			switch (property.get("name"))
			{
				case "goalgroupname" : properties.goalGroupName  = property.get("value");
				case "waitaction"    : properties.waitAction     = property.get("value");
				case "enemyconfig"   : properties.enemyConfig    = Std.parseInt(property.get("value"));
				case "stoneenabled"  : properties.stoneEnabled   = property.get("value") == "true";
				case "enemiesenabled": properties.enemiesEnabled = property.get("value") == "true";
				case "mobilehilite"  : properties.mobileHilite   = property.get("value");
			}
		}
		
		var objects:Map<String, ObjectData> = new Map();
		for (objectXml in xml.elementsNamed("object"))
		{
			var object = objectFromXml(objectXml);
			objects[object.name] = object;
		}
		
		return new TutorialStepData
			( Std.parseInt(name)
			, objects
			, properties
			);
	}
	
	static function objectFromXml(xml:Xml):ObjectData
	{
		var properties:ObjectProperties = {};
		
		var propertiesList = xml.elementsNamed("properties");
		if (propertiesList.hasNext())
		{
			for (property in propertiesList.next().elements())
			{
				switch (property.get("name"))
				{
					case "intro"    : properties.intro     = property.get("value");
					case "outro"    : properties.outro     = property.get("value");
					case "delay"    : properties.delay     = Std.parseFloat(property.get("value"));
					case "animation": properties.animation = [for (i in property.get("value").split(",")) Std.parseInt(i)];
				}
			}
		}
		
		var object =
			{ name      :xml.get("name")
			, type      :xml.get("type")
			, x         :Std.parseFloat(xml.get("x"))
			, y         :Std.parseFloat(xml.get("y"))
			, width     :Std.parseFloat(xml.get("width"))
			, height    :Std.parseFloat(xml.get("height"))
			, properties:properties
			};
		
		switch (xml.get("type"))
		{
			case "BitmapText":
				(cast object:TextData).text = xml.elementsNamed("text").next().firstChild().nodeValue;
			case "FollowSprite":// no error
			case type:
				throw 'unrecognized type:$type';
		}
		return object;
	}
	
	static public function arrayFromXml(data:Xml):Array<TutorialStepData>
	{
		var steps:Array<TutorialStepData> = [];
		for(group in data.elementsNamed("objectgroup"))
		{
			steps.push(TutorialStepData.fromXml(group));
		}
		steps.sort(sortSteps);
		return steps;
	}
	
	static function sortSteps(left:TutorialStepData, right:TutorialStepData):Int
	{
		return left.stepNumber - right.stepNumber;
	}
}

interface ITransition
{
	var properties(default, null):ObjectProperties;
	var alpha(default, set):Float;
	var scale(default, null):flixel.math.FlxPoint;
	var x(default, set):Float;
	var y(default, set):Float;
}

class TutorialText extends BitmapText implements ITransition
{
	public var properties(default, null):ObjectProperties;
	
	public function new (data:TextData):Void
	{
		super(data.x, data.y, data.text, NokiaText.font);
		
		ID = Std.parseInt(data.name.split("text").pop());
		x += (data.width - width) / 2;
		properties = data.properties;
		alignment = CENTER;
	}
}

class TutorialObject extends FlxSprite implements ITransition
{
	public var properties(default, null):ObjectProperties;
	
	public function new(data:ObjectData)
	{
		super(data.x, data.y, 'assets/images/ui/${data.name}.png');
		properties = data.properties;
		
		if (width > data.width || height > data.height)
		{
			loadGraphic(graphic, true, Std.int(data.width), Std.int(data.height));
			if (properties.animation != null)
				animation.add("default", properties.animation, 8);
			else
				animation.add("default", [for(i in 0...frames.frames.length) i], 8);
			animation.play("default");
		} 
	}
}

class FollowSprite extends TutorialObject
{
	public var target:FlxObject;
	
	public function new(data:ObjectData)
	{
		super(data);
		origin.y = data.height * 1.5;
		offset.copyFrom(origin);
	}
	
	override function update(elapsed:Float)
	{
		super.update(elapsed);
		
		if (target == null)
			return;
		
		if (!target.exists)
			target = null;// mem leak double protection
		
		x = target.x + target.width / 2;
		y = target.y + target.height / 2;
	}
}