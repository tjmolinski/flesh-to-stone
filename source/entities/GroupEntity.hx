package entities;

import flixel.math.FlxPoint;
import flixel.FlxObject;

class GroupEntity extends flixel.group.FlxSpriteGroup {
	
	var rect:FlxObject = new FlxObject();
	var proxyMember:FlxObject;
	
	public function new (x = 0.0, y = 0.0, maxSize = 0) {
		super(x, y, maxSize);
	}
	
	public function getRect():FlxObject {
		
		rect.x = proxyMember.x;
		rect.y = proxyMember.y;
		rect.last.copyFrom(proxyMember.last);
		rect.width = proxyMember.width;
		rect.height = proxyMember.height;
		rect.allowCollisions = proxyMember.allowCollisions;
		
		return rect;
	}
	
	public function setFromRect(rect:FlxObject):Void {
		
		x = rect.x;
		y = rect.y;
		proxyMember.last.copyFrom(rect.last);
	}
}