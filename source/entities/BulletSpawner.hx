package entities;

import flixel.FlxG;
import flixel.math.FlxPoint;
import flixel.group.FlxGroup;

/**
 * ...
 * @author TJ
 */
class BulletSpawner extends BaseShooter 
{
	public function new(x = 0.0, y = 0.0) { super(x, y); }
	
	public function init(pos:FlxPoint, bullets:FlxTypedGroup<Bullet>):Void {
		
		spawn(pos, bullets);
	}
	
	override private function shootBullets():Void
	{
		fireRandomPattern();
		switch(level)
		{
			case 0:
				if (FlxG.random.bool(50))
					nextAction = Leave;
			case 1:
				if (FlxG.random.bool(75))
					nextAction = Leave;
			case 2:
				if (FlxG.random.bool(90))
					nextAction = Leave;
			case 3:
				nextAction = Leave;
		}
	}
	
	private function fireRandomPattern():Void
	{
		var rand:Float = FlxG.random.float();
		var neg:Int = FlxG.random.bool(50) ? 1 : -1;
		var offset:Int = 0;
		var offsetRandom:Float = FlxG.random.float();
		if (offsetRandom > 0.75)
		{
			offset = 45;
		}
		else if (offsetRandom > 0.5)
		{
			offset = -45;
		}
		else if (offsetRandom > 0.25)
		{
			offset = 90;
		}
		else
		{
			offset = -90;
		}
		
		playRandomSound();
		
		if (rand > 0.75)
		{
			for (i in 0...6)
			{
				bullets.recycle(Bullet).spawn(x + halfWidth, y + halfHeight, ((36 * i) * neg) + offset);
			}
		}
		else if (rand > 0.5)
		{
			for (i in 0...10)
			{
				bullets.recycle(Bullet).spawn(x + halfWidth, y + halfHeight, ((18 * i) * neg) + offset);
			}
		}
		else if (rand > 0.25)
		{
			for (i in 0...10)
			{
				bullets.recycle(Bullet).spawn(x + halfWidth, y + halfHeight, (36 * i));
			}
		}
		else 
		{
			for (i in 0...20)
			{
				bullets.recycle(Bullet).spawn(x + halfWidth, y + halfHeight, (9 * i));
			}
		}
	}
}