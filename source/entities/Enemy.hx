package entities;

import utils.Sounds;
import flixel.FlxG;
import flixel.FlxSprite;
import flixel.effects.FlxFlicker;
import flixel.math.FlxPoint;
import flixel.tile.FlxTilemap;

import vfx.Warning;

class Enemy extends BaseSprite implements IMurderable {
	
	function new (x = 0.0, y = 0.0) { super(x, y); }
	
	function playWarningAnim(sprite:Warning, ?callback:Void->Void):Void {
		
		var wasMoving = moves;
		moves = false;
		var wasSolid = solid;
		solid = false;
		
		Sounds.play(SPAWN);
		sprite.play(this, function() {
			
			moves = wasMoving;
			solid = wasSolid;
			if (callback != null)
				callback();
		});
	}
	
	public function exit(?sprite:Warning, ?callback:()->Void):Void {
		
		if (sprite != null)
			sprite.exit(this, FlxG.random.float(0, 0.5), callback);
	}
		
	
	function setEdgePosition():Void {
		
		if (FlxG.random.bool()) {
			// LEFT/RIGHT
			x = FlxG.random.bool() ? -width : FlxG.width + width;
			y = FlxG.random.float(-height, FlxG.height + height);
			
		} else {
			// TOB/BOTTOM
			x = FlxG.random.float(-width, FlxG.width + width);
			y = FlxG.random.bool() ? -height : FlxG.height + height;
		}
		reset(x, y);
	}
	
	public function checkOffLand(map:FlxTilemap):Void {
		
		if (moves && getIsOffLand(map)) {
			
			var wasMoving = moves;
			moves = false;
			var wasSolid = solid;
			solid = false;
			FlxFlicker.flicker(this, 0.5, 0.04, false, true,
				function(_) {
					
					moves = wasMoving;
					solid = wasSolid;
					murder();
				}
			);
		}
	}
	
	public function getIsTouchingWater(map:FlxTilemap):Bool {
		
		return isCoordOnWater(map, x        , y)
			|| isCoordOnWater(map, x + width, y)
			|| isCoordOnWater(map, x        , y + height)
			|| isCoordOnWater(map, x + width, y + height);
	}
	
	public function getIsOffLand(map:FlxTilemap):Bool {
		
		return isCoordOnWater(map, x        , y)
			&& isCoordOnWater(map, x + width, y)
			&& isCoordOnWater(map, x        , y + height)
			&& isCoordOnWater(map, x + width, y + height);
	}
	
	inline function isCoordOnWater(map:FlxTilemap, x:Float, y:Float):Bool {
		
		return map.getTileCollisions(
			map.getTileByIndex(
				map.getTileIndexByCoords(FlxPoint.weak(x, y))
			)
		) > 0;
	}
	
	public function murder():Void {}
}