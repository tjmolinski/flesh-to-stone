package ui;

import flixel.FlxG;
import flixel.FlxSprite;
import flixel.effects.FlxFlicker;
import flixel.graphics.frames.FlxTileFrames;
import flixel.math.FlxPoint;
import flixel.tweens.FlxEase;
import flixel.tweens.FlxTween;

import openfl.geom.Rectangle;

import data.Levels;
import ui.BitmapText;

class LevelIconBase extends flixel.group.FlxSpriteGroup {
	
	inline static var COLUMNS = 3;
	inline static var TOP = 18;
	inline static var Y_SPACING = LevelIconBase.BOX_HEIGHT + 8;
	
	inline static public var BOX_WIDTH = 20;
	inline static public var BOX_HEIGHT = 20;
	inline static var FILL_COLOR = 0xFF52294b;
	inline static var OFF_COLOR  = 0xFF928fb8;
	inline static var ON_COLOR   = 0xFFfff540;
	
	inline static var ASSET_KEY = "levelIcon";
	
	public var boxWidth(get, never):Int;
	public var boxHeight(get, never):Int;
	
	var label:BitmapText;
	var box:FlxSprite;
	
	public function new (index:Int, text:String):Void {
		super(getIconX(index), getIconY(index));
		
		box = new FlxSprite(0, 0, FlxG.bitmap.get(ASSET_KEY));
		if (box.graphic == null)
		{
			box.makeGraphic(BOX_WIDTH * 2, BOX_HEIGHT, ON_COLOR, ASSET_KEY);
			var rect = new Rectangle(0, 0, BOX_WIDTH, BOX_HEIGHT);
			box.graphic.bitmap.fillRect(rect, OFF_COLOR);
			rect.x = 1;
			rect.y = 1;
			rect.width -= 2;
			rect.height -= 2;
			box.graphic.bitmap.fillRect(rect, FILL_COLOR);
			rect.x += BOX_WIDTH;
			box.graphic.bitmap.fillRect(rect, FILL_COLOR);
		}
		box.frames = FlxTileFrames.fromGraphic(box.graphic, FlxPoint.get(BOX_WIDTH, BOX_HEIGHT));
		box.animation.add("off", [0]);
		box.animation.add("on", [1]);
		add(box);
		
		label = new BitmapText(0, 4, text, 0);
		label.x = (box.width - label.width) / 2 + 1;
		add(label);
		
		deselect();
	}
	
	inline static public function getIconX(index)
	{
		return FlxG.width * (1 + (index % COLUMNS)) / (COLUMNS + 1) - BOX_WIDTH / 2;
	}
	
	inline static public function getIconY(index)
	{
		return TOP + Std.int(index / COLUMNS) * Y_SPACING;
	}
	
	public function showIntro():Float {
		
		scale.x = 0.01;
		scale.y = 0.01;
		FlxTween.tween(this, { 'scale.x':1, 'scale.y':1 }, 0.5, { ease:FlxEase.elasticOut });
		return 0.5;
	}
	
	public function resetProgress(delay:Float):Float { return 0; }
	
	public function deselect():Void {
		
		box.animation.play("off");
	}
	
	public function select():Void {
		
		box.animation.play("on");
	}
	
	inline function get_boxWidth () { return BOX_WIDTH; }
	inline function get_boxHeight() { return BOX_HEIGHT; }
	
	override function get_width ():Float { return boxWidth  * scale.x; }
	override function get_height():Float { return boxHeight * scale.y; }
}

class LevelIconNewGamePlus extends LevelIconBase {
	
	public function new ():Void {
		
		super(13, "+");
		label.y++;
		label.x--;
	}
	
	public function hide():Void {
		
		FlxTween.tween(this, { 'scale.x':0, 'scale.y':0 }, 0.5, { ease:FlxEase.elasticOut });
	}
}

class LevelIcon extends LevelIconBase {
	
	inline static public var INTRO_STAGGER = .5 / Levels.LEVELS;
	inline static public var INTRO_ICON_TIME = 0.45;
	inline static public var INTRO_BAR_TIME = 0.5;
	
	public var level(default, null):Int;
	
	var barTrack:Null<TrackBar>;
	var barProgress:Null<ProgressBar>;
	var lock:Null<FlxSprite>;
	
	public function new (level:Int):Void {
		
		super(level, Std.string(level + 1));
		
		this.level = level;
		
		if (Levels.getIsSeenUnlocked(level))
			createBars();
		else
			createLock();
	}
	
	inline function createBars():Void
	{
		add(barTrack = new TrackBar());
		
		add(barProgress = new ProgressBar(Levels.getSeenScoreRatio(level)));
		// barProgress.kill();
	}
	
	function createLock():Void {
		
		label.visible = false;
		lock = new FlxSprite(0, 0, "assets/images/ui/lock_icon.png");
		lock.x = (boxWidth - lock.width) / 2;
		lock.y = (boxHeight - lock.height) / 2;
		add(lock);
	}
	
	override function showIntro():Float {
		
		var tweenOptions:TweenOptions = 
			{ startDelay:level * INTRO_STAGGER
			, ease:FlxEase.elasticOut
			};
		
		scale.set(0.01, 0.01);
		
		FlxTween.tween(this, { 'scale.x':1, 'scale.y':1 }, INTRO_ICON_TIME, tweenOptions);
		
		return tweenOptions.startDelay + INTRO_ICON_TIME;
	}
	
	public function showNewProgress(callback:Void->Void):Void
	{
		FlxFlicker.flicker(barProgress, INTRO_BAR_TIME);
		barProgress.scaleTo(Levels.getScoreRatio(level), INTRO_BAR_TIME, callback);
	}
	
	public function showUnlock(callback:Void->Void):Void
	{
		FlxFlicker.flicker(lock, 0.5, false, true,
			function (_) {
				
				lock.kill();
				createBars();
				barTrack.scaleTo(1.0, 0.25, 0);
				FlxFlicker.flicker(label, 0.5);
				callback();
			}
		);
	}
	
	override function resetProgress(delay:Float):Float {
		
		var duration;
		if (level == 0)
			duration = barProgress.resetProgress(delay);
		else {
			
			remove(barTrack);
			duration = barProgress.resetProgress(
				delay,
				function() {
					
					createLock();
					lock.scale.set(0, 0);
					FlxTween.tween(lock, { "scale.x": 1, "scale.y":1 }, 0.5, { ease: FlxEase.backOut });
				}
			);
			duration += 0.5;
		}
		return duration + delay;
	}
}

@:forward
abstract TrackBar(IconBar) to FlxSprite {
	
	inline public function new () { this = new IconBar(0xFF5b537d); }
}

@:forward
abstract ProgressBar(IconBar) to FlxSprite {
	
	inline static public var PERCENT_COLOR = 0xFFfaffff;
	inline static public var FULL_COLOR = 0xFFfff540;
	
	inline public function new (scale:Float) {
		
		this = new IconBar(scale >= 1 ? FULL_COLOR : PERCENT_COLOR, scale);
	}
	
	public function scaleTo(scale:Float, duration:Float, ?callback:Void->Void):Void {
		
		this.scaleTo(scale, duration, callback, FULL_COLOR);
	}
	
	inline public function resetProgress(delay = 0.0, ?onComplete:Void->Void):Float {
		
		this.color = PERCENT_COLOR;
		var options:TweenOptions = { ease:FlxEase.quintIn, startDelay:delay };
		if (onComplete != null)
			options.onComplete = function(_) { onComplete(); };
		
		FlxTween.tween(this, { 'scale.x':0 }, 0.5, options);
		return 0.5;
	}
}

@:forward
abstract IconBar(FlxSprite) to FlxSprite {
	
	inline static var PADDING = 3;
	inline static var MAX_WIDTH = LevelIconBase.BOX_WIDTH - 2 * PADDING;
	
	
	inline public function new (color:Int, scale = 1.0) {
		this = new FlxSprite(PADDING, LevelIconBase.BOX_HEIGHT - PADDING - 1);
		
		setWidthFromMaxScale(scale);
		this.color = color;
	}
	
	/**
	 * Scaling the parent group sets the scale of the bar, which we scale to 1 in the intro. 
	 * we create different sized images so we can always display this at scale 1.
	 * @param scale 
	 */
	inline function setWidthFromMaxScale(scale:Float):Void
	{
		var width = Std.int(scale * MAX_WIDTH);
		
		if (width > 0)
			this.makeGraphic(width, 1, 0xFFffffff, true, 'levelIcon.bar-$width');
		else
			this.makeGraphic(MAX_WIDTH, 1, 0, true, 'levelIcon.bar-$width');
		
		this.width = width;
		this.origin.x = 0;
	}
	
	public function scaleTo(scale:Float, duration:Float, ?callback:Void->Void, filledColor = 0):Void {
		
		this.scale.x = this.width / MAX_WIDTH;
		setWidthFromMaxScale(1);
		this.alive = true;
		
		var tweenOptions:TweenOptions = { ease:FlxEase.quintOut,
			onComplete: function(_) {
				
				if (scale == 1 && filledColor != 0)
					this.color = filledColor;
				
				if (callback != null)
					callback();
			}
		};
		
		FlxTween.tween(this, { 'scale.x':scale }, duration, tweenOptions);
	}
}