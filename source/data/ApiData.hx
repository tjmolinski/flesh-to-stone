package data;

class ApiData {
	
	inline public static var id:String = "49447:eekbT5Au";
	inline public static var encryption:String = "2KYW5q5hIRNwHTV8aRIWRA==";
	
	inline public static var scoreboard_levels:Int = 8643;
	inline public static var scoreboard_endless:Int = 8644;
	
	inline public static var medal_story6:Int = 57688;
	inline public static var medal_story12:Int = 57689;
	inline public static var medal_story18:Int = 57690;
	inline public static var medal_story24:Int = 57691;
	inline public static var medal_endless24:Int = 57692;
}