package states;

import states.PlayState;
import utils.Sounds;
import ui.Inputs;

import flixel.FlxSprite;
import flixel.FlxState;
import flixel.text.FlxText;
import flixel.FlxG;
import flixel.effects.FlxFlicker;

/**
 * ...
 * @author TJ
 */
class InstructionsState extends FlxState 
{
	private var titleText:FlxText;
	private var instructionsText:FlxText;
	private var instructionBG:FlxSprite;
	
	private var enemiesText:FlxText;
	private var heroText:FlxText;
	private var abilityText:FlxText;
	private var powerupText:FlxText;
	private var powerMeterText:FlxText;
	private var moveKeysText:FlxText;
	private var noPowerDeathText:FlxText;
	private var switchingState:Bool = false;
	
	override public function create():Void
	{
		instructionBG = new FlxSprite(0, 0);
		instructionBG.loadGraphic("assets/images/instructions.png", false);
		add(instructionBG);
		
		titleText = new FlxText(0, FlxG.height * 0.05, FlxG.width, "HOW TO PLAY", 20, false);
		titleText.alignment = FlxTextAlign.CENTER;
		add(titleText);
		
		instructionsText = new FlxText(0, FlxG.height * 0.9, FlxG.width, "PRESS SHIFT KEY TO START", 20, false);
		instructionsText.alignment = FlxTextAlign.CENTER;
		add(instructionsText);
		
		enemiesText = new FlxText(FlxG.width * 0.5, FlxG.height * 0.2125, 200, "WATCH OUT FOR THESE THINGS!!!\nTHEY WILL KILL YOU", 10, false);
		add(enemiesText);
		
		heroText = new FlxText(FlxG.width * 0.09, FlxG.height * 0.75, 100, "THIS IS YOU ->", 10, false);
		add(heroText);
		
		abilityText = new FlxText(FlxG.width * 0.425, FlxG.height * 0.7125, 300, "PRESS SHIFT KEY TO CHANGE INTO STONE FORM\nIT MAKES YOU INVINCIBLE\nBUT DRAINS YOUR POWER", 10, false);
		add(abilityText);
		
		powerupText = new FlxText(FlxG.width * 0.275, FlxG.height * 0.53, 175, "<- COLLECT THESE TO FILL UP YOUR POWER BAR AND GET POINTS", 10, false);
		add(powerupText);
		
		noPowerDeathText = new FlxText(FlxG.width * 0.225, FlxG.height * 0.275, 100, "RUNNING OUT OF POWER WILL KILL YOU", 10, false);
		add(noPowerDeathText);
		
		powerMeterText = new FlxText(FlxG.width * 0.05, FlxG.height * 0.125, 175, "THIS IS YOUR POWER METER\nDONT DRAIN IT ALL!", 10, false);
		add(powerMeterText);
		
		moveKeysText = new FlxText(0, FlxG.height * 0.85, FlxG.width, "RUN WITH WASD OR IJKL", 10, false);
		moveKeysText.alignment = FlxTextAlign.CENTER;
		add(moveKeysText);
		
		super.create();
		
		FlxG.camera.fade(0xff000000, 0.5, true, function(){}, true);
	}
	
	override public function update(elapsed:Float):Void
	{
		if (Inputs.justPressed.ACCEPT && !switchingState)
		{
			switchingState = true;
			Sounds.play(MENU_SELECT);
			FlxFlicker.flicker(instructionsText, 0.5, 0.05, true, true);
			FlxG.camera.fade(0xff000000, 0.5, false, function(){
				FlxG.switchState(new PlayState(Endless));
			}, true);
			
		}
	}
	
}