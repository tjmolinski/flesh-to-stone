package data;

import flixel.FlxObject;
import flixel.FlxG;
import data.Tilemap.TileRange;

import flixel.math.FlxPoint;
import flixel.graphics.frames.FlxTileFrames;

class WaterMap extends flixel.tile.FlxTilemap {
	
	static var waterTileRange = new TileRange(46, 48, 0.8);
	
	var foreverScroll = FlxObject.NONE;
	
	public function new(tiles:Int, heightInTiles:Int = 20)
	{
		super();
		
		loadMapFromArray
			( [for (i in 0...20 * heightInTiles) waterTileRange.get()]
			, 20, heightInTiles
			, FlxTileFrames.fromBitmapAddSpacesAndBorders
				( 'assets/images/island-tiles${tiles + 1}.png'
				, FlxPoint.get(8, 8)
				, FlxPoint.get()
				, FlxPoint.get(2, 2)
				)
			, 8, 8
			);
	}
	
	public function clearBottomRow():Void
	{
		var i = totalTiles - widthInTiles;
		while (i < totalTiles)
			this._data[i++] = 46;
	}
	
	override function update(elapsed:Float)
	{
		super.update(elapsed);
		
		if (foreverScroll == FlxObject.NONE)
			return;
		
		var shift = Std.int((FlxG.camera.scroll.y - y) / 8);
		
		if (shift != 0 && foreverScroll & (shift > 0 ? FlxObject.DOWN : FlxObject.UP) > 0)
		{
			while(shift > 0)
			{
				var row = _data.splice(0, widthInTiles);
				while (row.length > 0)
					_data.push(row.shift());
				
				shift--;
				y += 8;
			}
			
			while(shift < 0)
			{
				var row = _data.splice(-widthInTiles, widthInTiles);
				while (row.length > 0)
					_data.unshift(row.pop());
				
				shift++;
				y -= 8;
			}
		}
	}
	
	public function enableForeverScroll(forceDir:Int = FlxObject.DOWN):Void { foreverScroll = forceDir; }
}