package ui;

import flixel.tweens.FlxTween;
import flixel.tweens.FlxEase;
import flixel.FlxG;
import flixel.FlxSprite;

enum InstructionsType
{
	Menu;
	Play;
}

class ControlInstructions extends flixel.group.FlxGroup.FlxTypedGroup<FlxSprite> {
	
	var keys:FlxSprite;
	var pad:FlxSprite;
	
	public function new (type:InstructionsType = Menu, showNow = false) {
		super(2);
		final strType = type == Menu ? "menu" : "play";
		add(keys = new FlxSprite(0, 0, 'assets/images/ui/keys_${strType}_help.png'));
		keys.x = (FlxG.width - keys.width) / 2;
		keys.y = FlxG.height;
		keys.visible = false;
		add(pad = new FlxSprite(0, 0, 'assets/images/ui/pad_${strType}_help.png'));
		pad.x = (FlxG.width - pad.width) / 2;
		pad.y = FlxG.height;
		pad.visible = false;
		
		Inputs.onInputChange.add(show);
		if (showNow)
			show();
	}
	
	public function show():Void {
		
		var sprite;
		if (Inputs.usingPad())
			sprite = pad;
		else
			sprite = keys;
		
		sprite.visible = true;
		
		sprite.y = FlxG.height;
		FlxTween.tween(sprite, { y:FlxG.height - sprite.height, alpha: 1 }, .25, { ease:FlxEase.backOut });
		FlxTween.tween(sprite, { y:FlxG.height, alpha: 0 }, .25, { ease:FlxEase.backIn, startDelay:3, onComplete:(_)->visible = false });
	}
	
	override function destroy() {
		super.destroy();
		
		Inputs.onInputChange.remove(show);
	}
}