package data;

import flixel.util.FlxSignal;
import utils.MultiCallback;
import haxe.PosInfos;
import flixel.FlxG;

import flixel.util.FlxSignal;
#if newgrounds
import io.newgrounds.NG;
import io.newgrounds.objects.Error;
#end

#if debug
enum TestScores {
	
	ForcedScores(?newgamePlusLevel:Int, scores:Array<Int>);
	RealScores;
}
#end

class Save {
	
	#if newgrounds
	public static var onSessionFail:FlxTypedSignal<Error->Void> = new FlxTypedSignal();
	#end
	
	static public var newGamePlusLevel(default, null) = 0;
	static public var allLevelsUnlocked(default, null) = false;
	static public var endlessScore(default, null) = 0;
	static public var onDataClear(default, null) = new FlxSignal();
	static var scores:Array<Int>;
	static var seenScores:Array<Int>;
	static var numLevelsComplete = 0;
	
	inline static var LOG = 
		// #if debug true #else false #end;
		false;
	
	inline static var UNLOCK_ALL_LEVELS = 
		// #if debug true #else false #end;
		false;
	
	inline static var CLEAR_DATA = 
		// #if debug true #else false #end;
		false;
	
	#if debug
	static var testScores:TestScores
		// = ForcedScores(0, [8 ,8 ,8 ,8 ,8 ,8 ,8 ,8 ,8 ,8 ,8 ,8 ]);
		// = ForcedScores(0, [8 ,8 ,8 ,8 ,8 ,8 ,8 ,8 ,8 ,8 ,8 ,0 ]);
		// = ForcedScores(0, [8 ,8 ,8 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ]);
		// = ForcedScores(0, [8 ,8 ,8 ,8 ,8 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ]);
		// = ForcedScores(0, [0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ]);
		// = ForcedScores(1, [12,12,12,12,12,12,12,12,12,12,12,12]);
		// = ForcedScores(1, [12,12,12,12,12,12,12,12,12,12,12,0 ]);
		// = ForcedScores(1, [12,12,12,12,12,0 ,0 ,0 ,0 ,0 ,0 ,0 ]);
		// = ForcedScores(1, [0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ]);
		= RealScores;
	#end
	
	static public function init():Void {
		
		if (scores != null)
			return;
		
		var prevSessionId:Null<String> = null;
		FlxG.save.bind("Flesh2Stone", "Sid'N'Mofface");
		if (FlxG.save.data.scores == null || CLEAR_DATA) {
			
			log(CLEAR_DATA ? "data cleared" : "no save found");
			clearData();
			
		} else {
			
			scores = FlxG.save.data.scores;
			if (FlxG.save.data.endlessScore != null)
				endlessScore = FlxG.save.data.endlessScore;
			prevSessionId = FlxG.save.data.prevSessionId;
			newGamePlusLevel = FlxG.save.data.newGamePlusLevel;
			FlxG.sound.muted = FlxG.save.data.mute;
			allLevelsUnlocked = FlxG.save.data.allLevelsUnlocked == true;
			log ( 'scores loaded ${scores.join(" ")}'
				+ '\nendless:$endlessScore'
				+ '\nsessionId:$prevSessionId'
				);
		}
		
		#if debug
		switch (testScores) {
			
			case RealScores:
			case ForcedScores(ngp, debugScores):
				newGamePlusLevel = ngp != null ? ngp : 0;
				scores = debugScores;
		}
		#end
		
		seenScores = scores.copy();
		
		#if newgrounds
		NG.createAndCheckSession(ApiData.id, prevSessionId,
			function inInvalidSession(e) {
				
				log("invalid session id, deleting from save data");
				FlxG.save.data.prevSessionId = null;
				FlxG.save.flush();
				onSessionFail.dispatch(e);
			}
		);
		NG.core.verbose = LOG;
		NG.core.initEncryption(ApiData.encryption);
		
		NG.core.onLogin.addOnce(function () {
			var onRequestComplete = new MultiCallback(checkLevelsComplete);
			var antiInstaCall = onRequestComplete.add();
			NG.core.requestScoreBoards(onRequestComplete.add());
			NG.core.requestMedals(onRequestComplete.add());
			antiInstaCall();
		});
		
		NG.core.onLogin.add(function () {
			
			FlxG.save.data.prevSessionId = NG.core.sessionId;
			FlxG.save.flush();
		});
		#end
	}
	
	static public function updateSeenScores():Void
	{
		for (i in 0...scores.length)
			seenScores[i] = scores[i];
	}
	
	static public function startNewGamePlus():Void {
		
		scores = [];
		seenScores = [];
		newGamePlusLevel++;
		FlxG.save.data.scores = scores;
		FlxG.save.data.newGamePlusLevel = newGamePlusLevel;
		FlxG.save.data.allLevelsUnlocked = false;
		allLevelsUnlocked = false;
		FlxG.save.flush();
	}
	
	static public function clearData():Void {
		
		scores = [];
		seenScores = [];
		FlxG.save.data.scores = scores;
		FlxG.save.data.endlessScore = 0;
		FlxG.save.data.newGamePlusLevel = 0;
		FlxG.save.data.allLevelsUnlocked = false;
		allLevelsUnlocked = false;
		FlxG.save.flush();
		
		onDataClear.dispatch();
	}
	
	static public function unlockAllLevels():Void {
		
		allLevelsUnlocked = true;
		FlxG.save.data.allLevelsUnlocked = allLevelsUnlocked;
		FlxG.save.flush();
	}
	
	static public function postScore(level:Int, score:Int):Void {
		
		if (level < 0 || level > Levels.LEVELS)
			throw 'invalid level: $level';
		
		while(scores.length <= level)
			scores.push(0);
		
		if (scores[level] < score) {
			
			scores[level] = score;
			FlxG.save.data.scores = scores;
		}
		
		checkLevelsComplete();
		
		FlxG.save.flush();
		log('saved score:$score level:$level, levelsComplete:$numLevelsComplete');
	}
	
	static public function postEndlessScore(score:Int):Void {
		
		if (endlessScore < score) {
			
			#if newgrounds
			if (endlessScore < 24 && score >= 24)
				NG.core.medals.get(ApiData.medal_endless24).sendUnlock();
			#end
			endlessScore = score;
			FlxG.save.data.endlessScore = endlessScore;
			FlxG.save.flush();
			log('saved score:$score level:endless');
		}
		// Always post suvival scores, even if it's not a new high
		#if newgrounds
		if (NG.core.loggedIn)
			NG.core.scoreBoards.get(ApiData.scoreboard_endless).postScore(score);
		#end
	}
	
	static public function checkLevelsComplete():Void {
		
		var localNumLevelsComplete = getUnseenCompletedLevelCount();
		if (numLevelsComplete < localNumLevelsComplete) {
			
			numLevelsComplete = localNumLevelsComplete;
			#if newgrounds
			if (NG.core.loggedIn)
				NG.core.scoreBoards.get(ApiData.scoreboard_levels).postScore(numLevelsComplete);
			else
				log("no score posted, not logged in");
			
			function unlockMedal(id:Int)
			{
				log('unlocking medal:$id');
				NG.core.medals.get(id).sendUnlock();
			}
			
			switch(numLevelsComplete)
			{
				case 6 : unlockMedal(ApiData.medal_story6 );
				case 12: unlockMedal(ApiData.medal_story12);
				case 18: unlockMedal(ApiData.medal_story18);
				case 24: unlockMedal(ApiData.medal_story24);
			}
			#end
		} else 
			log('no new levels completed local:$localNumLevelsComplete remote:$numLevelsComplete');
	}
	
	static public function getScore(level:Int):Int {
		
		if (scores == null || scores.length <= level)
			return 0;
		
		return scores[level];
	}
	
	static public function getIsUnlocked(level:Int):Bool {
		
		return level < Levels.LEVELS
			&& (allLevelsUnlocked || level == 0 || getScore(level - 1) >= Levels.unlockScoreGoal);
	}
	
	static public function getUnlockedLevelCount():Int {
		
		if (allLevelsUnlocked)
			return Levels.LEVELS;
		
		var count = 0;
		while(count < scores.length)
		{
			if (!getIsUnlocked(count))
				break;
			count++;
		}
		return count;
	}
	
	static public function getCompletedLevelCount():Int {
		
		var count = newGamePlusLevel * Levels.LEVELS;
		for (score in seenScores) {
			
			if (score >= Levels.scoreGoal)
				count++;
		}
		return count;
	}
	
	static public function getUnseenCompletedLevelCount():Int {
		
		var count = newGamePlusLevel * Levels.LEVELS;
		for (score in scores) {
			
			if (score >= Levels.scoreGoal)
				count++;
		}
		return count;
	}
	
	static public function getSeenScore(level:Int):Int {
		
		if (seenScores.length <= level)
			return 0;
		
		return seenScores[level];
	}
	
	static public function getIsSeenUnlocked(level:Int):Bool {
		
		return level < Levels.LEVELS
			&& (allLevelsUnlocked || level == 0 || getSeenScore(level - 1) >= Levels.unlockScoreGoal);
	}
	
	static public function getSeenUnlockedLevelCount():Int {
		
		var count = 0;
		while(count < seenScores.length)
		{
			if (!getIsSeenUnlocked(count))
				break;
			count++;
		}
		return count;
	}
	
	static public function getSeenCompletedLevelCount():Int {
		
		var count = newGamePlusLevel * Levels.LEVELS;
		for (score in seenScores) {
			
			if (score >= Levels.scoreGoal)
				count++;
		}
		return count;
	}
	
	inline static function log(msg:String, ?posInfos:PosInfos):Void {
		
		if (LOG)
			haxe.Log.trace(msg, posInfos);
	}
}