package entities;

import utils.Sounds;
import vfx.SmokeParticle;

import flixel.FlxG;
import flixel.FlxSprite;
import flixel.group.FlxGroup;
import flixel.group.FlxSpriteGroup;

class SkyLaser extends FlxSpriteGroup
{
	inline static var SMOKE_SPAWNS_PER_SECOND = 60;
	inline static var SMOKE_SPAWN_RATE = 1 / SMOKE_SPAWNS_PER_SECOND;
	
	public var warning(default, null):SkyLaserWarning;
	private var beam:SkyLaserBeam;
	
	private var halfWidth:Float = 0;
	private var halfHeight:Float = 0;

	private var smokeParticles:FlxTypedGroup<SmokeParticle>;
	private var smokeTimer:Float = 0;
	
	override public function new(?X:Float=0, ?Y:Float=0) 
	{
		super(X, Y);
		
		warning = new SkyLaserWarning(finishedWarning);
		beam = new SkyLaserBeam(kill);
		add(beam);
	}
	
	public function spawn(warningsGroup:FlxTypedGroup<FlxSprite>, smokes:FlxTypedGroup<SmokeParticle>):Void
	{
		reset(FlxG.random.bool() ? 80 : 16, 36);
		warning.animation.play("start", true, false, 0);
		warningsGroup.add(warning);
		warning.x = x; warning.y = y;
		warning.exists = true;
		beam.exists = false;
		smokeParticles = smokes;
		Sounds.play(SKY_LASER_WARN);
		smokeTimer = 0;
	}

	public function finishedWarning():Void
	{
		warning.exists = false;
		beam.exists = true;
		beam.animation.play("start", true, false, 0);
		Sounds.play(SKY_LASER);
	}

	override public function update(elapsed:Float):Void
	{
		super.update(elapsed);

		if (beam.exists && beam.animation.name == "active")
		{
			smokeTimer += elapsed;
			if (smokeTimer > SMOKE_SPAWN_RATE)
			{
				smokeTimer -= SMOKE_SPAWN_RATE;
				smokeParticles.recycle(SmokeParticle).spawn
					( x + (FlxG.random.float() * beam.frameWidth)
					, y + (FlxG.random.float() * (beam.frameHeight-24))
					, FlxG.random.float() * 360
					, FlxG.random.float() * 2 - 1
					, FlxG.random.float() * 2 - 1
					);
			}
		}
	}
	
	public function exit():Void
	{
		if (warning.exists)
			warning.exit(kill);
		else 
			beam.animation.play("end");
	}
}

class SkyLaserWarning extends FlxSprite
{
	public function new(callback:Void->Void) 
	{
		super(0.0, 0.0);
		
		exists = false;
		solid = false;
		loadGraphic("assets/images/fx/laser-warning.png", true, 63, 103);
		animation.add("start", [0,1,2,3,4,4,4,5,6,7,8,9,10,11,12,11,12,11,12,11,12,11,12,11,12,11,12,11,12], 10, false);
		animation.finishCallback = function(anim:String) {
			if(anim == "start")
			{
				callback();
			}
		};
	}
	
	public function exit(callback:()->Void):Void
	{
		var frame = animation.curAnim.curFrame;
		if (frame > 11) frame = 11;
		animation.play("start", true, true, frame);
		animation.finishCallback = function(anim:String) { callback(); };
	}
}

class SkyLaserBeam extends FlxSprite
{
	private var ACTIVE_LOOP_COUNT:Int = 5;
	private var currentLoopCount:Int = 0;

	public function new(callback:Void->Void) 
	{
		super(0, -36);
		
		exists = false;
		solid = false;
		offset.y = -y;
		loadGraphic("assets/images/enemies/laser-beam.png", true, 64, 128);
		animation.add("start", [0, 0, 1, 1, 2], 20, false);
		animation.add("active", [3, 4, 5, 6], 20, false);
		animation.add("end", [2, 1, 1, 0], 20, false);
		animation.finishCallback = function(anim:String) {
			if(anim == "start")
			{
				solid = true;
				animation.play("active", true, false, 0);
				currentLoopCount = 0;
			}
			if(anim == "active")
			{
				if (currentLoopCount >= ACTIVE_LOOP_COUNT - 1)
				{
					solid = false;
					animation.play("end", true, false, 0);
				}
				else
				{
					currentLoopCount++;
					animation.play("active", true, false, 0);
				}
			}
			if(anim == "end")
			{
				exists = false;
				callback();
			}
	    };
	}
}