package ui;

import ui.Button.ButtonSize;
import ui.Button.ButtonType;
import ui.MusicButton.MusicButtonGroup;
import ui.BitmapText.NokiaText;
import utils.Sounds;

import openfl.Lib;
import openfl.net.URLRequest;

import flixel.FlxG;
import flixel.FlxBasic;
import flixel.group.FlxGroup.FlxTypedGroup;
import flixel.tweens.FlxEase;
import flixel.tweens.FlxTween;

import ui.ButtonGroup.ITransitionable;

enum User
{
	Twitter   (alias:String, ?name:String);
	Newgrounds(alias:String, ?name:String, ?song:Int);
	Instagram (alias:String, ?name:String);
	SoundCloud(alias:String, ?name:String, ?song:String);
}

@:forward
abstract CreditsButtons(FlxBasic) to FlxBasic
{
	var thisAsButtons(get, never):ITransitionable;
	inline function get_thisAsButtons() return cast this;
	
	inline public function new(onExit:()->Void)
	{
		#if FLX_TOUCH
		if (FlxG.onMobile)
			@:privateAccess
			this = new CreditsMobileButtons(onExit);
		else
		#end
			@:privateAccess
			this = new CreditsDesktopButtons(onExit);
	}
	
	inline public function startIntro(delay = 0.0, ?callback) { thisAsButtons.startIntro(delay, callback); }
	inline public function startOutro(delay = 0.0, ?callback) { thisAsButtons.startOutro(delay, callback); }
}


#if FLX_TOUCH
class CreditsMobileButtons extends FlxTypedGroup<FlxBasic> implements ITransitionable
{
	function new(onExit:()->Void)
	{
		super(0);
		active = false;
		visible = false;
		
		// MADE BY
		addHeader(1, "MADE BY");
		addLinkButton(16, 11, Twitter("GeoKureli"), Medium);
		addLinkButton(84, 11, Newgrounds("BigDumps"), Medium);
		
		// MUSIC
		addHeader(30, "MUSIC");
		var musicButtons = new MusicButtonGroup(5);
		add(musicButtons);
		var buttonY = 40;
		function addMusicButton(user:User, title, song)
		{
			musicButtons.create(32, buttonY, Link.getAlias(user), title, song, Link.getUrl(user));
			buttonY += 17;
		}
		addMusicButton(Newgrounds("SuperBastard"          , 825461              ), "Waste Time Online" , "WasteTimeOnline"      );
		addMusicButton(SoundCloud("BigDumps", "tjmolinski", "turning-into-stone"), "Flesh to Stohn"    , "ldsong"               );
		addMusicButton(Newgrounds("NinjaMuffin99"         , 831234              ), "Wet Pixel Dreams"  , "Wet-Pixel-Dreams_slim");
		addMusicButton(Newgrounds("Kreyowitz"             , 729803              ), "Bridging the Gap"  , "BridgingtheGap_slim"  );
		addMusicButton(Newgrounds("Ef-X"                  , 382454              ), "The Chase"         , "MySong3_slim"         );
		
		// COVER ART
		addHeader(125, "COVER ART");
		addLinkButton(40, 134, Instagram("Imrie", "CreatedByImrie"));
		
		addButton(4, 134, Icon("back"), 
			function ()
			{
				musicButtons.stopAll();
				onExit();
			}
		);
	}
	
	inline function addHeader(y:Float, text:String):BitmapText
	{
		var header = new BitmapText(0, y, text);
		header.centerXOnStage();
		add(header);
		return header;
	}
	
	inline function addButton(x:Float, y:Float, type:ButtonType, ?onClick:()->Void):Void
	{
		add(new Button(x, y, type, onClick));
	}
	
	inline function addLinkButton(x:Float, y:Float, user:User, size:ButtonSize = Big):Void
	{
		addButton(x, y, Text(Link.getAlias(user), size, NokiaText.font), Link.to.bind(user));
	}
	
	public function startIntro(delay = 0.0, ?callback:()->Void):Void
	{
		visible = true;
		
		function onComplete()
		{
			active = true;
			
			if (callback != null)
				callback();
		}
		
		for (i in  0...members.length)
		{
			final onIntroComplete = i == members.length - 1 ? onComplete : null;
			if (Std.is(members[i], BitmapText))
				tweenBitmapText(cast members[i], true, delay, onIntroComplete);
			else
				cast (members[i], ITransitionable).startIntro(delay, onIntroComplete);
			
			delay += 0.125;
		}
	}
	
	public function startOutro(delay = 0.0, ?callback:()->Void):Void
	{
		active = false;
		
		for (i in 0...members.length)
		{
			final onOutroComplete = i == members.length - 1 ? callback : null;
			if (Std.is(members[i], BitmapText))
				tweenBitmapText(cast members[i], false, delay, onOutroComplete);
			else
				cast (members[i], ITransitionable).startOutro(delay, onOutroComplete);
			
			delay += 0.125;
		}
	}
	
	inline function tweenBitmapText(text:BitmapText, isIntro:Bool, delay:Float, duration = 0.2, ?onComplete:()->Void):Void
	{
		var options:TweenOptions = { ease:FlxEase.backIn, startDelay:delay };
		if (onComplete != null)
			options.onComplete = (_)->onComplete();
		
		var val = 0;
		if (isIntro)
		{
			text.scale.set(0, 0);
			val = 1;
		}
		FlxTween.tween(text.scale, { x:val, y:val }, duration, options);
	}
}
#end

class CreditsDesktopButtons extends ButtonGroup
{
	var songs = new Map<Int, String>();
	function new(onExit:()->Void)
	{
		super(0);
		
		final topY = 3;
		var buttonY = topY + 10;
		var button:BitmapText;
		
		inline function addLinkButton(user:User, ?song:String)
		{
			if (song != null)
				songs[members.length] = song;
			
			button = new NokiaText(0, buttonY, Link.getAlias(user));
			addButton(button, Link.to.bind(user));
			button.x = -button.width - 2;
			buttonY += 10;
		}
		
		inline function addHeader(text:String, ?y:Int)
		{
			button = addNewButton(0, y != null ? y : buttonY, text, null);
			disableButon(button);
			button.useDefaultBorder();
			button.color = 0xFFffffff;
			button.x = -button.width - 2;
			if (y == null)
				buttonY += 10;
		}
		
		addLinkButton(Twitter   ("GeoKureli"));
		addLinkButton(Newgrounds("BigDumps"));
		addHeader("MADE BY", topY); //Can't draw this first because it will be selected by default
		buttonY += 6;
		addHeader("MUSIC");
		addLinkButton(Newgrounds("SuperBastard"          , 825461              ), "WasteTimeOnline");
		addLinkButton(SoundCloud("BigDumps", "tjmolinski", "turning-into-stone"), "ldsong");
		addLinkButton(Newgrounds("NinjaMuffin99"         , 831234              ), "Wet-Pixel-Dreams_slim");
		addLinkButton(Newgrounds("Kreyowitz"             , 729803              ), "BridgingtheGap_slim");
		addLinkButton(Newgrounds("Ef-X"                  , 382454              ), "MySong3_slim");
		buttonY += 6;
		addHeader("COVER ART");
		addLinkButton(Instagram ("Imrie", "CreatedByImrie"));
		buttonY += 6;
		button = addNewButton(0, buttonY, "MAIN MENU", onExit);
		button.x = -button.width;
		
		keysBack = BACK;
		onBack = onExit;
		
		active = false;
	}
	
	override function set_selected(value:Int):Int
	{
		if (songs.exists(selected) && FlxG.sound.music != null)
			FlxG.sound.music.stop();
		
		if (songs.exists(value))
			Sounds.playMusic(songs[value]);
		
		return super.set_selected(value);
	}
}

class Link
{
	inline static function openUrl(url:String):Void { Lib.getURL(new URLRequest(url), "_blank"); }
	
	static public function to(user:User)
	{
		openUrl(getUrl(user));
	}
	
	inline static public function getAlias(user:User):String
	{
		return switch user { case Twitter(alias) | Newgrounds(alias) | Instagram(alias) | SoundCloud(alias): alias; } 
	}
	
	inline static public function getUrl(user:User)
	{
		return switch(user)
		{
			case Twitter   (alias, null      ): 'https://twitter.com/$alias';
			case Twitter   (_    , name      ): 'https://twitter.com/$name';
			case Newgrounds(alias, null, null): 'http://$alias.newgrounds.com';
			case Newgrounds(_    , name, null): 'http://$name.newgrounds.com';
			case Newgrounds(_    , _   , song): 'https://newgrounds.com/audio/listen/$song';
			case Instagram (alias, null      ): 'https://instagram.com/$alias/';
			case Instagram (_    , name      ): 'https://instagram.com/$name/';
			case SoundCloud(alias, null, null): 'https://soundcloud.com/$alias/';
			case SoundCloud(_    , name, null): 'https://soundcloud.com/$name/';
			case SoundCloud(_    , name, song): 'https://soundcloud.com/$name/$song/';
		}
	}
}
