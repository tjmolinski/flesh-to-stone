package states;

import data.Tilemap;
import data.WaterMap;
import entities.Enemy;
import entities.PBJMouse;
import states.PlayState;
import ui.BitmapText;
import ui.TextSequence;
import utils.Sounds;
import vfx.LightningShader;
import vfx.PixelPerfectSmoke;
import vfx.Warning;

import flixel.FlxCamera;
import flixel.FlxG;
import flixel.effects.particles.FlxEmitter;
import flixel.effects.particles.FlxParticle;
import flixel.system.FlxAssets.FlxShader;
import flixel.util.FlxTimer;
import flixel.util.FlxColor;

import openfl.filters.ShaderFilter;

import lycan.effects.Lightning;

/**
 * Just separating out the outro logic so it's all in one spot
 *  PlayState is fucking huge
 */

enum SuccessOutro
{
	Normal;
	Cool;
	Epic;
	Final;
}

@:access(states.PlayState)
@:forward
abstract PlayStateSuccessOutros(PlayState) from PlayState to PlayState
{
	var pbjMouse(get, never):PBJMouse; function get_pbjMouse() return this.pbjMouse;
	
	public function playOutro(type:SuccessOutro, onComplete:()->Void):Void
	{
		pbjMouse.active = false;
		
		function showSuccessAndEnd()
		{
			var success = new BitmapText(0, 0, "SUCCESS");
			success.scale.scale(2);
			success.centerXOnStage();
			success.centerYOnStage();
			success.text = "";
			this.add(success);
			final letterTime = 0.2;
			final loops = 7;
			new FlxTimer().start
				( letterTime
				,   (timer)->
					{
						success.text = "SUCCESS".substr(0, timer.elapsedLoops);
						FlxG.cameras.shake(0.025, letterTime / 2);
					}
				, loops
				);
			new FlxTimer().start(letterTime * loops + .5, (_)->onComplete());
		}
		
		// playSimpleOutro(showSuccessAndEnd); // debug
		// playCoolOutro  (showSuccessAndEnd); // debug
		// playEpicOutro  (showSuccessAndEnd); // debug
		// playFinalOutro (onComplete       ); // debug
		switch(type)
		{
			case Normal: playSimpleOutro(showSuccessAndEnd);
			case Cool  : playCoolOutro  (showSuccessAndEnd);
			case Epic  : playEpicOutro  (showSuccessAndEnd);
			case Final : playFinalOutro (onComplete       );
		}
	}
	
	inline function exitEnemies():Void
	{
		this.homers.stop();
		this.beams.stop();
		this.floaters.stop();
		this.seekers.stop();
		this.latchers.stop();
		this.lungers.stop();
		this.shooters.stop();
		this.skyLasers.stop();
		function exitWithAnim(enemy:Enemy) enemy.exit(this.warnings.recycle(Warning));
		function exit(enemy:Enemy) enemy.exit(this.warnings.recycle(Warning));
		this.seekers .forEachAlive(exitWithAnim);
		this.homers  .forEachAlive(exitWithAnim);
		this.lungers .forEachAlive(exitWithAnim);
		this.latchers.forEachAlive(exit);
		this.floaters.forEachAlive(exit);
		this.shooters.forEachAlive((shooter)->shooter.exit(this.warnings));
		this.beams.forEachAlive((beam)->beam.exit());
		this.skyLasers.forEachAlive((laser)->laser.exit());
	}
	
	inline function setupShaderCam(?shader:FlxShader):FlxCamera
	{
		if (shader == null)
			shader = new LightningShader();
		
		FlxCamera.defaultCameras = [FlxG.camera];
		var shaderCam:FlxCamera = new FlxCamera();
		shaderCam.setFilters([new ShaderFilter(shader)]);
		shaderCam.bgColor = 0;
		return FlxG.cameras.add(shaderCam);
	}
	
	function playSimpleOutro(onOutroComplete:()->Void):Void
	{
		pbjMouse.blink(1.0, ()->pbjMouse.showOutroRiseAndExit(onOutroComplete));
	}
	
	function playCoolOutro(onOutroComplete:()->Void):Void
	{
		pbjMouse.blink(1.0, ()->pbjMouse.showOutroRise(exitEnemies));
		new FlxTimer().start(3.0, (_)->pbjMouse.showOutroExit(onOutroComplete));
	}
	
	function playEpicOutro(onOutroComplete:()->Void):Void
	{
		var shaderCam = setupShaderCam();
		
		pbjMouse.blink(1.0, ()->pbjMouse.showOutroRise(lightningStrike.bind(0.25, shaderCam, 0)));
		new FlxTimer().start(2.35, (_)->lightningStrike(0.25, shaderCam, 1));
		new FlxTimer().start(2.70,
			function(_)
			{
				exitEnemies();
				heroLightning(0.75, shaderCam);
			}
		);
		new FlxTimer().start(3.70,
			function (_)
			{
				FlxG.cameras.remove(shaderCam);
				pbjMouse.showOutroExit();
			}
		);
		new FlxTimer().start(4.7, (_)->onOutroComplete());
	}
	
	public function playFinalOutro(onOutroComplete:()->Void):Void
	{
		var shaderCam = setupShaderCam(new PixelPerfectSmoke());
		
		pbjMouse.showOutroRise(true, exitEnemies);
		FlxG.sound.music.fadeOut();
		
		var delay = 2.0;
		final intakeDuration = 3.25;
		new FlxTimer().start(delay, 
			function(_)
			{
				Sounds.play(END_CHARGE);
				pbjMouse.blink(intakeDuration);
				energyIntake(intakeDuration, shaderCam);
			}
		);
		delay += intakeDuration;
		delay += 0.25;//wait
		final burstDuration = 3.0;
		new FlxTimer().start(delay, 
			function(_)
			{
				Sounds.play(EXPLOSION);
				energyBurst(burstDuration, shaderCam);
			}
		);
		delay += burstDuration;
		new FlxTimer().start(delay, 
			function (_)
			{
				FlxG.cameras.remove(shaderCam);
				startFinalPan(onOutroComplete);
			}
		);
	}
	
	function startFinalPan(callback:()->Void):Void
	{
		var water = new WaterMap(Tilemap.currentTiles, 21);
		water.y = FlxG.height;
		water.enableForeverScroll();
		this.add(water);
		this.members.remove(water);
		@:privateAccess
		this.members.insert(this.members.indexOf(this.map) + 1, water);
		
		pbjMouse.active = true;
		pbjMouse.solid = false;
		pbjMouse.acceleration.y = 50;
		FlxG.camera.maxScrollX = FlxG.width;
		FlxG.camera.minScrollX = 0;
		FlxG.camera.minScrollY = 0;
		FlxG.camera.follow(pbjMouse);
		var text = new TextSequence();
		
		function endFinalIntro():Void
		{
			this.members.remove(water);
			this.members.push(water);
			this.members.remove(pbjMouse);
			this.members.push(pbjMouse);
			this.members.remove(text);
			this.members.push(text);
			water.y -= FlxG.camera.scroll.y;
			pbjMouse.y -= FlxG.camera.scroll.y;
			FlxG.camera.target = null;
			FlxG.camera.scroll.y = 0;
			FlxG.camera.maxScrollX = null;
			text.text.scrollFactor.set();
			new FlxTimer().start(3, (_)->callback());
		}
		
		new FlxTimer().start(3, 
			function (_)
			{
				Sounds.playMusic("ur-anime-gf-just-got-killed");
				this.add(text);
				text.show
					(   [ "YOU SURPRISED EVEN ME"
						, "NO BEAST CAN BEST YOU"
						, ""
						, "THANKS FOR PLAYING"
						]
					, endFinalIntro
					, false // allowSkip
					, false // allowScroll
					);
			}
		);
	}
	
	inline function lightningStrike(duration:Float, ?camera:FlxCamera, sound:Int):Void
	{
		Sounds.play(DEATH, sound);
		pbjMouse.blink(duration);
		var zone = Lightning.to(duration, pbjMouse, this, FlxG.cameras.flash.bind(FlxColor.WHITE, 0.1));
		if (camera != null)
			zone.cameras = [camera];
	}
	
	inline function heroLightning(duration:Float, ?camera:FlxCamera):Void
	{
		Sounds.play(BEAM, 2);
		pbjMouse.blink(duration);
		var zone = Lightning.from(duration, pbjMouse, this);
		if (camera != null)
			zone.cameras = [camera];
	}
	
	inline function energyIntake(duration:Float, amount = 200, ?camera:FlxCamera):Void
	{
		var emitter = new FlxTypedEmitter<Particle>
			( pbjMouse.x + pbjMouse.width / 2
			, pbjMouse.y + pbjMouse.height / 2
			//, amount
			);
		emitter.launchMode = CIRCLE;
		emitter.particleClass = Particle;
		emitter.lifespan.set(1.0, duration);
		emitter.speed.set(100, 200);
		emitter.makeParticles(6, 6, FlxColor.WHITE, amount);
		emitter.start();
		this.add(emitter);
		
		if (camera != null)
			emitter.cameras = [camera];
		
		final steps = 4;
		final stepTime = duration / steps;
		
		new FlxTimer().start(stepTime, (timer)->FlxG.cameras.shake(timer.elapsedLoops * 0.015, stepTime), steps);
		
		new FlxTimer().start(duration, (_)->{ emitter.kill(); this.remove(emitter); });
	}
	
	
	inline function energyBurst(duration:Float, amount = 200, ?camera:FlxCamera):Void
	{
		var emitter = new FlxEmitter
			( pbjMouse.x + pbjMouse.width / 2
			, pbjMouse.y + pbjMouse.height / 2
			//, amount
			);
		emitter.launchMode = CIRCLE;
		emitter.lifespan.set(duration);
		emitter.speed.set(100);
		emitter.makeParticles(6, 6, FlxColor.WHITE, amount);
		emitter.start();
		this.add(emitter);
		Sounds.play(DEATH);
		
		if (camera != null)
			emitter.cameras = [camera];
		
		new FlxTimer().start(duration, (_)->{ emitter.kill(); this.remove(emitter); });
	}
}

class Particle extends FlxParticle
{
	static var DISTANCE = Math.sqrt(FlxG.width * FlxG.height * 2);
	
	override function onEmit()
	{
		super.onEmit();
		
		// makeGraphic(4, 4);
		
		x -= velocity.x * lifespan;
		y -= velocity.y * lifespan;
	}
}