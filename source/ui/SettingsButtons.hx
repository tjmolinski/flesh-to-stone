package ui;

import flixel.FlxBasic;
import flixel.FlxG;

import data.Save;
import ui.ButtonGroup.ITransitionable;

@:forward
abstract SettingsButtons(FlxBasic) to FlxBasic
{
	var thisAsButtons(get, never):ITransitionable;
	inline function get_thisAsButtons() return cast this;
	
	inline public function new(onExit:()->Void)
	{
		#if FLX_TOUCH
		if (FlxG.onMobile)
			@:privateAccess
			this = new SettingsMobileButtons(onExit);
		else
		#end
			@:privateAccess
			this = new SettingsDesktopButtons(onExit);
	}
	
	inline public function startIntro(?callback:() -> Void):Void { thisAsButtons.startIntro(callback); }
	inline public function startOutro(?callback:() -> Void):Void { thisAsButtons.startOutro(callback); }
	
	static public function prompt(buttons:FlxBasic, onConfirm:()->Void, mouse:Bool):Void
	{
		var prompt = new Prompt(mouse);
		var parent = FlxG.state;
		parent.add(prompt);
		buttons.active = false;
		prompt.setup(
			"ARE YOU SURE?",
			onConfirm,
			null,
			function () {
				
				parent.remove(prompt);
				buttons.active = true;
			}
		);
	}
}

#if FLX_TOUCH
class SettingsMobileButtons extends MobileButtonGroup
{
	function new(onExit:()->Void)
	{
		super(0);
		
		final buttonX = 40;
		var buttonY = 84;
		addTextButton(buttonX, buttonY, "UNLOCK ALL",
			SettingsButtons.prompt.bind(this, Save.unlockAllLevels, true));
		buttonY += 21;
		addTextButton(buttonX, buttonY, "CLEAR SAVE",
			SettingsButtons.prompt.bind(this, Save.clearData, true));
		buttonY += 21;
		addTextButton(buttonX, buttonY, "MAIN MENU", onExit);
	}
}
#end

class SettingsDesktopButtons extends ButtonGroup
{
	function new(onExit:()->Void)
	{
		super(0);
		
		var button;
		var buttonY = 92;
		button = addNewButton(0, buttonY, "[M]UTE/UNMUTE", FlxG.sound.toggleMuted);
		button.x = -button.width;
		buttonY += 10;
		button = addNewButton(0, buttonY, "UNLOCK ALL LEVELS",
			SettingsButtons.prompt.bind(this, Save.unlockAllLevels, false));
		button.x = -button.width;
		buttonY += 10;
		button = addNewButton(0, buttonY, "CLEAR SAVE DATA",
			SettingsButtons.prompt.bind(this, Save.clearData, false));
		button.x = -button.width;
		buttonY += 10;
		button = addNewButton(0, buttonY, "MAIN MENU", onExit);
		button.x = -button.width;
		
		keysBack = BACK;
		onBack = onExit;
		
		active = false;
	}
}