package entities;

import vfx.Warning;
import utils.Sounds;
import flixel.effects.FlxFlicker;
import flixel.FlxG;
import flixel.math.FlxVector;
import flixel.math.FlxPoint;

 enum LatcherStates {
	CHASING;
	LATCHED;
	DYING;
 }

class Latcher extends Enemy {
	
	inline static var SPEED = 50;
	inline static var KILL_SPEED = 100;
	inline static var LATCH_DISTANCE = 6;
	inline static var LATCH_RUMBLE = 2;
	
	private var myTarget:PBJMouse;
	private var halfWidth:Float = 0.0;
	private var halfHeight:Float = 0.0;
	private var targetOffset:FlxVector;

	private var _currentState:LatcherStates;
	
	public function new(x = 0.0, y = 0.0) {
		super(x, y);
		
		loadGraphic("assets/images/enemies/latcher.png", true, 4, 4);
		animation.add("chase", [0, 1, 2, 3], 10, true);
		animation.play("chase", true, false, -1);
		halfWidth = width / 2;
		halfHeight = height / 2;
		maxVelocity.set(100, 100);
	}
	
	public function init(_target: PBJMouse):Void {
		
		setEdgePosition();
		myTarget = _target;
		solid = true;
		_currentState = CHASING;
	}
	
	override function update(elapsed:Float) {
		super.update(elapsed);
		
		switch(_currentState)
		{
			case CHASING:
				handleChasing(elapsed);
			case LATCHED:
				handleLatched(elapsed);
			case DYING:
				handleDying(elapsed);
		}
	}
	
	private function handleChasing(elapsed: Float):Void
	{
		var vel = getDistance();
		vel.length = SPEED;
        velocity.copyFrom(vel);
		angle = Math.round(vel.degrees / 90) * 90;
	}

	private function handleLatched(elapsed: Float):Void
	{
		velocity.set(0, 0);
		x = myTarget.x + myTarget.halfWidth - halfWidth + targetOffset.x * LATCH_DISTANCE * scale.x + FlxG.random.float(-LATCH_RUMBLE, LATCH_RUMBLE);
		y = myTarget.y + myTarget.halfHeight - halfHeight + targetOffset.y * LATCH_DISTANCE * scale.y + FlxG.random.float(-LATCH_RUMBLE, LATCH_RUMBLE);
	}
	
	private function handleDying(elapsed:Float):Void
	{
		if (!isOnScreen())
			kill();
	}
	
    public function chase():Void
    {
        animation.play("chase", true, false, -1);
		_currentState = CHASING;
    }

	public function latched():Void
	{
		Sounds.play(LATCH);
		targetOffset = getDistance().normalize().negate();
		animation.play("chase", true, false, -1);
		_currentState = LATCHED;
		solid = false;
	}
	
	override function exit(?sprite:Warning, ?callback:() -> Void)
	{
		latched();
		murder();
	}
	
	override function murder():Void
	{
		if (_currentState == LATCHED)
		{
			velocity.copyFrom(targetOffset).scale(KILL_SPEED);
			_currentState = DYING;
			var vec = FlxVector.get();
			vec.copyFrom(velocity);
			angle = Math.round(vec.degrees / 90) * 90;
			vec.put();
		}
		else
			kill();
	}
	
	function getDistance():FlxVector
	{
		return FlxVector.get(
			(myTarget.x + myTarget.halfWidth) - (x + halfWidth),
			(myTarget.y + myTarget.halfHeight) - (y + halfHeight)
		);
	}
}