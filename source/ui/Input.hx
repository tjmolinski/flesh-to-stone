package ui;

enum Input {
	
	ACCEPT;
	BACK;
	UP;
	DOWN;
	LEFT;
	RIGHT;
	STONE;
	PAUSE;
}