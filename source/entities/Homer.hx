package entities;

import flixel.math.FlxPoint;
import flixel.math.FlxVector;
import flixel.util.FlxColor;

import vfx.Warning;
import vfx.Explosion;
import vfx.SpriteUtil;

/**
 * ...
 * @author TJ
 */
class Homer extends Enemy
{
	static inline var SPEED = 40;
	static inline var SPEED_UP_TIME = 0.2;
	static inline var SLOW_DOWN_TIME = 0.5;
	private var myTarget:PBJMouse;
	private var halfWidth:Float = 0.0;
	private var halfHeight:Float = 0.0;

	private var direction:Float = 90.0;
	
	private var lifeTimeBuffer:Float = 0.0;
	private var lifeTime:Float = 5.0;
	private var explosion:Explosion;

	@:keep
	public function new()
	{
		super();
		// loadGraphic("assets/images/enemies/homing-missle.png", true, 12, 12);
		SpriteUtil.loadAnimationWithSpaces(this, "assets/images/enemies/homing-missle.png", FlxPoint.get(12, 12));
		animation.add("side_run", [0, 1, 2, 3], 10, true);
		animation.add("up_run", [4, 5, 6, 7], 10, true);
		animation.add("down_run", [8, 9, 10, 11], 10, true);
		animation.play("side_run", true, false, -1);
		
		width  -= 4;
		height -= 4;
		centerOffsets();
		offset.y++;
		halfWidth  = width  / 2;
		halfHeight = height / 2;
		
		maxVelocity.set(SPEED / SPEED_UP_TIME, SPEED / SPEED_UP_TIME);
		drag.set(SPEED / SLOW_DOWN_TIME, SPEED / SLOW_DOWN_TIME);
	}

	public function spawn(pos:FlxPoint, target:PBJMouse, explosion:Explosion, warning:Warning)
	{
		reset(pos.x, pos.y);
		// setEdgePosition();
		
		solid = true;
		myTarget = target;
		this.explosion = explosion;
		color = FlxColor.WHITE;
		lifeTime = 5.0;
		lifeTimeBuffer = 0.0;
		
		playWarningAnim(warning);
	}

	override public function update(elapsed:Float):Void
	{
		/*if (!alive)
		{
			exists = false;	
		}
		else
		{
			if (lifeTimeBuffer >= lifeTime)
			{
				murder();
			}
			else
			{
				lifeTimeBuffer += elapsed;
				
				if (lifeTimeBuffer > lifeTime - 1)
				{
					color = color == FlxColor.RED ? FlxColor.WHITE : FlxColor.RED;
				}
			}
		}*/
		super.update(elapsed);
		
		
		homeOnTarget();
	}
	
	private function homeOnTarget():Void
	{
		if (myTarget != null)
		{
			var vel = new FlxVector((myTarget.x + myTarget.halfWidth) - (x + halfWidth), (myTarget.y + myTarget.halfHeight) - (y + halfHeight));
			vel = vel.normalize();
			vel = vel.scale(SPEED);
			acceleration.set(vel.x, vel.y);	
			direction = Math.round(vel.degrees / 90) * 90;

			switch(direction) {
				case -180:
					animation.play("side_run", false, false, 0);
					flipX = true;
				case 180:
					animation.play("side_run", false, false, 0);
					flipX = true;
				case 0:
					animation.play("side_run", false, false, 0);
					flipX = false;
				case 90:
					animation.play("down_run", false, false, 0);
				case -90:
					animation.play("up_run", false, false, 0);
			}
		}
	}

	override function murder():Void
	{
		explosion.spawn(x + halfWidth, y + halfHeight);
		kill();
	}
}