package ui;

import openfl.events.TouchEvent;
import flixel.FlxG;
import flixel.math.FlxVector;

class RelativeTouchManager {
	
	inline static public var JOYSTICK_RADIUS = 24;
	inline static public var JOYSTICK_MAX_DRAG = 10;
	inline static public var ACTION_RADIUS = 10;
	inline static public var DIGITAL_DEAD_RANGE = 4;
	inline static public var ANALOG_DEAD_RANGE = 0.15;
	
	static public var action(get, never):Bool;
	inline static function get_action():Bool {
		return actionTouch != null;
	}
	static public var prevAction:Bool;
	
	static var actionPos   (default, null):FlxVector = new FlxVector(180, 110);
	static var anchorPos   (default, null):FlxVector = new FlxVector(-20, 110);
	
	static public var dir         (default, null):FlxVector = new FlxVector();
	static public var dirUnit     (default, null):FlxVector = new FlxVector();
	static public var prevDir     (default, null):FlxVector = new FlxVector();
	static public var anchor      (default, null):FlxVector = new FlxVector(-20, 110);
	
	static public var joystickTouch(default, null):Null<Touch>;
	static public var actionTouch(default, null):Null<Touch>;
	
	static public function update(elapsed:Float):Void {
		
		prevDir.copyFrom(dir);
		prevAction = action;
		
		for (touch in (FlxG.touches.list:Array<Touch>)) {
			
			if (touch.released && !touch.justReleased) {
				
				if (touch == joystickTouch) {
					
					joystickTouch = null;
					dir.set();
					dirUnit.set();
					
					anchor.x = anchorPos.x;
					anchor.y = anchorPos.y;
					
				} else if (touch == actionTouch)
					actionTouch = null;
				
			} else if (touch.justPressed && touch != joystickTouch && touch != actionTouch
			&& (joystickTouch == null || actionTouch == null)) {
				
				var dis = FlxVector.weak();
				inline function isWithin(touch:Touch, center:FlxVector, distance:Float):Bool
				{
					return dis.set(center.x - touch.x, center.y - touch.y).lengthSquared <= distance * distance;
				}
				
				if (joystickTouch == null && touch.x <= FlxG.width * 1 / 3)
				{
					joystickTouch = touch;
					anchor.set(touch.x, touch.y);
				}
				else if (actionTouch == null && touch.x >= FlxG.width * 2 / 3)
					actionTouch = touch;
			}
		}
		
		if (joystickTouch != null) {
			
			joystickTouch.getDistanceFrom(anchor, dir);
			if (dir.lengthSquared > JOYSTICK_MAX_DRAG * JOYSTICK_MAX_DRAG) {
				
				dir.normalize().scale(JOYSTICK_MAX_DRAG);
				anchor.set(joystickTouch.x - dir.x, joystickTouch.y - dir.y);
			}
			
			dirUnit.copyFrom(dir).scale(1 / JOYSTICK_MAX_DRAG); 
			if (Math.abs(dirUnit.x) < ANALOG_DEAD_RANGE)
				dirUnit.x = 0;
			if (Math.abs(dirUnit.y) < ANALOG_DEAD_RANGE)
				dirUnit.y = 0;
		}
	}
	
	static public function isAcceptEvent(e:TouchEvent):Bool {
		
		for (touch in (FlxG.touches.list:Array<ui.Touch>)) {
			
			if (touch.touchPointID == e.touchPointID && touch != joystickTouch)
				return e.type == TouchEvent.TOUCH_TAP
					|| (e.type == TouchEvent.TOUCH_MOVE && justPressed(ACCEPT))
					;
		}
		return false;
	}
	
	static public function pressed(input:Input):Bool {
		
		return switch(input) {
			case UP | DOWN | LEFT | RIGHT : checkDir(dir, input);
			case BACK | PAUSE : false;
			case STONE | ACCEPT: action;
		}
	}
	
	static public function justPressed(input:Input):Bool {
		
		return switch(input) {
			case UP | DOWN | LEFT | RIGHT:
				checkDir(dir, input) && !checkDir(prevDir, input);
			case BACK | PAUSE : false;
			case STONE | ACCEPT: action && !prevAction;
		}
	}
	
	static public function justReleased(input:Input):Bool {
		
		return switch(input) {
			case UP | DOWN | LEFT | RIGHT:
				!checkDir(dir, input) && checkDir(prevDir, input);
			case BACK | PAUSE : false;
			case STONE | ACCEPT: !action && prevAction;
		}
	}
	
	inline static function checkDir(v:FlxVector, input:Input):Bool {
		
		return switch(input) {
			case UP    : v.y < -DIGITAL_DEAD_RANGE;
			case DOWN  : v.y >  DIGITAL_DEAD_RANGE;
			case LEFT  : v.x < -DIGITAL_DEAD_RANGE;
			case RIGHT : v.x >  DIGITAL_DEAD_RANGE;
			default: false;
		}
	}
}