package ;

import openfl.display.Bitmap;
import openfl.display.BitmapData;
import openfl.events.MouseEvent;
import openfl.geom.Rectangle;
import openfl.events.ProgressEvent;
import openfl.events.Event;

@:keep @:bitmap("assets/images/ui/preloader_cart.png")
private class Cart extends BitmapData {}

@:keep @:bitmap("assets/images/ui/preloader_system.png")
private class Console extends BitmapData {}

class Preloader extends DefaultPreloader {
	
	static inline var SCALE = 8;
	
	var cart:Bitmap;
	var console:Bitmap;
	
	override public function new():Void {
		// super(#if debug 10 #end);
		super();
	}
	
	override function onInit():Void {
		super.onInit();
		
		addChild(cart = new Bitmap(new Cart(16, 12)));
		cart.scaleX = cart.scaleY = SCALE;
		cart.smoothing = false;
		cart.x = (stage.stageWidth  - 16 * SCALE) / 2;
		cart.y = (stage.stageHeight - 16 * SCALE) / 2 - (39 * SCALE / 2);//78x4;
		cart.scrollRect = new Rectangle(0, 0, 0, 16);
		
		addChild(console = new Bitmap(new Console(32, 57)));
		console.scaleX = console.scaleY = SCALE;
		console.smoothing = false;
		console.x = (stage.stageWidth  - 32 * SCALE) / 2;
		console.y = (stage.stageHeight - 57 * SCALE) / 2;
	}
	
	override function update(percent:Float):Void {
		
		// Animate load bar
		var rect = cart.scrollRect;
		rect.width = Std.int(16 * 4 * percent) / 4;
		cart.scrollRect = rect;
		super.update(percent);
	}
	
	override function startOutro(callback:Void->Void) {
		
		// stage.addEventListener(MouseEvent.CLICK, function (_) { 
			callback();
		// });
	}
}

private class DefaultPreloader extends openfl.display.Sprite {
	
	var _loaded:Bool;
	var _waited:Bool;
	var _loadPercent:Float;
	var _startTime:Float;
	var _waitTime:Float;
	
	public function new(minDisplayTime:Float = 0) 
	{
		_waitTime = minDisplayTime;
		_waited = false;
		_loaded = false;
		super();
		
		addEventListener
			( Event.ADDED_TO_STAGE
			, function onAddedToStage(_) {
				
				removeEventListener(Event.ADDED_TO_STAGE, onAddedToStage);
				
				onInit();
				updateByteProgess(loaderInfo.bytesLoaded, loaderInfo.bytesTotal);
				
				addEventListener(ProgressEvent.PROGRESS, onProgress);
				addEventListener(Event.COMPLETE, onComplete);
				
				_startTime = Date.now().getTime();
				addEventListener(Event.ENTER_FRAME, onEnterFrame);
			}
		);
		
	}
	
	
	
	function onInit() {}
	
	@:noCompletion
	function updateByteProgess(bytesLoaded:Int, bytesTotal:Int):Void {
		
		_loadPercent = 0.0;
		if (bytesTotal > 0) {
			
			_loadPercent = bytesLoaded / bytesTotal;
			if (_loadPercent > 1)
				_loadPercent = 1;
		}
	}
	
	@:noCompletion
	function onEnterFrame(event:Event):Void {
		
		
		var time = Date.now().getTime() - _startTime;
		if (time > _waitTime * 1000.0) {
			
			time = _waitTime * 1000.0;
			if (!_waited) {
				
				_waited = true;
				checkForOutro();
			}
		}
		
		var percent = _loadPercent;
		if (_waitTime > 0)
			percent *= time / _waitTime / 1000.0;
		
		update(percent);
	}
	
	function update(percent:Float):Void { }
	
	function onProgress(event:ProgressEvent):Void {
		
		updateByteProgess(Std.int(event.bytesLoaded), Std.int(event.bytesTotal));
	}
	
	function onComplete(event:Event):Void {
		
		updateByteProgess(loaderInfo.bytesLoaded, loaderInfo.bytesTotal);
		
		event.preventDefault();
		removeEventListener(ProgressEvent.PROGRESS, onProgress);
		removeEventListener(Event.COMPLETE, onComplete);
		
		_loaded = true;
		checkForOutro();
	}
	
	function checkForOutro():Void {
		
		if (_loaded && _waited)
			startOutro(endOutro);
	}
	
	function startOutro(callback:Void->Void):Void {
		
		callback();
	}
	
	function endOutro():Void {
		
		destroy();
		dispatchEvent(new Event(Event.UNLOAD));
	}
	
	function destroy():Void {
		
		removeEventListener(Event.ENTER_FRAME, onEnterFrame);
	}
}
