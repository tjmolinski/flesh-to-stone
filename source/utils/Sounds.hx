package utils;

import flixel.util.FlxTimer;
import states.PlayState.LevelType;
import flixel.FlxG;
import flixel.system.FlxSound;

enum Sound {
	EXPLOSION;
	HIT;
	DEATH;
	GOAL;
	MENU_NAV;
	MENU_SELECT;
	MENU_BAD;
	BEAM;
	SHOOT;
	STATUE;
	LOW_HEALTH_ALERT;
	POWER;
	SPAWN;
	TURRET;
	LUNGE;
	LATCH;
	SKY_LASER;
	SKY_LASER_WARN;
	CHEER;
	SUCCESS;
	SCORE_SMOOSH;
	SCORE_LIFT;
	END_CHARGE;
}

class Sounds {
	
	static var sounds:Map<Sound, Array<FlxSound>>;
	
	static public function init():Void {
		
		sounds = new Map();
		
		sounds[DEATH] =
			[ load("death1", 0.5)
			, load("death2", 0.5)
			, load("death3", 0.5)
			, load("death4", 0.5)
			];
		sounds[STATUE] =
			[ load("statue1", 0.5)
			, load("statue2", 0.5)
			, load("statue3", 0.5)
			, load("statue4", 0.5)
			];
		sounds[GOAL] =
			[ load("goal1")
			, load("goal2")
			, load("goal3")
			, load("goal4")
			, load("goal5")
			, load("goal6")
			, load("goal7")
			, load("goal8")
			];
		sounds[LOW_HEALTH_ALERT] =
			[ load("lowhealth", 0.5)
			];
		sounds[SHOOT] =
			[ load("shoot1")
			, load("shoot2")
			, load("shoot3")
			, load("shoot4")
			];
		sounds[BEAM] =
			[ load("beam1")
			, load("beam2")
			, load("beam3")
			];
		sounds[MENU_SELECT] =
			[ load("buttonpress1")
			, load("buttonpress2")
			, load("buttonpress3")
			];
		sounds[MENU_NAV] =
			[ load("menu_nav1")
			, load("menu_nav2")
			, load("menu_nav3")
			, load("menu_nav4")
			];
		sounds[MENU_BAD] =
			[ load("bad1")
			];
		sounds[EXPLOSION] =
			[ load("explosion1")
			, load("explosion2")
			, load("explosion3")
			, load("explosion4")
			];
		sounds[HIT] =
			[ load("hit1")
			, load("hit2")
			, load("hit3")
			, load("hit4")
			];
		sounds[SPAWN] =
			[ load("spawn1")
			, load("spawn2")
			, load("spawn3")
			, load("spawn4")
			];
		sounds[POWER] =
			[ load("power1")
			, load("power2")
			, load("power3")
			];
		sounds[TURRET] =
			[ load("turret1")
			, load("turret2")
			];
		sounds[LUNGE] =
			[ load("lunge1")
			, load("lunge2")
			, load("lunge3")
			, load("lunge4")
			];
		sounds[LATCH] =
			[ load("latch1")
			, load("latch2")
			, load("latch3")
			, load("latch4")
			];
		sounds[SKY_LASER] =
			[ load("skylaser1")
			, load("skylaser2")
			, load("skylaser3")
			];
		sounds[SKY_LASER_WARN] =
			[ load("skylaser_warn1")
			, load("skylaser_warn2")
			];
		sounds[CHEER] =
			[ load("cheer1", 0.75)
			, load("cheer2", 0.75)
			, load("cheer3", 0.75)
			, load("cheer4", 0.75)
			, load("cheer5", 0.75)
			];
		sounds[SUCCESS] =
			[ load("success1")
			, load("success3")
			];
		sounds[SCORE_SMOOSH] =
			[ load("smoosh1", 0.3)
			, load("smoosh2", 0.3)
			, load("smoosh3", 0.3)
			, load("smoosh4", 0.3)
			];
		sounds[SCORE_LIFT] =
			[ load("lift1", 0.3)
			, load("lift2", 0.3)
			, load("lift3", 0.3)
			, load("lift4", 0.3)
			];
		sounds[END_CHARGE] = [load("chargeUp")];
	}
	
	inline static function load(name:String, volume = 1.0):FlxSound {
		
		return FlxG.sound.load('assets/sounds/$name.mp3', volume);
	}
	
	static public function getDuration(type:Sound, num:Int):Float
	{
		return sounds[type][num].length / 1000;
	}
	
	static public function getCount(type:Sound):Int
	{
		return sounds[type].length;
	}
	
	inline static public function delay(duration:Float, type:Sound, num = -1, log = false):Void
	{
		if (log)
			trace(type, num, duration);
		
		if (duration <= 0)
			play(type, num);
		
		new FlxTimer().start(duration, (_)->play(type, num));
	}
	
	static public function play(type:Sound, num = -1):Null<FlxSound> {
		
		if (num < 0)
			num = FlxG.random.int(0, sounds[type].length - 1);
		
		if (num >= sounds[type].length)
			return null;
		
		return sounds[type][num].play();
	}
	
	static public function playMusic(name:String, volume = 1.0):Void {
		
		FlxG.sound.playMusic('assets/music/$name.mp3', volume);
	}
	
	static public function stopMusic():Void {
		
		if (FlxG.sound.music != null)
			FlxG.sound.music.stop();
	}
	
	static public function playLevelMusic(name:String, level:LevelType):Void {
		
		var num = switch(level) {
			case Empty(NONE), Endless: FlxG.random.int(0, 2);
			case Empty(goals):
				switch(goals) {
					case HARD: 2; case MEDIUM: 1; case EASY,_: 0;
				}
			case Story(num): num;
		}
		
		switch(num % 3) {
			case 0: playMusic("ldsong"             , 0.75);
			case 1: playMusic("BridgingtheGap_slim", 0.75);
			case 2: playMusic("MySong3_slim"       , 0.75);
		}
	}
}