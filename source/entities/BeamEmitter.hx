package entities;

import flixel.FlxG;
import flixel.FlxSprite;

/**
 * ...
 * @author TJ
 */
class BeamEmitter extends FlxSprite 
{
	@:keep
	public function new()
	{
		super();
		
		loadGraphic("assets/images/enemies/beam-emitter.png", true, 14, 14);
		
		animation.add("intro", [0, 1], 15);
		animation.add("life", [2, 3, 4, 5, 6], 15);
		centerOffsets();
	}
	
	public function spawn(newX:Float, newY:Float, _angle:Float, xSpeed:Float, ySpeed:Float)
	{
		super.reset(newX - (width/2), newY - (height/2));
		angle = _angle;
		velocity.set(xSpeed, ySpeed);
		moves = false;
		animation.play("intro");

		return this;
	}

	override public function update(elapsed:Float):Void
	{
		if (!alive)
		{
			exists = false;	
		}
		else if (velocity.x > 0 && x > FlxG.width)
		{
			kill();
		}
		else if (velocity.x < 0 && x < 0)
		{
			kill();
		}
		else if (velocity.y < 0 && y < 0)
		{
			kill();
		}
		else if (velocity.y > 0 && y > FlxG.height)
		{
			kill();
		}
		super.update(elapsed);
	}
}