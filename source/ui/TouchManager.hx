package ui;

import flixel.FlxG;
import flixel.input.touch.FlxTouch;
import flixel.math.FlxVector;

enum TouchMode {
	MENU;
	JOYSTICK;
	SWIPE;
	RELATIVE;
	NONE;
}

class TouchManager {
	
	inline static var DIGITAL_DEAD_RANGE = 10;
	inline static var ANALOG_MAX_RANGE = 16;
	inline static var ANALOG_DEAD_RANGE = 4;
	inline static var RELATIVE_DEAD_RANGE = 6;
	
	static public var action(get, never):Bool;
	inline static function get_action():Bool { return actionTouch != null; }
	static public var prevAction:Bool;
	static public var dir         (default, null):FlxVector = new FlxVector();
	static public var prevDir     (default, null):FlxVector = new FlxVector();
	static public var prevPos     (default, null):FlxVector = new FlxVector();
	static public var dirUnit     (default, null):FlxVector = new FlxVector();
	static public var anchor      (default, null):FlxVector = new FlxVector();
	static var joystickTouch:FlxTouch;
	static var actionTouch:FlxTouch;
	
	static public function update(elapsed:Float):Void {
		
		prevDir.copyFrom(dir);
		prevAction = action;
		
		for (touch in FlxG.touches.justReleased()){ 
			
			if (touch == joystickTouch) {
				
				joystickTouch = null;
				dir.set();
				dirUnit.set();
				anchor.set();
			}
			
			if (touch == actionTouch)
				actionTouch = null;
		}
		
		for (touch in FlxG.touches.justStarted()) {
			
			var screenPos = touch.getScreenPosition();
			
			if (screenPos.x < FlxG.width / 2) {
				
				if (joystickTouch == null) {
					
					joystickTouch = touch;
					joystickTouch.getPosition(prevPos);
					anchor.copyFrom(joystickTouch.justPressedPosition);
				}
				
			} else if (actionTouch == null)
				actionTouch = touch;
		}
		
		if (joystickTouch != null) {
			
			dir.set
				( joystickTouch.x - joystickTouch.justPressedPosition.x
				, joystickTouch.y - joystickTouch.justPressedPosition.y
				);
			
			dirUnit.copyFrom(dir);
			if (dirUnit.lengthSquared > ANALOG_MAX_RANGE * ANALOG_MAX_RANGE)
				dirUnit.normalize();
			else
				dirUnit.scale(1 / ANALOG_MAX_RANGE);
			
			joystickTouch.getPosition(prevPos);
		}
	}
	
	static public function pressed(input:Input):Bool {
		
		return switch(input) {
			case UP | DOWN | LEFT | RIGHT : checkDir(dir, input);
			case BACK | PAUSE : false;
			case STONE | ACCEPT: action;
		}
	}
	
	static public function justPressed(input:Input):Bool {
		
		return switch(input) {
			case UP | DOWN | LEFT | RIGHT:
				checkDir(dir, input) && !checkDir(prevDir, input);
			case BACK | PAUSE : false;
			case STONE | ACCEPT: action && !prevAction;
		}
	}
	
	static public function justReleased(input:Input):Bool {
		
		return switch(input) {
			case UP | DOWN | LEFT | RIGHT:
				!checkDir(dir, input) && checkDir(prevDir, input);
			case BACK | PAUSE : false;
			case STONE | ACCEPT: !action && prevAction;
		}
	}
	
	inline static function checkDir(v:FlxVector, input:Input):Bool {
		
		return switch(input) {
			case UP    : v.y <= -DIGITAL_DEAD_RANGE;
			case DOWN  : v.y >=  DIGITAL_DEAD_RANGE;
			case LEFT  : v.x <= -DIGITAL_DEAD_RANGE;
			case RIGHT : v.x >=  DIGITAL_DEAD_RANGE;
			default: false;
		}
	}
}