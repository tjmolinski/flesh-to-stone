package utils;

class MultiCallback
{
	var callback:()->Void;
	var listeners:Array<()->Void> = [];
	
	public function new(callback:()->Void):Void
	{
		this.callback = callback;
	}
	
	public function add():()->Void
	{
		var listener:()->Void;
		listener = function ()
		{
			if (listeners.remove(listener))
				checkListeners();
		}
		listeners.push(listener);
		return listener;
	}
	
	inline function checkListeners():Void
	{
		if (listeners.length == 0 && callback != null)
		{
			callback();
			destroy();
		}
	}
	
	public function destroy():Void
	{
		while(listeners.length > 0)
			listeners.pop();
		
		callback = null;
	}
}