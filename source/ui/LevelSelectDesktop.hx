package ui;

import flixel.FlxG;
import flixel.util.FlxSignal;
import flixel.util.FlxTimer;

import data.Levels;
import data.Save;
import ui.LevelIcon.LevelIconBase;
import ui.LevelIcon.LevelIconNewGamePlus;
import utils.Sounds;

class LevelSelectDesktop extends ButtonGroup.TypedButtonGroup<LevelIconBase>
{
	inline static var COLUMNS = 3;
	inline static var ROWS = 4;
	
	var showingNGP = false;
	var maxUnlocked:Int;
	
	public var onNewGamePlusEnabled(default, null) = new FlxSignal();
	
	public function new (callback:Void->Void)
	{
		super(Levels.LEVELS);
		maxUnlocked = Levels.getUnlockedLevelCount();
		
		for(i in 0...Levels.LEVELS)
			addButton(new LevelIcon(i), callback);
		
		// Disable normal key logic for grid logic
		keysNext = null;
		keysPrev = null;
	}
	
	override function startIntro(delay = 0.0, ?callback:Void->Void):Void
	{
		visible = true;
		active = false;
		members[selected].deselect();
		callback = endIntro.bind(callback);
		
		var duration = 0.0;
		for (button in members)
		{
			var iconDuration = button.showIntro();
			if (iconDuration > duration)
				duration = iconDuration;
		};
		
		new FlxTimer().start(duration, (_)->checkEndIntro(callback));
	}
	
	function checkEndIntro(callback:Void->Void):Void {
		
		inline function getLevelIcon(level):LevelIcon { return cast members[level]; }
		
		var unlock = Levels.getUnseenUnlockedLevel();
		if (unlock > -1) {
			
			getLevelIcon(unlock - 1).showNewProgress(
				getLevelIcon(unlock).showUnlock.bind(checkEndIntro.bind(callback))
			);
			
			Levels.updateSeenScores();
		
		} else if (Levels.getUnseenProgressLevel() > -1) {
			
			getLevelIcon(Levels.getUnseenProgressLevel())
				.showNewProgress(checkEndIntro.bind(callback));
			
			Levels.updateSeenScores();
		}
		else if (Levels.hasUnlockedPlus() && !showingNGP)
			showNewGamePlusButton(checkEndIntro.bind(callback));
		else
			callback();
	}
	
	function endIntro(callback:Null<Void->Void>):Void {
		
		active = true;
		members[selected].select();
		
		if (callback != null)
			callback();
	}
	
	function showNewGamePlusButton(callback:Void->Void):Void {
		
		maxSize = Levels.LEVELS + 1;
		maxUnlocked = maxSize;
		showingNGP = true;
		
		var button = new LevelIconNewGamePlus();
		addButton(button, onClickPlus.bind(button));
		var duration = button.showIntro();
		new FlxTimer().start(duration, (_)->callback());
	}
	
	function onClickPlus(plusButton:LevelIconNewGamePlus):Void {
		
		plusButton.deselect();
		active = false;
		var prompt = new Prompt();
		var parent = FlxG.state;
		prompt.setup(
			"START NEW GAME PLUS?",
			function onYes()
			{
				showingNGP = false;
				plusButton.hide();
				startNewGamePlusTransition();
			},
			plusButton.select,
			function removePrompt() {
				
				parent.remove(prompt);
				active = true;
			}
		);
		parent.add(prompt);
	}
	
	function startNewGamePlusTransition():Void {
		
		onNewGamePlusEnabled.dispatch();
		Save.startNewGamePlus();
		active = false;
		
		final totalStagger = 1.0;
		var duration = 0.0;
		for (i in 0...members.length) {
			
			var iconDuration = members[Levels.LEVELS - i].resetProgress(i / Levels.LEVELS * totalStagger);
			if (iconDuration > duration)
				duration = iconDuration;
		}
		
		new FlxTimer().start(duration,
			function(_) {
				
				selected = 0;
				active = true;
			}
		);
	}
	
	override function checkKeys(elapsed:Float)
	{
		var newSelected = selected;
		if (Inputs.justPressed.RIGHT)
		{
			newSelected++;
			if (newSelected % COLUMNS == 0)
				newSelected -= COLUMNS;
		}
		
		if (Inputs.justPressed.LEFT)
		{
			newSelected--;
			if (selected % COLUMNS == 0)
				newSelected += COLUMNS;
		}

		if (Inputs.justPressed.DOWN)
		{
			newSelected += COLUMNS;
			if (showingNGP)
			{
				if (newSelected >= maxSize && selected < maxSize - 1)
					newSelected = maxSize - 1;
				else if (selected == maxSize - 1)
					newSelected = 1;// top-center
			}
			else if (newSelected >= maxSize)
				newSelected -= maxSize;
		}

		if (Inputs.justPressed.UP)
		{
			newSelected -= COLUMNS;
			if (showingNGP)
			{
				if (newSelected < 0)
					newSelected = maxSize - 1;
				else if (selected == maxSize - 1)
					newSelected = 10;// directly above NPG
			}
			else if (newSelected < 0)
				newSelected += maxSize;
		}
		
		if (selected != newSelected)
		{
			if (newSelected <= maxUnlocked)
				selected = newSelected;
			else
				Sounds.play(MENU_BAD);
		}
		super.checkKeys(elapsed);
	}
	
	override function set_selected(value:Int):Int
	{
		if (members.length > value) {
			
			if (selected != value)
				Sounds.play(MENU_NAV);
			members[selected].deselect();
			selected = value;
			members[selected].select();
		}
		return selected;
	}
}