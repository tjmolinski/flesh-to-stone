package ui;


import utils.Sounds;
import io.newgrounds.objects.Error;
import data.Save;
import flixel.FlxG;
import flixel.util.FlxTimer;

import io.newgrounds.NG;

class NgConnector extends flixel.group.FlxGroup {
	
	var infoText:BitmapText;
	var onComplete:Void->Void;
	
	public function new () { super(); }
	
	public function start(onComplete:Void->Void):Void {
		
		this.onComplete = onComplete;
		
		infoText = new BitmapText(0, FlxG.height * 0.6, "");
		infoText.color = 0xFFffff00;
		setText("CONNECTING...");
		add(infoText);
		
		if (NG.core.attemptingLogin) {
			
			// Hacky way of adding 2 listeners that get removed when either is called
			var onFail:Error->Void;// needed so onSuccess can reference onFail and vice versa
			function onSuccess() {
				
				showMsgAndContinue("CONNECTED");
				Save.onSessionFail.remove(onFail);
			}
			onFail = function(e) {
				
				NG.core.onLogin.remove(onSuccess);
				startNewSession();
			}
			
			Save.onSessionFail.addOnce(onFail);
			NG.core.onLogin.addOnce(onSuccess);
			
		} else {
			
			startNewSession();
		}
	}
	
	function startNewSession():Void {
		
		NG.core.requestLogin(
			function() {
				showMsgAndContinue("CONNECTED");
				Sounds.play(CHEER);
			},
			onPending,
			showMsgAndContinue.bind("FAILED TO CONNECT", _),
			showMsgAndContinue.bind("LOGIN CANCELLED")
		);
	}
	
	function setText(text) {
		
		infoText.text = text;
		infoText.centerXOnStage();
	}
	function showMsgAndContinue(text:String, ?_) {
		
		setText(text);
		new FlxTimer().start(1.0, function(_) { onComplete(); });
	}
	
	function onPending() {
		
		var prompt = new Prompt(true);
		prompt.setup(
			"LOGIN TO NEWGROUNDS?",
			NG.core.openPassportUrl,
			showMsgAndContinue.bind("LOGIN CANCELLED"),
			remove.bind(prompt)
		);
		add(prompt);
	}
}