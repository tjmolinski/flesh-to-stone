package states;

#if newgrounds
import data.ApiData;
import ui.MedalPopup;
#end

import data.Save;
import data.Tilemap;
import data.WaterSeam;
import ui.BitmapText;
import ui.ControlInstructions;
import ui.Inputs;
import utils.Sounds;
import utils.Transitions;

import flixel.FlxBasic;
import flixel.FlxG;
import flixel.effects.FlxFlicker;
import flixel.tweens.FlxEase;
import flixel.tweens.FlxTween;
import flixel.util.FlxSignal;

abstract LevelSelectState(LevelSelectStateBase) to LevelSelectStateBase
{
	inline public function new (isFromPlay = true, ?selected:Int):Void
	{
		#if FLX_TOUCH
		if (FlxG.onMobile)
			this = new LevelSelectStateMobile(isFromPlay);
		else#end
			this = new LevelSelectStateDesktop(isFromPlay, selected);
		
		this.onNewGamePlusEnabled.add
		(
			function ()
			{
				FlxFlicker.flicker(this.titleText);
				if (this.titleText.text == "LEVELS")
					this.titleText.text += " ";
				this.titleText.text += "+";
			}
		);
	}
}

class LevelSelectStateBase extends MapState
{
	#if debug
	inline static var DEBUG_MEDAL:Null<Int>
		//  = ApiData.medal_story6;
		 = null;
	#end
	
	public var onNewGamePlusEnabled(default, null) = new FlxSignal();
	public var titleText(default, null):BitmapText;
	
	var buttons:FlxBasic;
	var isFromPlay = false;
	
	public function new (isFromPlay:Bool)
	{
		this.isFromPlay = isFromPlay;
		super();
	}
	
	override function create()
	{
		super.create();
		
		Sounds.init();
		
		if (isFromPlay)
		{
			Sounds.playMusic("WasteTimeOnline");
			if (!FlxG.onMobile)
				add(new ControlInstructions(true));
		}
		
		map.x = -FlxG.width;
		
		var title = "LEVELS";
		for (i in 0...Save.newGamePlusLevel)
			title += "+";
		titleText = new BitmapText(0, 0, title);
		titleText.centerXOnStage();
		titleText.y = -titleText.height;
		add(titleText);
		
		createLevelIcons();
	}
	
	function createLevelIcons():Void { }
	
	function startIntro():Void
	{
		#if newgrounds
		showMedalUnlocks(startButtonIntro);
		#else
		startButtonIntro();
		#end
	}
	
	function startButtonIntro():Void
	{
		FlxTween.tween(titleText, { y:4 }, 0.5, { ease:FlxEase.backOut } );
	}
	
	#if newgrounds
	function showMedalUnlocks(callback:()->Void):Void
	{
		var medalId
			#if debug = DEBUG_MEDAL != null? DEBUG_MEDAL : getUnseenMedal();
			#else = getUnseenMedal();
			#end
			
		if (medalId != null)
		{
			var popup:MedalPopup = new MedalPopup();
			add(popup);
			popup.show(medalId, ()->{ remove(popup); callback(); });
		}
		else
			callback();
	}
	
	
	function getUnseenMedal():Null<Int> {
		
		var complete = Save.getUnseenCompletedLevelCount();
		if (Save.getCompletedLevelCount() != complete)
		{
			return switch(complete)
			{
				case 6 : ApiData.medal_story6;
				case 12: ApiData.medal_story12;
				case 18: ApiData.medal_story18;
				case 24: ApiData.medal_story24;
				default: null;
			}
		}
		else
		{
			return null;
		}
	}
	#end
	
	override public function update(elapsed:Float):Void
	{
		super.update(elapsed);
		
		if (Inputs.justPressed.BACK && buttons.active)
		{
			transitionToMainMenu();
			Sounds.play(MENU_SELECT);
		}
	}
	
	function transitionToMainMenu():Void
	{
		buttons.active = false;
		Transitions.panToType(-1, 0, MenuState);
	}
	
	function transtitionToLevel(level:Int)
	{
	
		inline function addNewMap(level:Int, ?tiles:Int):Tilemap
		{
			var newMap = new Tilemap();
			newMap.switchLevel(level);
			if (tiles != null)
				newMap.switchTiles(tiles);
			add(newMap);
			members.remove(newMap);
			members.unshift(newMap);
			return newMap;
		}
		
		var newWorld = Std.int(level / 3);
		var currentWorld = Tilemap.currentTiles;
		if (newWorld > currentWorld)
		{
			var newMap = addNewMap(level, newWorld);
			newMap.y = FlxG.height * 2;
			
			if (currentWorld == 0 && newWorld == 3)
				add(new WaterSeam(0, 3, newMap.y - 8));
			
			Transitions.panToStateInTime(0, newMap.y / FlxG.height, 3.0, new PlayState(Story(level)));
		}
		else if (newWorld < currentWorld)
		{
			var newMap = addNewMap(level, newWorld);
			newMap.y = -FlxG.height;
			if (newWorld != 2 && currentWorld != 1)
			{
				newMap.y -= 8;
				add(new WaterSeam(newWorld, currentWorld));
			}
			Transitions.panToStateInTime(0, newMap.y / FlxG.height, 1.0, new PlayState(Story(level)));
		}
		else if (level > Tilemap.currentLevel)
		{
			var newMap = addNewMap(level);
			newMap.x = FlxG.width;
			Transitions.panToState(1, 0, new PlayState(Story(level)));
		}
		else
		{
			map.switchLevel(level);
			Transitions.panToState(-1, 0, new PlayState(Story(level)));
		}
	}
	
	override function destroy()
	{
		super.destroy();
		buttons = null;
		onNewGamePlusEnabled.removeAll();
	}
}