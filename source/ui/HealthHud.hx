package ui;

import flixel.FlxSprite;

import entities.PBJMouse;

class HealthHud extends flixel.group.FlxSpriteGroup {
	
	public function new (x:Float = 8, y:Float = 8) {
		super(x, y, PBJMouse.MAX_STONE_HEALTH);
		
		/* disabled for haxe 4.0.0-rc.1, fixed on dev
		for (i in 0 ... PBJMouse.MAX_STONE_HEALTH)
			add(new FlxSprite(18 * i, 0, "assets/images/ui/hitpoint.png"));}
		*/
	}
	
	public function set(value:Float):Void {
		
		if (PBJMouse.MAX_STONE_HEALTH == 0)
			return;
		
		//var remainder:Float = value - Std.int(value);
		value = Std.int(value);
		
		var current = countLiving();
		while (value > current) {
			
			members[current].revive();
			current++;
		}
		
		while (value < current) {
			
			current--;
			members[current].kill();
		}
	}
}