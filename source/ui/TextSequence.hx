package ui;

import flixel.FlxG;
import flixel.FlxSprite;
import flixel.group.FlxGroup;
import flixel.math.FlxRect;
import flixel.tweens.FlxTween;
import flixel.tweens.FlxEase;
import flixel.util.FlxTimer;

class TextSequence extends FlxGroup
{
	public var text(default, null):BitmapText;
	
	var allowSkip = false;
	var isInProgress = false;
	var onComplete:()->Void;
	
	public function show
	( msgs:Array<String>
	, callback:()->Void
	, allowSkip = false
	, showSkipDelay = 6.0
	, allowScroll = true
	):Void
	{
		isInProgress = true;
		this.allowSkip = allowSkip;
		
		var skipText:FlxSprite;
		if (allowSkip)
		{
			add(skipText = new FlxSprite("assets/images/text/skip.png"));
			skipText.scrollFactor.set();
			skipText.x = FlxG.width - skipText.width;
			skipText.y = FlxG.height;
			
			FlxTween.tween
				( skipText
				, { y:FlxG.height - skipText.height }
				, 0.5
				, { ease:FlxEase.circOut, startDelay:showSkipDelay }
				);
		}
		
		final distance = 8 * 4;
		text = new BitmapText("", 0);
		add(text);
		if (!allowScroll) text.scrollFactor.set();
		text.clipRect = FlxRect.get(0, 0, text.width, distance);
		final clip = text.clipRect;
		function updateClipRect(_) { text.clipRect = text.clipRect; }
		
		onComplete = function()
		{
			if (isInProgress)
			{
				isInProgress = false;
				callback();
				if (skipText != null)
					FlxTween.tween(skipText, { y:FlxG.height }, 0.5, { ease:FlxEase.circIn });
			}
		};
		
		final tweenTime = 2.0;
		final delayTime = 0.5;
		var delay = 0.0;
		function showText(msg:String, holdTime = 2.0, last = false):Void
		{
			text.text = msg;// needed for get height
			final centerY = (FlxG.height - text.height) / 2
				+ (allowScroll ? FlxG.camera.scroll.y : 0);
			FlxTween.tween
				( text
				, { y:centerY }
				, tweenTime
				,   { startDelay:delay
					, onStart:(_)->
						{
							text.text = msg;
							text.centerXOnStage();
							text.y = centerY + distance;
							clip.width = text.width;
						}
					, ease:FlxEase.circOut
					}
				);
			FlxTween.tween
				( clip
				, { y:0 }
				, tweenTime
				,   { startDelay:delay
					, ease:FlxEase.circOut
					, onStart:(_)->{ clip.y = -distance; }
					, onUpdate:updateClipRect
					, onComplete:(_)->{ clip.y = text.height - distance; }
					}
				);
			delay += tweenTime + holdTime;
			
			if (last) return;
			
			FlxTween.tween
				( text
				, { y:centerY - distance }
				, tweenTime
				, { startDelay:delay, ease:FlxEase.circIn }
				);
			FlxTween.tween
				( clip
				, { y:text.height }
				, tweenTime
				, { startDelay:delay, ease:FlxEase.circIn, onUpdate:updateClipRect }
				);
			delay += tweenTime + delayTime;
		}
		delay += 2.0;// initial delay
		
		do showText(msgs.shift(), msgs.length == 0)
		while(msgs.length > 0);
		
		new FlxTimer().start(delay + 1.0, (_)->onComplete());
	}
	
	override function update(elapsed:Float)
	{
		super.update(elapsed);
		
		if (isInProgress && allowSkip && (FlxG.keys.pressed.ANY || FlxG.mouse.pressed))
			onComplete();
	}
}