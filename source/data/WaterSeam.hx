package data;

import flixel.FlxG;
import flixel.math.FlxPoint;
import flixel.graphics.frames.FlxTileFrames;

class WaterSeam extends flixel.tile.FlxTilemap
{
	inline static var WIDTH = 20;
	inline static var HEIGHT = 1;
	
	public function new(topTiles:Int, bottomTiles:Int, y = -8.0)
	{
		super();
		this.y = y;
		
		if (topTiles >= bottomTiles)
			throw 'topTiles($topTiles) must not exceed bottomTiles($bottomTiles)';
		
		if (bottomTiles == 2 && topTiles == 1)
			throw "no transition needed from 2 to 1, same color";
		
		if (bottomTiles > 1) bottomTiles--;
		if (topTiles > 1) topTiles--;
		
		var world = 4 * (topTiles == 1 ? 2 : bottomTiles - 1);
		var map = [for (i in 0...WIDTH) world + FlxG.random.int(1, 2)];
		map[0] = world;
		map[WIDTH-1] = world + 3;
		
		loadMapFromArray
			( map
			, WIDTH, HEIGHT
			, FlxTileFrames.fromBitmapAddSpacesAndBorders
				( "assets/images/worldTransitions.png"
				, FlxPoint.get(8, 8)
				, FlxPoint.get()
				, FlxPoint.get(2, 2)
				)
			, 8, 8
			);
			
	}
}