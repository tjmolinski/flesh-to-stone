package entities;

import vfx.Explosion;
import vfx.Warning;

import flixel.math.FlxPoint;
import flixel.math.FlxVector;

class Seeker extends Enemy {
	
	static inline var SPEED = 30;
	static inline var JUMP_SPEED = SPEED * 4;
	static inline var ACCEL = SPEED * 10;
	
	static var count = 0;
	
	var pbjMouse:PBJMouse;
	var scared = false;
	var explosion:Explosion;
	var halfWidth:Float;
	var halfHeight:Float;
	
	public function new (x = 0.0, y = 0.0) {
		super(x, y);
		
		loadGraphic("assets/images/enemies/bullet.png", true, 8, 8);
		setColorTransform(0.5, 1.0, 1.0, 1.0, 128, 0, 128);
		animation.add("life", [0, 1, 2, 3, 4, 5], 10, true);
		animation.play("life");
		
		width -= 2;
		height -= 2;
		centerOffsets();
		drag.set(ACCEL, ACCEL);
		halfWidth = width / 2;
		halfHeight = height / 2;
	}
	
	public function init(pos:FlxPoint, pbjMouse:PBJMouse, explosion:Explosion, warning:Warning):Void {
		
		ID = count++;
		this.explosion = explosion;
		this.pbjMouse = pbjMouse;
		scared = false;
		maxVelocity.set(SPEED, SPEED);
		moves = true;
		solid = true;
		
		x = pos.x;
		y = pos.y;
		
		playWarningAnim(warning);
	}
	
	override function update(elapsed:Float) {
		super.update(elapsed);
		
		var statue = pbjMouse.isStatue();
		acceleration.set();
		
		if (!scared) {
			
			var v = FlxVector.get(pbjMouse.x - x, pbjMouse.y - y)
				.normalize();
			
			if (statue) {
				
				// Pussy jump on transform
				velocity.copyFrom(v).scale(-JUMP_SPEED);
				maxVelocity.set(JUMP_SPEED, JUMP_SPEED);
				scared = true;
			}
			else
				acceleration.copyFrom(v.scale(ACCEL));
			
			v.put();
			
		} else if (!statue) {
			
			scared = false;
			maxVelocity.set(SPEED, SPEED);
		}
	}
	
	override function murder() {
		
		solid = false;
		moves = false;
		explosion.spawn(x + halfWidth, y + halfHeight);
		kill();
	}
}