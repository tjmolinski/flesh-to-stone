package vfx;

import flixel.FlxG;

/**
 * ...
 * @author TJ
 */
class TitleBullet extends flixel.addons.display.FlxSpriteAniRot 
{
	private var speed:Float = 50;

	@:keep
	public function new()
	{
		super("assets/images/enemies/bullet.png", 16, false, false, 8, 8);
		animation.add("life", [0, 1, 2, 3, 4, 5], 10, true);
		animation.play("life", true, false, -1);
	}

	public function spawn(newX:Float)
	{
		super.reset(newX - (width/2), - 50);
		solid = true;
		angularAcceleration = (Math.random() * 90) - 45;
		var randScale = (Math.random() * 2) + 1;
		scale.set(randScale, randScale);
		acceleration.set(0, speed);
	}

	override public function update(elapsed:Float):Void
	{
		if (!alive)
		{
			exists = false;	
		}
		else if (y > FlxG.height + 50)
		{
			kill();
		}
		super.update(elapsed);
	}
	
}