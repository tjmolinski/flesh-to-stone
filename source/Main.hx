package;

import data.Save;
import states.SplashState;
import ui.Inputs;
import ui.Keys;
#if FLX_TOUCH
import ui.MobileInputUi;
#end

import flixel.FlxG;
import flixel.FlxGame;

import openfl.display.Sprite;

class Main extends Sprite
{
	inline static public var SKIP_MENU =
		// #if debug !/* uncomment to toggle */ #end
		false;
	
	inline static public var RUN_DEBUG_TEST =
		// #if debug !/* uncomment to toggle */ #end
		false;
	
	public function new()
	{
		super();
		Keys.setup(stage);
		var game = 
			if (RUN_DEBUG_TEST)
				new FlxGame(640, 640, SplashState, 1, 60, 60, true, false);
			else
				new FlxGame(160, 160, SplashState, 1, 60, 60, true, false);
		
		var wingSize = (stage.stageWidth - 640) / 2;
		game.x += wingSize;
		addChild(game);
		
		Save.init();
		
		FlxG.mouse.visible = false;
		FlxG.sound.volume = 0.5;
		FlxG.sound.muteKeys = [M];
		FlxG.plugins.add(new Inputs());
		
		// FlxG.scaleMode = new RatioScaleMode(true);
		// FlxG.scaleMode.horizontalAlign = CENTER;
		// FlxG.scaleMode.verticalAlign = CENTER;
		// FlxG.scaleMode.onMeasure(stage.stageWidth, stage.stageHeight);
		
		// Wings
		// var leftWing = new Shape();
		// leftWing.graphics.beginFill(0x144491);
		// leftWing.graphics.drawRect(-wingSize, 0, wingSize, stage.stageHeight);
		// leftWing.graphics.endFill();
		// leftWing.x = game.x;
		// addChild(leftWing);
		
		// var rightWing = new Shape();
		// rightWing.graphics.beginFill(0x144491);
		// rightWing.graphics.drawRect(0, 0, wingSize, stage.stageHeight);
		// rightWing.graphics.endFill();
		// rightWing.x = stage.stageWidth - wingSize;
		// addChild(rightWing);
		
		#if FLX_TOUCH
		if (FlxG.onMobile)
		{
			var uiIndicators = new MobileInputUi();
			uiIndicators.x = game.x;
			addChild(uiIndicators);
		}
		#end
	}
}