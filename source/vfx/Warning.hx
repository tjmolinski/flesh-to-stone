package vfx;

import flixel.util.FlxTimer;
import utils.Sounds;
import flixel.FlxSprite;

class Warning extends FlxSprite {
	
	public function new () {
		super();
		loadGraphic("assets/images/fx/enemy-warning.png", true, 16, 16);
		animation.add("warning", [0,1,2,3,4,5,6], 10, false);
		animation.add("exit", [6,5,4,3,2,1,0], 10, false);
		animation.add("explosion", [7,8,9,10,11,12,13], 10, false);
		
		offset.set(8, 8);
	}
	
	public function play(enemy:FlxSprite, callback:Void->Void):Void {
		
		x = enemy.x + enemy.width  / 2;
		y = enemy.y + enemy.height / 2;
		enemy.visible = false;
		animation.play("warning");
		animation.finishCallback = function(anim) {
			
			if (anim == "warning") {
				
				enemy.visible = true;
				animation.play("explosion");
				
			} else if (anim == "explosion") {
				
				callback();
				kill();
			}
		};
	}
	
	public function exit(enemy:FlxSprite, delay = 0.0, callback:Void->Void):Void {
			
		enemy.moves = false;
		enemy.solid = false;
		x = enemy.x + enemy.width  / 2;
		y = enemy.y + enemy.height / 2;
		
		function realExit(?_):Void
		{
			enemy.kill();
			animation.play("exit");
			Sounds.play(SPAWN);
			
			animation.finishCallback = function(anim) {
			
				if (callback != null)
					callback();
				
				kill();
			};
		}
		
		if (delay > 0)
			new FlxTimer().start(delay, realExit);
		else
			realExit();// rc3 inline
	}
}