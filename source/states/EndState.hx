package states;

import ui.MedalPopup;
import data.Levels;
import data.EndStateAssetData;
import states.PlayState;
import ui.Button.SoundButton;
import ui.Button.FullscreenButton;
import ui.BitmapText;
import ui.ButtonGroup;
import ui.ControlInstructions;
import ui.Inputs;
import ui.MobileButtonGroup;
import ui.ScoreBar;
import utils.Sounds;
import utils.Transitions;
import vfx.KO;

import flixel.FlxG;
import flixel.effects.FlxFlicker;
import flixel.math.FlxPoint;
import flixel.tweens.FlxEase;
import flixel.tweens.FlxTween;
import flixel.util.FlxTimer;

/**
 * ...
 * @author TJ
 */
class EndState extends MapState 
{
	var assetData:EndStateAssetData;
	
	public function new(assetData:EndStateAssetData)
	{
		this.assetData = assetData;
		
		super();
	}
	
	override public function create():Void
	{
		super.create();
		
		Sounds.init();
		#if FLX_TOUCH
		Inputs.touchMode = NONE;
		#end
		
		map.y = -FlxG.height;
		
		var scoreBar = new ScoreBarStatic(assetData.prevLevel, 4, 1.0);
		scoreBar.score = assetData.score;
		scoreBar.setNoScrollMode();
		
		add(new KO(FlxPoint.weak(assetData.koX, assetData.koY)));
		
		var delay = 0.0;
		if (assetData.unlockedMedal != null)
		{
			var popup = new MedalPopup();
			delay += popup.show(assetData.unlockedMedal, 12, remove.bind(popup));
		}
		else if (assetData.unlockedLevel)
		{
			var unlockText = new BitmapText(0, 1, "NEW LEVEL UNLOCKED!");
			unlockText.centerXOnStage();
			add(unlockText);
			scoreBar.flickerPoint(scoreBar.maxScore >> 1);
			FlxTween.tween(unlockText, { y:12 }, 0.5, { ease:FlxEase.backOut, startDelay: 0.25 });
			new FlxTimer().start(0.5, (_)->FlxFlicker.flicker(unlockText, 0.5));
			Sounds.play(CHEER);
			delay += 1.0;
		}
		else if (assetData.newBest)
		{
			var unlockText = new BitmapText(0, 1, "NEW BEST!");
			unlockText.centerXOnStage();
			add(unlockText);
			FlxTween.tween(unlockText, { y:12 }, 0.5, { ease:FlxEase.backOut, startDelay: 0.25 });
			new FlxTimer().start(0.5, (_)->FlxFlicker.flicker(unlockText, 0.5));
			Sounds.play(CHEER);
			delay += 1.0;
		}
		
		add(scoreBar);
		
		inline function createUi(onRetry:()->Void, onLevels:()->Void, onMain:()->Void):ITransitionable
		{
			#if FLX_TOUCH
			if (FlxG.onMobile)
				return new EndStateUiMobile(onRetry, onLevels, onMain);
			#end 
				return new EndStateUiDesktop(onRetry, onLevels, onMain);
		}
		
		var prevLevelNum = switch(assetData.prevLevel)
		{
			case Story(num): num;
			case _: null;
		}
		
		var buttons = createUi
		(
			function ()
			{
				if (scoreBar != null)
					scoreBar.resetForRetry(0.45);
				
				if (Levels.seenFtue)
					panUpTo(PlayState, [assetData.prevLevel]);
				else
					panUpTo(TutorialState);
			},
			Transitions.panToState.bind(1, -1, new LevelSelectState(prevLevelNum)),
			panUpTo.bind(MenuState)
		);
		add(cast buttons);
		
		if (!FlxG.onMobile)
			add(new ControlInstructions());
		
		// Start Intro
		FlxG.sound.music.stop();
		
		new FlxTimer().start(delay + 0.25,
			function(_) { Sounds.playMusic("Wet-Pixel-Dreams_slim"); }
		);
		delay += 0.25;
		
		buttons.startIntro(delay);
	}
	
	inline static function scaleText(text:BitmapText, scale:Float):Void
	{
		text.offset.set(-text.width * (scale - text.scale.x) / 2, -text.height * (scale - text.scale.y) / 2);
		text.width *= scale / text.scale.x;
		text.height *= scale / text.scale.y;
		
		text.scale.scale(scale);
	}
	
	function panUpTo(stateType:Class<MapState>, ?args:Array<Dynamic>):Void {
		
		Transitions.panToType(0, -1, stateType, args);
	}
}

class EndStateUiMobile extends MobileButtonGroup
{
	public function new (onRetry:()->Void, onLevels:()->Void, onMain:()->Void)
	{
		super(5);
		
		var buttonY = 94;
		final buttonSpacing = 19;
		addTextButton(40, buttonY, "RETRY", onRetry);
		buttonY += buttonSpacing;
		if (Levels.seenFtue)
		{
			addTextButton(40, buttonY, "LEVELS", onLevels);
			buttonY += buttonSpacing;
		}
		addTextButton(40, buttonY, "MAIN MENU", onMain);
		add(new SoundButton(4, 160 - 21 - 4));
		add(new FullscreenButton(160 - 19 - 4, 160 - 21 - 4));
	}
}

class EndStateUiDesktop extends ui.ButtonGroup
{
	public function new (onRetry:()->Void, onLevels:()->Void, onMain:()->Void)
	{
		super(3);
		
		final buttonYSpacing = 10;
		var buttonY = FlxG.height - buttonYSpacing * 4.5;
		addNewButton(0, buttonY, "RETRY", onRetry);
		if (Levels.seenFtue)
		{
			buttonY += buttonYSpacing;
			addNewButton(0, buttonY, "LEVEL SELECT", onLevels);
		}
		buttonY += buttonYSpacing;
		addNewButton(0, buttonY, "MAIN MENU", onMain);
	}
}