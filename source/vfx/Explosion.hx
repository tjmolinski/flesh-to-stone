package vfx;

import utils.Sounds;
import flixel.FlxG;
import flixel.FlxSprite;

/**
 * ...
 * @author TJ
 */
class Explosion extends FlxSprite 
{
	public function new()
	{
		super();
		
		loadGraphic("assets/images/fx/explosion.png", true, 18, 18);
		animation.add("life", [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10], 10, false);
		visible = false;
		offset.set(width / 2, height/ 2);
	}

	public function spawn(x:Float, y:Float)
	{
		super.reset(x, y);
		animation.finishCallback = function(anim:String) {
			if(anim == "life")
			{
				animation.finishCallback = null;
				kill();
			}
		};
		visible = true;
		animation.play("life", true, false, 0);
		Sounds.play(EXPLOSION);
	}
	
	override public function update(elapsed:Float):Void
	{
		if (!alive)
		{
			exists = false;	
			visible = false;
		}
		super.update(elapsed);
	}
}