package entities;

import vfx.Warning;
import utils.Sounds;
import flixel.FlxG;
import flixel.FlxObject;
import flixel.effects.FlxFlicker;
import flixel.tweens.FlxEase;
import flixel.tweens.FlxTween;
import flixel.util.FlxDestroyUtil.IFlxDestroyable;
import flixel.util.FlxTimer;

class Floater extends Enemy {
	
	inline static var SPEED = 30;
	inline static var BURST_SCALE = 2.0;
	inline static var ALLOW_CHAINING = false;
	inline static var BURST = true;
	inline static var DESTROY_STUFF_LENGTH = 0.5;
	
	var exploding = false;
	var destroyStuff:Array<IFlxDestroyable> = [];
	var wasOnScreen = false;
	
	public function new(x = 0.0, y = 0.0, velAngle = 0.0) {
		super(x, y);
		
		loadGraphic("assets/images/enemies/bullet.png", true, 8, 8);
		setColorTransform(0.5, 1.0, 1.0, 1.0, 0, 0, 128);
		animation.add("life", [0, 1, 2, 3, 4, 5], 10, true);
		animation.play("life");
		
		velocity.x = SPEED * Math.cos(velAngle);
		velocity.y = SPEED * Math.sin(velAngle);
	}
	
	public function init():Void {
		
		setEdgePosition();
		scale.set(1, 1);
		width  = frameWidth  - 2;
		height = frameHeight - 2;
		centerOffsets();
		solid = true;
		moves = true;
		exploding = false;
		wasOnScreen = false;
		
		var velAngle = FlxG.random.float(Math.PI / 2);
		velocity.x = SPEED * Math.cos(velAngle);
		velocity.y = SPEED * Math.sin(velAngle);
		if (x > FlxG.width * 0.5)
			velocity.x *= -1;
		if (y > FlxG.height * 0.5)
			velocity.y *= -1;
	}
	
	override function update(elapsed:Float) {
		super.update(elapsed);
		
		var onScreen = isOnScreen();
		
		if (onScreen)
			wasOnScreen = true;
		
		if (!onScreen && wasOnScreen)
			kill();
	}
	
	public function onCollide(floater:Floater):Void {
		
		if (BURST) {
			
			if (!exploding && (!floater.exploding || ALLOW_CHAINING))
				explode(true);
			if (!floater.exploding && (!exploding || ALLOW_CHAINING))
				floater.explode();
			
		} else {
			
			kill();
			floater.kill();
		}
	}
	
	override function exit(?sprite:Warning, ?callback:() -> Void) {
		
		new FlxTimer().start(FlxG.random.float(0, .5), (_)->explode(true));
	}
	
	function explode(playSound = false):Void {
		
		solid = false;
		moves = false;
		
		var destroyable = FlxTween.tween(
			this,
			{ 'scale.x': BURST_SCALE, 'scale.y': BURST_SCALE },
			0.125,
			{ ease:FlxEase.quintOut
			, startDelay: 0.75
			, onUpdate: function(_) { onScaleChange(); }
			, onStart: function(_) { Sounds.play(EXPLOSION); }
			}
		);
		destroyStuff.push(destroyable);
		
		flicker(this, 1.0, true,
			function(_):Void {
				
				if(exists) {
					
					exploding = true;
					solid = true;
					destroyStuff.push(new FlxTimer().start(DESTROY_STUFF_LENGTH, function(_){ kill(); } ));
				}
			}
		);
	}
	
	function flicker(
		target:FlxObject,
		duration:Float,
		endVisibility = true,
		?completeCallback:FlxFlicker->Void
	):FlxFlicker {
		
		if (target == null)
			return null;
		
		return FlxFlicker.flicker(
			target,
			duration,
			0.04,
			endVisibility,
			true,
			completeCallback
		);
	}
	
	function onScaleChange():Void {
		
		x -= offset.x;
		y -= offset.y;
		width  = Math.abs(scale.x) * frameWidth  / 8 * 7;
		height = Math.abs(scale.y) * frameHeight / 8 * 7;
		centerOffsets();
		x += offset.x;
		y += offset.y;
	}
	
	override function murder():Void {
		
		solid = false;
		FlxFlicker.flicker(this, 1, 0.04, true, true, function(_) { kill(); });
		Sounds.play(HIT);
	}
	
	override function kill() {
		
		while (destroyStuff.length > 0){
			
			var destroyable = destroyStuff.pop();
			
			if (Std.is(destroyable, FlxFlicker))
				destroyFlicker(cast destroyable);
			else if (Std.is(destroyable, FlxTween))
				destroyTween(cast destroyable);
			else if (Std.is(destroyable, FlxTimer))
				destroyTimer(cast destroyable);
		}
		
		super.kill();
	}
	
	inline function destroyFlicker(flicker:FlxFlicker):Void {
		
		if (!flicker.timer.finished)
			flicker.stop();
	}
	
	inline function destroyTween(tween:FlxTween):Void {
		
		if (!tween.finished)
			tween.cancel();
	}
	
	inline function destroyTimer(timer:FlxTimer):Void {
		
		if (!timer.finished)
			timer.cancel();
	}
}