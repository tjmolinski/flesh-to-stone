package data;

import flixel.FlxG;
import flixel.math.FlxPoint;
import flixel.graphics.frames.FlxTileFrames;

import openfl.Assets;

typedef MapData = Array<Array<Int>>;
class Tilemap extends flixel.tile.FlxTilemap {
	
	inline static var GRASS_LEFT = 3;
	inline static var GRASS_TOP = 5;
	inline static var GRASS_RIGHT = GRASS_LEFT + 14;
	inline static var GRASS_BOTTOM = GRASS_TOP + 12;
	
	static public var currentLevel(default, null) = 0;
	static public var currentTiles(default, null) = 0;
	
	static var tiles =
		[ new TileRange(46, 48, 0.8)// water // check first, most common
		, new TileRange(1 , 5 , 0.0)// top
		, new TileRange(9 , 11, 0.0)// left
		, new TileRange(12, 13, 0.0)// bottom left
		, new TileRange(14, 16, 0.0)// right
		, new TileRange(17, 18, 0.0)// bottom right
		, new TileRange(19, 21, 0.0)// bottom
		// , new TileRange(22, 28, 0.7)// tall grass
		// , new TileRange(37, 42, 0.7)// short grass
		, new TileRange(54, 55, 0.0)// craggies
		, new TileRange(57, 65, 0.0)// cliff
		];
	
	static var levelMaps:Map<Int, String> = new Map();
	static var worldData:Map<Int, MapData> = new Map();
	static var currentMap:Null<MapData>;
	
	public function new()
	{
		super ();
		
		if (currentMap == null)
			loadMapData();
		
		loadCurrentLevel();
	}
	
	inline function loadCurrentLevel()
	{
		loadMapFromCSV
			( levelMaps[currentLevel]
			, FlxTileFrames.fromBitmapAddSpacesAndBorders
				( 'assets/images/island-tiles${currentTiles+1}.png'
				, FlxPoint.get(8, 8)
				, FlxPoint.get()
				, FlxPoint.get(2, 2)
				)
			, 8, 8	// w, h
			, null	// auto tile
			, 0, 0, 46// indices: starting, draw, collide
			);
	}
	
	public function switchLevel(level:Int):Void
	{
		if (level == currentLevel)
			return;
		
		generateLevelCsv(level);
		currentLevel = level;
		loadCurrentLevel();
	}
	
	
	public function switchTiles(tiles:Int):Void
	{
		if (tiles == currentTiles)
			return;
		
		currentTiles = tiles;
		loadCurrentLevel();
	}
	
	#if debug
		
		override function update(elapsed:Float)
		{
			super.update(elapsed);
			var newTiles = currentTiles;
			if (FlxG.keys.justPressed.COMMA    ) newTiles = 1;
			if (FlxG.keys.justPressed.PERIOD   ) newTiles = 2;
			if (FlxG.keys.justPressed.SLASH    ) newTiles = 3;
			if (FlxG.keys.justPressed.SEMICOLON) newTiles = 4;
			if (newTiles != currentTiles)
			{
				currentTiles = newTiles;
				loadCurrentLevel();
			}
		}
	#end
	
	static function loadMapData():Void
	{
		var data = Xml.parse(openfl.Assets.getText("assets/data/level.tmx"))
			.elementsNamed("map").next();
		
		var csv:Null<String> = null;
		var csvsByName:Map<String, String> = new Map();
		for (layer in data.elementsNamed("layer"))
			csvsByName[layer.get("name")] = layer.firstElement().firstChild().toString();
		
		inline function createBaseLevel(csvName:String, level:Int):Void
		{
			var csv = csvsByName[csvName];
			if (csv == null)
				throw 'missing level.tmx layer $csvName';
			
			worldData[level] = convertTiledData(csv);
		}
		
		createBaseLevel("World1", 0);
		// createBaseLevel("World2", 1);
		// createBaseLevel("World3", 2);
		// createBaseLevel("World4", 3);
		
		currentMap = worldData[0];
		levelMaps[currentLevel] = generateCsv(currentMap);
	}
	
	static function convertTiledData(csv:String):MapData
	{
		var rows = csv.split("\n");
		rows.shift();
		rows.pop();
		var data:MapData = [];
		for (i in 0...rows.length)
		{
			// trace('$i[${rows[i].length}]: ' + rows[i]);
			var cols = rows[i].split(",");
			var j = cols.length;
			while (j-- > 0)
			{
				switch cols[j]
				{
					case "", null: cols.splice(j, 1);
					case col: cols[j] = Std.string(Std.parseInt(col) - 2);
				}
			}
			data.push(cols.map(function(s):Int { return Std.parseInt(s); }));
			
			// trace('$i[${rows[i].length}]: ' + rows[i]);
		}
		return data;
	}
	
	inline static function generateCsv(map:MapData):String
	{
		return map.map((row)-> row.join(",")).join("\n");
	}
	
	static function generateLevelCsv(level:Int):Void
	{
		if (levelMaps.exists(level))
			return;
		
		var prevSeed = FlxG.random.currentSeed;
		FlxG.random.currentSeed = level;
		randomizeMap();
		FlxG.random.currentSeed = prevSeed;
		
		levelMaps[level] = generateCsv(currentMap);
	}
	
	static function randomizeMap():Void
	{
		for (i in GRASS_LEFT...GRASS_RIGHT)
			for (j in GRASS_TOP...GRASS_BOTTOM)
				currentMap[j][i] = Grass.Tall;
		
		// few big squares
		for (c in GRASS_LEFT...GRASS_RIGHT-3)
		{
			for (r in GRASS_TOP...GRASS_BOTTOM-3)
			{
				if (FlxG.random.bool(10))
				{
					currentMap[r  ][c  ] = Grass.Short;
					currentMap[r+1][c  ] = Grass.Short;
					currentMap[r+2][c  ] = Grass.Short;
					currentMap[r+3][c  ] = Grass.Short;
					currentMap[r  ][c+1] = Grass.Short;
					currentMap[r+1][c+1] = Grass.Short;
					currentMap[r+2][c+1] = Grass.Short;
					currentMap[r+3][c+1] = Grass.Short;
					currentMap[r  ][c+2] = Grass.Short;
					currentMap[r+1][c+2] = Grass.Short;
					currentMap[r+2][c+2] = Grass.Short;
					currentMap[r+3][c+2] = Grass.Short;
					currentMap[r  ][c+3] = Grass.Short;
					currentMap[r+1][c+3] = Grass.Short;
					currentMap[r+2][c+3] = Grass.Short;
					currentMap[r+3][c+3] = Grass.Short;
				}
			}
		}
		
		// many little squares
		for (c in GRASS_LEFT...GRASS_RIGHT-1)
		{
			for (r in GRASS_TOP...GRASS_BOTTOM-1)
			{
				if (FlxG.random.bool(20))
				{
					currentMap[r][c] = Grass.Short;
					currentMap[r+1][c] = Grass.Short;
					currentMap[r][c+1] = Grass.Short;
					currentMap[r+1][c+1] = Grass.Short;
				}
			}
		}
		
		for (c in GRASS_LEFT...GRASS_RIGHT)
			for (r in GRASS_TOP...GRASS_BOTTOM)
				currentMap[r][c] = getAutoGrassTile(r, c);
		
		for (r in 0...20)
			for (c in 0...20)
				currentMap[r][c] = getAutoTile(currentMap[r][c]);
	}
	
	static function getAutoTile(tile:Int):Int
	{
		for (range in tiles)
		{
			if (range.has(tile))
				return range.get();
		}
		return tile;
	}
	
	static function getAutoGrassTile(row:Int, col:Int):Int
	{
		if (getIsTall(currentMap[row][col]))
			return  getRandomTall();
		
		return getAutoShort
			( col == GRASS_LEFT   || getIsTall(currentMap[row][col-1])
			, col == GRASS_RIGHT  || getIsTall(currentMap[row][col+1])
			, row == GRASS_TOP    || getIsTall(currentMap[row-1][col])
			, row == GRASS_BOTTOM || getIsTall(currentMap[row+1][col])
			);
	}
	
	static function getAutoShort(left:Bool, right:Bool, up:Bool, down:Bool):Int
	{
		return
			if      (left  && up  ) TopLeft;
			else if (right && up  ) TopRight;
			else if (left  && down) BottomLeft;
			else if (right && down) BottomRight;
			else if (right) Right;
			else if (up   ) Top;
			else if (left ) Left;
			else if (down ) Bottom;
			else getRandomShort();
	}
	
	inline static function getIsShort(grass:Int):Bool
	{
		return grass >= (Grass.TopLeft:Int);
	}
	
	inline static function getIsTall(grass:Int):Bool
	{
		return grass < (Grass.TopLeft:Int);
	}
	
	inline static function getRandomTile(min, max, emptyChance = .7):Int
	{
		return min + (FlxG.random.bool(emptyChance * 100) ? 0 : FlxG.random.int(1, max - min));
	}
	
	inline static function getRandomShort(emptyChance = 0.7):Int
	{
		return getRandomTile(Grass.Short, Grass.Short + 5, emptyChance);
	}
	
	inline static function getRandomTall(emptyChance = 0.7):Int
	{
		return getRandomTile(Grass.Tall, Grass.Tall + 6, emptyChance);
	}
	
	inline static function getRandomTop():Int
	{
		return getRandomTile(1, 7);
	}
}

@:forward
@:enum abstract Grass(Int) to Int from Int
{
	var Tall = 22;
	var Short = 37;
	var TopLeft = 29;
	var Top = 30;
	var TopRight = 31;
	var Left = 32;
	var Right = 33;
	var BottomLeft = 34;
	var Bottom = 35;
	var BottomRight = 36;
}

class TileRange
{
	var min:Int;
	var max:Int;
	var emptyChance:Float;
	
	public function new(min, max, emptyChance = 0.7)
	{
		this.min = min;
		this.max = max;
		this.emptyChance = emptyChance;
	}
	
	public function has(tile:Int):Bool return tile >= min && tile <= max;
	public function get():Int
	{
		return (FlxG.random.bool(emptyChance * 100) ? min : FlxG.random.int(min, max));
	}
}