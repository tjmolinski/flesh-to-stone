package states;


import data.Levels;
import states.PlayState;
import states.LevelSelectState.LevelSelectStateBase;
import ui.ControlInstructions;
import ui.LevelSelectDesktop;
import utils.Transitions;

/**
 * ...
 * @author TJ
 */
class LevelSelectStateDesktop extends LevelSelectStateBase 
{
	var selectedLevel:Null<Int>;
	var buttonsGroup(get, never):LevelSelectDesktop;
	function get_buttonsGroup() return cast buttons;
	
	public function new (isFromPlay:Bool, selectedLevel:Null<Int>)
	{
		this.selectedLevel = selectedLevel;
		super(isFromPlay);
	}
	
	override public function create():Void
	{
		super.create();
		
		if (selectedLevel != null && selectedLevel >= 0 && selectedLevel < Levels.LEVELS)
			buttonsGroup.selected = selectedLevel;
		
		add(new ControlInstructions());
		
		startIntro();
	}
	
	override function createLevelIcons()
	{
		add(buttons = new LevelSelectDesktop(()->transtitionToLevel(buttonsGroup.selected)));
		
		buttonsGroup.onNewGamePlusEnabled.add(onNewGamePlusEnabled.dispatch);
	}
	
	override function startButtonIntro()
	{
		super.startButtonIntro();
		
		buttonsGroup.startIntro();
	}
}