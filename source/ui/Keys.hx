package ui;

import flixel.input.keyboard.FlxKey;
import flixel.FlxG;
import flixel.util.FlxSignal;

import openfl.display.Stage;
import openfl.events.KeyboardEvent;

/**
 * Used for HTML requests that require user input, like fullscreen. 
 * for anything else use Inputs.press and junk.
 */
class Keys {
	
	static public var onPress:Map<Input, FlxSignal> = new Map();
	
	static public function setup(stage:Stage):Void {
		
		stage.addEventListener(KeyboardEvent.KEY_DOWN, onKeyPress, false, 1);
	}
	
	static public function add(input:Input, callback:Void->Void):Void {
		
		if (!onPress.exists(input))
			onPress[input] = new FlxSignal();
		
		onPress[input].add(callback);
	}
	
	static public function remove(input:Input, callback:Void->Void):Void {
		
		if (!onPress.exists(input))
			return;
		
		onPress[input].remove(callback);
	}
	
	static function onKeyPress(e:KeyboardEvent):Void {
		
		if (e.keyCode == FlxKey.F || e.keyCode == FlxKey.F4)
			FlxG.fullscreen = !FlxG.fullscreen;
		else {
			
			for (input in onPress.keys()) {
				
				for (key in Inputs.getKeys(input))
					if (key == e.keyCode)
						onPress[input].dispatch();
			}
		}
	}
}