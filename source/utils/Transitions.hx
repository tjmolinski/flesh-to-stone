package utils;

import flixel.FlxG;
import flixel.tweens.FlxEase;
import flixel.tweens.FlxTween;

import states.MapState;

class Transitions {
	
	
	
	inline static function createState(stateType:Class<MapState>, ?args:Array<Dynamic>):MapState {
		
		if (args == null) args = [];
		
		return Type.createInstance(stateType, args);
	}
	
	inline static public function panToType(x:Int, y:Int, stateType:Class<MapState>, ?args:Array<Dynamic>):Void {
		
		panToTypeInTime(x, y, 0.5, stateType, args);
	}
	
	inline static public function panToTypeInTime(x:Int, y:Int, duration:Float, stateType:Class<MapState>, ?args:Array<Dynamic>):Void {
		
		panToStateInTime(x, y, duration, createState(stateType, args));
	}
	
	inline static public function panToState(x:Float, y:Float, state:MapState):Void {
		
		panToStateInTime(x, y, 0.5, state);
	}
	
	inline static public function panToStateInTime(x:Float, y:Float, duration = 0.5, state:MapState):Void {
		
		panToInTimeHelper(x, y, duration, (_)->FlxG.switchState(state));
	}
	
	inline static public function panToInTime(x:Int, y:Int, duration:Float, onComplete:Void->Void):Void {
		
		panToInTimeHelper(x, y, duration, (_)->onComplete());
	}
	
	static function panToInTimeHelper(x:Float, y:Float, duration:Float, onComplete:FlxTween->Void):Void {
		
		var scroll = FlxG.camera.scroll;
		FlxTween.tween(
			scroll,
			{ x: scroll.x + x * FlxG.width, y:scroll.y + y * FlxG.height },
			duration,
			{ onComplete:onComplete
			, ease :FlxEase.sineInOut
			}
		);
	}
	
	inline static public function fadeToTypeInTime(duration, stateType:Class<MapState>, ?args:Array<Dynamic>):MapState {
		
		return fadeToInTime(duration, createState(stateType, args));
	}
	
	inline static public function fadeToTypeInTimeColored(duration, color, stateType, ?args):MapState {
		
		return fadeToInTimeColored(duration, color, createState(stateType, args));
	}
	
	inline static public function fadeTo(state:MapState):MapState {
		return fadeToInTime(0.5, state);
	}
	
	inline static public function fadeToType(stateType:Class<MapState>, ?args:Array<Dynamic>):MapState {
		
		return fadeToInTime(0.5, createState(stateType, args));
	}
	
	static public function fadeToInTime(duration:Float, state:MapState):MapState {
		
		return fadeToInTimeColored(duration, 0xff000000, state);
	}
	
	static public function fadeToInTimeColored(duration:Float, color:Int, state:MapState):MapState {
		
		FlxG.camera.fade(color, duration, false, FlxG.switchState.bind(state));
		return state;
	}
}