package vfx;

import flixel.FlxG;
import flixel.FlxSprite;
import flixel.graphics.FlxGraphic;
import flixel.graphics.frames.FlxTileFrames;
import flixel.math.FlxPoint;
import flixel.system.FlxAssets.FlxGraphicAsset;
import flixel.util.FlxBitmapDataUtil;

class SpriteUtil {
	
	static public function loadAnimationWithSpaces(
		sprite      :FlxSprite,
		source      :FlxGraphicAsset,
		tileSize    :FlxPoint,
		?tileSpacing:FlxPoint,
		unique      = false,
		?key        :String
	):FlxSprite {
		
		if (tileSpacing == null)
			tileSpacing = FlxPoint.get(1, 1);
		
		source = FlxBitmapDataUtil.addSpacesAndBorders(
			FlxG.bitmap.add(source, false).bitmap,
			tileSize,
			tileSpacing
		);
		var graph:FlxGraphic = FlxG.bitmap.add(source, unique, key);
		sprite.width = tileSize.x;
		sprite.height = tileSize.y;
		sprite.frames = FlxTileFrames.fromGraphic(graph, tileSize, null, tileSpacing);
		return sprite;
	}
}