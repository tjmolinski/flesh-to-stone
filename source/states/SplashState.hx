package states;

import flixel.util.FlxTimer;
import flixel.FlxG;
import flixel.FlxSprite;

import openfl.events.MouseEvent;

import utils.Sounds;
import utils.Transitions;
import vfx.Explosion;

class SplashState extends flixel.FlxState {
	
	var splash:FlxSprite;
	
	override function create() {
		super.create();
		
		Sounds.init();
		
		FlxG.camera.bgColor = FlxG.stage.color;
		
		splash = new FlxSprite(0, 0);
		splash.loadGraphic("assets/images/ui/splash.png", true, 40, 64);
		splash.animation.add("idle", [0, 1, 2, 1], 10);
		splash.animation.add("action", [0, 3, 4, 5, 6], 10, false);
		splash.animation.add("reaction", [7, 8, 8, 8, 8, 8, 8, 8], 10, false);
		splash.animation.play("idle");
		splash.scale.set(2, 2);
		splash.x = (FlxG.width  - splash.width ) / 2 + 1;
		splash.y = (FlxG.height - splash.height) / 2 - 6;
		add(splash);
		
		FlxG.stage.addEventListener(MouseEvent.CLICK, onClick);
	}
	
	function onClick(e:MouseEvent) {
		
		if (splash.animation.name == "idle") {
			
			FlxG.stage.removeEventListener(e.type, onClick);
			
			#if !debug
			if (FlxG.onMobile)
				FlxG.fullscreen = true;
			#end
			
			splash.animation.play("action");
			splash.animation.finishCallback = function (anim) {
				
				if (anim == "action") {
					
					Sounds.play(HIT, 3);
					splash.animation.play("reaction");
					
				} else if (anim == "reaction") {
					
					splash.animation.finishCallback = null;
					outro();
				}
			}
		}
	}
	
	function outro():Void {
		
		new FlxTimer().start(0.125, onTimerLoop, 16);
	}
	
	function onTimerLoop(timer:FlxTimer):Void {
		
		if (timer.loopsLeft == 8) {
			
			if (Main.RUN_DEBUG_TEST)
				FlxG.switchState(new DebugState());
			if (Main.SKIP_MENU)
				Transitions.fadeToInTimeColored(1, 0xFFffffff, new PlayState(Story(1)));
			else
				Transitions.fadeToTypeInTimeColored(1, 0xFFffffff, MenuState);
			
		} else {
			
			var explosion = new Explosion();
			explosion.scale.set(4, 4);
			explosion.spawn(
				FlxG.random.float(0.15, 0.85) * FlxG.width,
				FlxG.random.float(0.15, 0.85) * FlxG.height
			);
			add(explosion);
		}
	}
}