package states;

import flixel.FlxG;
import flixel.tweens.FlxEase;
import flixel.tweens.FlxTween;
import flixel.util.FlxTimer;

#if newgrounds
import io.newgrounds.NG;
#end

import data.Levels;
import data.WaterMap;
import states.LevelSelectState;
import ui.Button.FullscreenButton;
import ui.Button.SoundButton;
import ui.ControlInstructions;
import ui.CreditsButtons;
import ui.Inputs;
import ui.MainMenuButtons;
import ui.SettingsButtons;
import ui.TextSequence;
import utils.Sounds;
import utils.Transitions;
import vfx.Title;

class MenuState extends MapState
{
	static var skipNg = false;
	static var isFirstShow = true;
	
	private var title:Title;
	private var mainButtons:MainMenuButtons;
	private var settingsButtons:SettingsButtons;
	private var creditsButtons:CreditsButtons;
	private var instructions:ControlInstructions;
	private var forceMusic:Bool;
	
	#if FLX_TOUCH
	private var soundButton:SoundButton;
	private var fullscreenButton:FullscreenButton;
	#end
	
	public function new (forceMusic = false){
		super();
		
		this.forceMusic = forceMusic || isFirstShow;
		if (this.forceMusic && FlxG.sound.music != null)
			FlxG.sound.music.stop();
		
	}
	override public function create():Void
	{
		super.create();
		
		Sounds.init();
		
		// FlxG.debugger.drawDebug = true;
		
		add(title = new Title());
		
		add(mainButtons = new MainMenuButtons
			( transitionToNormalPlay
			, transitionToEndless
			, transitionToSettings
			, transitionToCredits
			)
		);
		add(settingsButtons = new SettingsButtons(transitionFromSettings));
		add(creditsButtons = new CreditsButtons(transitionFromCredits));
		
		if (!FlxG.onMobile)
		{
			add(instructions = new ControlInstructions());
			#if (newgrounds && debug)
				cast(mainButtons, MainMenuDesktopButtons).addDebugNgButton(logInOutHandler);
			#end
		}
		
		if (isFirstShow)
			startFirstIntro(showTitleIntro);
		else
			showTitleIntro();
		
		#if flixel_studio
			flixel.addons.studio.FlxStudio.create();
		#end
	}
	
	function startFirstIntro(onComplete:()->Void):Void
	{
		var level = Levels.getHighestPlayedLevel();
		var world = Std.int(level / 3);
		map.switchLevel(level);
		map.switchTiles(world);
		var water = new WaterMap(world, 20);
		water.clearBottomRow();
		water.y -= FlxG.height;
		FlxG.camera.scroll.y -= FlxG.height;
		
		var text = new TextSequence();
		text.add(water);
		add(text);
		
		new FlxTimer().start(0.5, (_)->Sounds.playMusic("ur-anime-gf-just-got-killed"));
		
		function onTextComplete()
		{
			FlxTween.tween(FlxG.camera.scroll, { y:0 }, 1.0,
				{ ease: FlxEase.quadInOut, onComplete: (_)-> { remove(text); onComplete(); } }
			);
		}
		
		var msgs
			= if (Levels.getHighestPlayedLevel() >= 3)
				[ "YOU ARE PERSISTENT"
				, "THAT WILL HELP"
				, "RETURN SOUTH..."
				, "TO THE ISLES OF BEASTS"
				, "YOUR PRIZE AWAITS"
				];
			else
				[ "YOU ARE STRONG..."
				, "BUT NOT STRONG ENOUGH"
				, "HEAD SOUTH..."
				, "TO THE ISLES OF BEASTS"
				, "THEY GUARD AN\nANCIENT POWER"
				];
		
		text.show(msgs, onTextComplete, Levels.seenFtue);
	}
	
	function showTitleIntro():Void
	{
		title.startIntro(0.75, onTitleIntroComplete);
	}
	
	inline function onTitleIntroComplete() {
		
		#if newgrounds
			#if debug
				if (!FlxG.onMobile)
					// allow log in/out on debug mode for testing
					checkNg(function() {
						if(NG.core.loggedIn)
							setLogInOutText("DEBUG LOG OUT", false);
						startButtonsIntro();
					});
				else
					checkNg(startButtonsIntro);
			#else
				checkNg(startButtonsIntro);
			#end
		#else
			startButtonsIntro();
		#end
	}
	
	#if newgrounds
		function checkNg(onComplete:Void->Void):Void {
			
			if (skipNg || NG.core.loggedIn)
				onComplete();
			else {
				
				skipNg = true;
				
				var ui = new ui.NgConnector();
				add(ui);
				ui.start(function(){
					remove(ui);
					onComplete();
				});
			}
		}
		
		#if debug
			function logInOutHandler():Void {
				
				if (NG.core.loggedIn) {
					
					trace('logging out');
					NG.core.logOut(setLogInOutText.bind("DEBUG LOG IN"));
					
				} else {
					
					trace('logging in');
					mainButtons.visible = false;
					skipNg = false;
					checkNg(function () {
						
						mainButtons.visible = true;
						setLogInOutText("DEBUG LOG OUT");
					});
				}
			}
			
			function setLogInOutText(text:String, ?activateButtons = true):Void {
				
				cast (mainButtons, MainMenuDesktopButtons).setNgText(text);
				if (activateButtons)
					mainButtons.active = true;
			}
		#end
	#end
	
	function startButtonsIntro():Void {
		
		if (isFirstShow && instructions != null) {
			
			instructions.show();
			isFirstShow = false;
		}
		
		if (forceMusic)
			Sounds.playMusic("WasteTimeOnline");
		
		#if FLX_TOUCH
			Inputs.touchMode = NONE;
			if (FlxG.onMobile)
			{
				add(soundButton = new SoundButton(137, 135));
				soundButton.startIntro();
				add(fullscreenButton = new FullscreenButton(4, 135));
				fullscreenButton.startIntro();
			}
		#end
		
		mainButtons.startIntro();
	}
	
	function toggleFullScreen():Void {
		
		FlxG.fullscreen = !FlxG.fullscreen;
	}
	
	function transitionToNormalPlay():Void {
		
		if (Levels.seenFtue)
			Transitions.panToState(1, 0, new LevelSelectState(false));
		else
			startOutro(FlxG.switchState.bind(new TutorialState(true)));
	}
	
	function transitionToEndless():Void {
		
		startOutro(FlxG.switchState.bind(new EndlessState(true)));
	}
	
	function transitionToSettings():Void {
		
		mainButtons.startOutro(settingsButtons.startIntro.bind());
	}
	
	function transitionFromSettings():Void {
		
		settingsButtons.startOutro(mainButtons.startIntro.bind());
	}
	
	function transitionToCredits():Void {
		
		startQuickOutro(creditsButtons.startIntro.bind(), true);
	}
	
	function transitionFromCredits():Void {
		
		var activateMobileButtons:Null<()->Void> = null;
		#if FLX_TOUCH
		if (FlxG.onMobile)
			activateMobileButtons = function()
			{
				fullscreenButton.startIntro(0.25);
				fullscreenButton.active = true;
			}
		#end
		
		creditsButtons.startOutro(
			function () {
				
				Sounds.playMusic("WasteTimeOnline");
				title.startQuickIntro();
				mainButtons.startIntro(0.25, activateMobileButtons);
			}
		);
	}
	
	/**
	 * Remove menu assets for transitioning to a state on this same island
	 * @param callback 
	 */
	inline function startOutro(callback:()->Void):Void {
		
		FlxG.sound.defaultMusicGroup.pause();
		startQuickOutro(callback);
	}
	
	/**
	 * Hide menu assets for transitioning to a state on this same island
	 * @param callback 
	 */
	inline function startQuickOutro(?callback:()->Void, keepSound = false):Void {
		
		mainButtons.startOutro();
		#if FLX_TOUCH
		if (FlxG.onMobile)
		{
			if (!keepSound)
			{
				soundButton.startOutro();
				soundButton.active = false;
			}
			fullscreenButton.startOutro();
			fullscreenButton.active = false;
		}
		#end
		
		title.startOutro(0.25, callback);
	}
}