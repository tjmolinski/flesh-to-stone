package entities;

import vfx.Warning;

interface IExitable {
	function exit(?sprite:Warning, ?callback:()->Void):Void;
}

interface IMurderable extends IExitable {
	function murder():Void;
	
}