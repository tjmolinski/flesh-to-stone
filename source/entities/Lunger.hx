package entities;

import utils.Sounds;
import flixel.math.FlxPoint;
import flixel.math.FlxVector;
import flixel.util.FlxColor;

import vfx.Explosion;
import vfx.Warning;

/**
 * ...
 * @author TJ
 */
 enum LungerStates {
	CHASING;
	LUNGING;
	ANTICIPATING;
	STAGGER;
	STUNNED;
 }

class Lunger extends Enemy 
{
	inline static var chaseSpeed = 20;
	inline static var lungeSpeed = 100;
	inline static var staggerBounceBack = 50;
	inline static var chaseTime = 2.0;
	inline static var lungeTime = 1.0;
	inline static var stunTime = 0.5;
	inline static var staggerTime = 0.5;
	inline static var anticipateTime = 1.0;
	inline static var anticipateBlinkTime = 0.3;
	
	private var myTarget:PBJMouse;
	private var lungeTarget:FlxVector;
	private var halfWidth:Float = 0.0;
	private var halfHeight:Float = 0.0;

	private var _currentState:LungerStates;
	private var direction:Float = 90.0;
	
	private var chaseTimeBuffer:Float = 0.0;
	private var lungeTimeBuffer:Float = 0.0;
	private var stunTimeBuffer:Float = 0.0;
	private var staggerTimeBuffer:Float = 0.0;
	private var anticipateTimeBuffer:Float = 0.0;
	
	private var explosion:Explosion;

	public var isLunging(get, null):Bool = false;
	function get_isLunging():Bool
	{
		return _currentState == LUNGING;
	}

	@:keep
	public function new()
	{
		super();
		loadGraphic("assets/images/enemies/lunger.png", true, 8, 8);
		animation.add("up_run", [18, 19], 5, true);
		animation.add("side_run", [0, 1], 5, true);
		animation.add("down_run", [9, 10], 5, true);
		animation.add("up_lunge", [20, 21, 22, 23, 24], 5, true);
		animation.add("side_lunge", [2, 3, 4, 5, 6], 5, true);
		animation.add("down_lunge", [11, 12, 13, 14, 15], 5, true);
		animation.add("up_anticipate", [25, 26], 15, true);
		animation.add("side_anticipate", [7, 8], 15, true);
		animation.add("down_anticipate", [16, 17], 15, true);
		animation.add("up_stagger", [29], 5, true);
		animation.add("side_stagger", [27], 5, true);
		animation.add("down_stagger", [28], 5, true);
		animation.add("up_stun", [34, 35], 5, true);
		animation.add("side_stun", [30, 31], 5, true);
		animation.add("down_stun", [32, 33], 5, true);
		animation.play("side_run", true, false, 0);
		halfWidth = width / 2;
		halfHeight = height / 2;
		maxVelocity.set(100, 100);
	}

	public function spawn(pos:FlxPoint, target:PBJMouse, explosion:Explosion, warning:Warning)
	{
		reset(pos.x, pos.y);
		
		solid = true;
		myTarget = target;
		this.explosion = explosion;
		color = FlxColor.WHITE;
		chaseTimeBuffer = 0.0;
		lungeTimeBuffer = 0.0;
		stunTimeBuffer = 0.0;
		staggerTimeBuffer = 0.0;
		anticipateTimeBuffer = 0.0;
		isLunging = false;
		_currentState = CHASING;
		
		playWarningAnim(warning);
	}

	override public function update(elapsed:Float):Void
	{
		super.update(elapsed);
		
		switch(_currentState)
		{
			case CHASING:
				handleChasing(elapsed);
			case LUNGING:
				handleLunging(elapsed);
			case STUNNED:
				handleStunned(elapsed);
			case STAGGER:
				handleStagger(elapsed);
			case ANTICIPATING:
				handleAnticipating(elapsed);
		}
	}
	
	private function handleStagger(elapsed: Float):Void
	{
		if (staggerTimeBuffer >= staggerTime)
		{
			stun();
			return;
		}
		else
		{
			staggerTimeBuffer += elapsed;
		}
	}
	
	private function handleStunned(elapsed: Float):Void
	{
		if (stunTimeBuffer >= stunTime)
		{
			chase();
			return;
		}
		else
		{
			stunTimeBuffer += elapsed;
		}
	}

	private function handleAnticipating(elapsed: Float):Void
	{
		lungeTarget = new FlxVector((myTarget.x + myTarget.halfWidth) - (x + halfWidth), (myTarget.y + myTarget.halfHeight) - (y + halfHeight));
		direction = Math.round(lungeTarget.degrees / 90) * 90;
		
		if (anticipateTimeBuffer >= anticipateTime)
		{
			setColorTransform();
			lunge();
			return;
		}
		else
		{
			anticipateTimeBuffer += elapsed;
			
			if (anticipateTimeBuffer > anticipateTime - anticipateBlinkTime)
			{
				trace('$anticipateTimeBuffer > $anticipateTime - $anticipateBlinkTime');
				final flash = 0xFF * Std.int(anticipateTimeBuffer / 0.04) % 2;
				setColorTransform(1, 1, 1, 1, flash, flash, flash);
			}
		}
	}
	
	private function handleLunging(elapsed: Float):Void
	{
		if (lungeTimeBuffer >= lungeTime)
		{
			velocity.set(0, 0);	
			chase();
			return;
		}
		else
		{
			lungeTimeBuffer += elapsed;
		}

		lungeTarget = lungeTarget.normalize();
		lungeTarget = lungeTarget.scale(lungeSpeed);
		velocity.set(lungeTarget.x, lungeTarget.y);
		direction = Math.round(lungeTarget.degrees / 90) * 90;
	}
	
	private function handleChasing(elapsed: Float):Void
	{
		if (chaseTimeBuffer >= chaseTime)
		{
			velocity.set(0, 0);
			anticipate();
			return;
		}
		else
		{
			chaseTimeBuffer += elapsed;
		}
		
		var vel = new FlxVector((myTarget.x + myTarget.halfWidth) - (x + halfWidth), (myTarget.y + myTarget.halfHeight) - (y + halfHeight));
		vel = vel.normalize();
		vel = vel.scale(chaseSpeed);
		velocity.set(vel.x, vel.y);	
		direction = Math.round(vel.degrees / 90) * 90;
		
		switch(direction) {
			case -180:
				animation.play("side_run", false, false, 0);
				flipX = true;
			case 180:
				animation.play("side_run", false, false, 0);
				flipX = true;
			case 0:
				animation.play("side_run", false, false, 0);
				flipX = false;
			case 90:
				animation.play("down_run", false, false, 0);
			case -90:
				animation.play("up_run", false, false, 0);
		}
	}
	
	public function anticipate():Void
	{
		velocity.set(0, 0);
		switch(direction) {
			case -180:
				animation.play("side_anticipate", true, false, 0);
				flipX = true;
			case 180:
				animation.play("side_anticipate", true, false, 0);
				flipX = true;
			case 0:
				animation.play("side_anticipate", true, false, 0);
				flipX = false;
			case 90:
				animation.play("down_anticipate", true, false, 0);
			case -90:
				animation.play("up_anticipate", true, false, 0);
		}
		anticipateTimeBuffer = 0.0;
		_currentState = ANTICIPATING;
	}

	public function stagger(): Void
	{
		switch(direction) {
			case -180:
				animation.play("side_stagger", true, false, 0);
				flipX = true;
			case 180:
				animation.play("side_stagger", true, false, 0);
				flipX = true;
			case 0:
				animation.play("side_stagger", true, false, 0);
				flipX = false;
			case 90:
				animation.play("down_stagger", true, false, 0);
			case -90:
				animation.play("up_stagger", true, false, 0);
		}
		var vel =  cast(FlxVector.get().copyFrom(velocity), FlxVector);
		vel.normalize();
		vel.scale(staggerBounceBack);
		vel.negate();
		velocity.copyFrom(vel);	
		staggerTimeBuffer = 0.0;
		_currentState = STAGGER;
	}

	public function stun(): Void
	{
		switch(direction) {
			case -180:
				animation.play("side_stun", true, false, 0);
				flipX = true;
			case 180:
				animation.play("side_stun", true, false, 0);
				flipX = true;
			case 0:
				animation.play("side_stun", true, false, 0);
				flipX = false;
			case 90:
				animation.play("down_stun", true, false, 0);
			case -90:
				animation.play("up_stun", true, false, 0);
		}
		velocity.set(0, 0);	
		stunTimeBuffer = 0.0;
		_currentState = STUNNED;
	}

	public function lunge():Void
	{
		switch(direction) {
			case -180:
				animation.play("side_lunge", true, false, 0);
				flipX = true;
			case 180:
				animation.play("side_lunge", true, false, 0);
				flipX = true;
			case 0:
				animation.play("side_lunge", true, false, 0);
				flipX = false;
			case 90:
				animation.play("down_lunge", true, false, 0);
			case -90:
				animation.play("up_lunge", true, false, 0);
		}
		lungeTimeBuffer = 0.0;
		_currentState = LUNGING;
		Sounds.play(LUNGE);
	}

	public function chase():Void
	{
		chaseTimeBuffer = 0.0;
		_currentState = CHASING;
	}

	override function murder():Void
	{
		explosion.spawn(x + halfWidth, y + halfHeight);
		kill();
	}
	
}