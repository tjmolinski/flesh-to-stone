package states;

abstract EndlessState(PlayState) to PlayState
{
	inline public function new(showUiIntro = false)
	{
		this = new PlayState(Endless, showUiIntro);
	}
}