package ui;

import ui.ButtonGroup.ITransitionable;
import flixel.util.FlxTimer;
import ui.BitmapText.NokiaText;
import flixel.group.FlxGroup.FlxTypedGroup;
import utils.Sounds;
import ui.Button.ToggleButton;
import openfl.net.URLRequest;
import openfl.Lib;

class MusicButtonPair extends FlxTypedGroup<Button> implements ITransitionable
{
	public var link(get, never):MusicButton; inline function get_link() return cast this.members[0];
	public var play(get, never):PlayButton ; inline function get_play() return cast this.members[1];
	
	public function new(x, y, artist, title, song, url, onToggle)
	{
		super(2);
		@:privateAccess
		final link = new MusicButton(x, y, artist, title, url);
		this.add(link);
		@:privateAccess
		final play = new PlayButton(x, y, link, song, onToggle);
		this.add(play);
		link.x += play.width - 1;
	}
	
	public function startIntro(delay = 0.0, ?callback:()->Void):Void
	{
		link.startIntro(delay);
		play.startIntro(delay, callback);
	}
	
	public function startOutro(delay = 0.0, ?callback:()->Void):Void
	{
		link.startOutro(delay);
		play.startOutro(delay, callback);
	}
}

class MusicButtonGroup extends FlxTypedGroup<MusicButtonPair> implements ITransitionable
{
	public function new(maxSize = 0)
	{
		super(maxSize);
	}
	
	public function create(x, y, artist, title, song, url):MusicButtonPair
	{
		return add
			( new MusicButtonPair
				( x
				, y
				, artist
				, title
				, song
				, url
				, onButtonToggle.bind(members.length, _)
				)
			);
	}
	
	function onButtonToggle(index:Int, value:Bool):Void
	{
		if (value)
		{
			var i = this.members.length;
			while (i-- > 0)
				if (i != index)
					this.members[i].play.toggled = false;
		}
	}
	
	public function stopAll():Void
	{
		inline onButtonToggle(-1, true);
	}
	
	public function startIntro(delay = 0.0, ?callback:()->Void):Void
	{
		if (members.length == 0)
		{
			delayCallSafe(delay, callback);
			return;
		}
		
		var i = members.length - 1;
		// Call last with callback, only
		members[i].startIntro(delay, callback);
		
		while(i-- > 0)
		{
			delay += 0.1;
			members[i].startIntro(delay);
		}
	}
	
	public function startOutro(delay = 0.0, ?callback:()->Void):Void
	{
		if (members.length == 0)
		{
			delayCallSafe(delay, callback);
			return;
		}
		
		var i = members.length - 1;
		// Call last with callback, only
		members[i].startOutro(delay, callback);
		
		while(i-- > 0)
		{
			delay += 0.1;
			members[i].startOutro(delay);
		}
	}
	
	inline function delayCallSafe(delay:Float, ?callback:()->Void):Void
	{
		if (callback != null)
		{
			if (delay == 0)
				callback();
			else
				new FlxTimer().start(delay, (_)->callback);
		}
	}
}

class PlayButton extends ToggleButton
{
	var musicButton:MusicButton;
	
	function new (x, y, musicButton:MusicButton, song:String, ?onToggle:(Bool)->Void)
	{
		this.musicButton = musicButton;
		
		var callback =
			if (onToggle == null)
				playSong.bind(_, song);
			else (toggle)->
				{
					onToggle(toggle);
					playSong(toggle, song);
				}
		
		super(x, y, None("pauseButton"), None("playButton"), callback);
	}
	
	function playSong(toggled:Bool, name:String):Void
	{
		if (toggled)
			Sounds.playMusic(name);
		else
			Sounds.stopMusic();
	}
}

class MusicButton extends Button
{
	var artist:String;
	var title:String;
	
	function new (x, y, artist:String, title:String, url:String)
	{
		super(x, y, Text(artist, Big, "musicButton", NokiaText.font), goToLink.bind(url));
		label.offset.y--;
	}
	
	inline public function onPlay() setText(title);
	inline public function onPause() setText(artist);
	
	inline function setText(text:String):Void
	{
		(cast label:BitmapText).text = text;
		setLabel(label);
	}
	
	function goToLink(url:String):Void
	{
		Lib.getURL(new URLRequest(url), "_blank");
	}
}