package states;

import data.Tilemap;

import flixel.FlxG;
import flixel.FlxState;

class MapState extends FlxState
{
	var map:Tilemap;
	
	override function create() {
		super.create();
		
		add(map = new Tilemap());
	}
	
	#if debug
	var enableMapSwitching = true;
	
	override function update(elapsed:Float)
	{
		super.update(elapsed);
		
		if (enableMapSwitching == false)
			return;
		
		if (FlxG.keys.justPressed.ZERO ) setDebugLevel(0);
		if (FlxG.keys.justPressed.ONE  ) setDebugLevel(1);
		if (FlxG.keys.justPressed.TWO  ) setDebugLevel(2);
		if (FlxG.keys.justPressed.THREE) setDebugLevel(3);
		if (FlxG.keys.justPressed.FOUR ) setDebugLevel(4);
		if (FlxG.keys.justPressed.FIVE ) setDebugLevel(5);
		if (FlxG.keys.justPressed.SIX  ) setDebugLevel(6);
		if (FlxG.keys.justPressed.SEVEN) setDebugLevel(7);
		if (FlxG.keys.justPressed.EIGHT) setDebugLevel(8);
		if (FlxG.keys.justPressed.NINE ) setDebugLevel(9);
	}
	
	function setDebugLevel(level:Int):Void
	{
		map.switchLevel(level);
		map.switchTiles(Std.int(level / 3));
	}
	#end
}