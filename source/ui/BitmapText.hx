package ui;

import flixel.FlxG;
import flixel.graphics.frames.FlxBitmapFont;
import flixel.math.FlxPoint;
import flixel.math.FlxRect;

@:forward
abstract NokiaText(BitmapText) to BitmapText
{
	static public var font(get, null):FlxBitmapFont = null;
	
	inline public function new (x = 0.0, y = 0.0, text = "", borderColor = 0xFF050914)
	{
		this = new BitmapText(x, y, text, borderColor, font);
	}
	
	inline static function get_font()
	{
		if (font == null)
		{
			@:privateAccess
			font = BitmapText.createNokiaFont();
		}
		return font;
	}
}

@:forward
abstract NokiaMonoText(BitmapText) to BitmapText
{
	static public var font(get, null):FlxBitmapFont = null;
	
	inline public function new (x = 0.0, y = 0.0, text = "", borderColor = 0xFF050914)
	{
		this = new BitmapText(x, y, text, borderColor, font);
	}
	
	inline static function get_font()
	{
		if (font == null)
		{
			@:privateAccess
			font = BitmapText.createNokiaMonoFont();
		}
		return font;
	}
}

@:forward
abstract KOText(BitmapText) to BitmapText
{
	static public var font(get, null):FlxBitmapFont = null;
	
	inline public function new (x = 0.0, y = 0.0, text = "")
	{
		this = new BitmapText(x, y, text, font);
	}
	
	inline static function get_font()
	{
		if (font == null)
		{
			@:privateAccess
			font = BitmapText.createKOFont();
		}
		return font;
	}
}

class BitmapText extends flixel.text.FlxBitmapText
{
	static var mainFont:FlxBitmapFont = null;
	
	@:allow(NokiaText)
	static function createNokiaFont():FlxBitmapFont
	{
		var chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789!#$%&*()-_+=[]',.|:?";
		
		var widths = 
		[
			6,6,6,6,6,6,6,6,3,5,7,5,8,7,7,6,7,6,5,7,6,7,8,7,7,6,	//UPPERCASE
			6,6,5,6,6,4,6,6,3,4,6,3,9,6,6,6,6,5,5,4,6,6,8,6,6,6,	//LOWERCASE
			6,4,6,6,6,6,6,6,6,6,									//DIGITS
			3,6,6,7,7,6,4,4,5,6,6,5,4,4,2,3,3,3,3,6					//SYMBOLS
		];
		
		var font = new FlxBitmapFont(FlxG.bitmap.add("assets/images/text/nokia.png").imageFrame.frame);
		font.lineHeight = 9;
		font.spaceWidth = 4;
		var frame:FlxRect;
		var x:Int = 0;
		for (i in 0...widths.length)
		{
			frame = FlxRect.get(x, 0, widths[i] - 1, font.lineHeight);
			font.addCharFrame(chars.charCodeAt(i), frame, FlxPoint.weak(), widths[i]);
			x += widths[i];
		}
		return font;
	}
	
	@:allow(NokiaMonoText)
	static function createNokiaMonoFont():FlxBitmapFont
	{
		var chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789!#$%&*()-_+=[]',.|:?";
		var size = 7;
		
		var widths = 
		[
			6,6,6,6,6,6,6,6,3,5,7,5,8,7,7,6,7,6,5,7,6,7,8,7,7,6,	//UPPERCASE
			6,6,5,6,6,4,6,6,3,4,6,3,9,6,6,6,6,5,5,4,6,6,8,6,6,6,	//LOWERCASE
			6,4,6,6,6,6,6,6,6,6,									//DIGITS
			3,6,6,7,7,6,4,4,5,6,6,5,4,4,2,3,3,3,3,6					//SYMBOLS
		];
		
		var font = new FlxBitmapFont(FlxG.bitmap.add("assets/images/text/nokia.png").imageFrame.frame);
		font.lineHeight = 9;
		font.spaceWidth = size;
		var frame:FlxRect;
		var x:Int = 0;
		for (i in 0...widths.length)
		{
			frame = FlxRect.get(x, 0, widths[i] - 1, font.lineHeight);
			font.addCharFrame(chars.charCodeAt(i), frame, FlxPoint.weak(), size);
			x += widths[i];
		}
		return font;
	}
	
	static function createGBFont():FlxBitmapFont
	{
		var font = FlxBitmapFont.fromMonospace
		(
			"assets/images/text/gameboy.png",
			"ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789-;',.:?!+[]/",
			FlxPoint.weak(7, 8),
			null,
			FlxPoint.weak(1, 0)
		);
		font.getCharFrame(";".charCodeAt(0)).frame.width = 3;
		font.getCharFrame("'".charCodeAt(0)).frame.width = 3;
		font.getCharFrame("+".charCodeAt(0)).frame.width++;
		font.getCharFrame(".".charCodeAt(0)).frame.width = 3;
		font.getCharFrame(",".charCodeAt(0)).frame.width = 3;
		font.getCharFrame(":".charCodeAt(0)).frame.width = 3;
		font.getCharFrame("[".charCodeAt(0)).frame.width = 4;
		font.getCharFrame("]".charCodeAt(0)).frame.width = 4;
		font.getCharFrame("/".charCodeAt(0)).frame.width = 5;
		return font;
	}
	
	static function createKOFont():FlxBitmapFont
	{
		return FlxBitmapFont.fromMonospace
		(
			"assets/images/text/KO.png",
			"KO",
			FlxPoint.weak(19, 26),
			null,
			FlxPoint.weak(1, 0)
		);
	}
	
	public function new (x = 0.0, y = 0.0, text = "", borderColor = 0xFF050914, ?font:FlxBitmapFont):Void
	{
		if (font == null)
		{
			if (mainFont == null)
				mainFont = createGBFont();
			
			font = mainFont;
		}
		
		super(font);
		
		this.x = x;
		this.y = y;
		this.text = text;
		moves = false;
		active = false;
		
		if (borderColor >= 0xFF000000)
			useDefaultBorder(borderColor);
	}
	
	inline public function useDefaultBorder(borderColor = 0xFF050914):Void
	{
		setBorderStyle(OUTLINE, borderColor, 1, 0);
		lineHeight = font.fontName == mainFont.fontName ? 10 : 9;
	}
	
	public function centerXOnStage():Void
	{
		x = (FlxG.width - width) / 2;
	}
	
	public function centerYOnStage():Void
	{
		y = (FlxG.height - height) / 2;
	}
	
	override function set_alpha(value:Float):Float
	{
		if (borderColor & 0xFF000000 > 0)
			setBorderStyle
				( borderStyle
				, borderColor & 0xffffff | (Std.int(value * 0xFF) << 24)
				, borderSize
				, borderQuality
				);
		
		return super.set_alpha(value);
	}
}
