package utils;

import flixel.FlxG;
import flixel.group.FlxGroup;
import flixel.util.FlxSignal;

typedef SpawnerData =
{
	?max                 :Int,
	?interval            :Float,
	?minInterval         :Float,
	?maxInterval         :Float,
	?delay               :Float,
	?end                 :Float,
	?duration            :Float,
	?spawnsPerInterval   :Int,
	?minSpawnsPerInterval:Int,
	?maxSpawnsPerInterval:Int,
	?duplicationTime     :Float,
	?debugLog            :Bool
}

class Spawner<T:flixel.FlxSprite> extends FlxTypedGroup<T> {
	
	public var max:Int;
	public var delay(default, null):Float;
	public var end(default, null):Float;
	public var duration(get, never):Float;
	public var minInterval(default, null):Float;
	public var maxInterval(default, null):Float;
	public var minSpawnsPerInterval(default, null):Int;
	public var maxSpawnsPerInterval(default, null):Int;
	public var duplicationTime(default, null):Float;
	public var debugLog:Bool;
	
	public var onSpawn(default, never) = new FlxTypedSignal<T->Void>();
	public var onEnd(default, never) = new FlxSignal();
	
	var type:Null<Class<T>>;
	var timer = 0.0;
	var nextSpawn = 0.0;
	var finite = true;
	
	public function new(type:Class<T>, data:SpawnerData = null) {
		
		super(0);
		/* NOTE: super.maxSize is fucking weird and causing issues
		 * use `max` instead of `maxSize` to avoid recycling live goals
		 */
		
		setDefaults();
		this.type = type;
		if (data != null)
			parseData(data);
	}
	
	function setDefaults():Void {
		
		finite = true;
		timer = 0.0;
		nextSpawn = 0.0;
		delay = 0;
		end = -1;
		minInterval = 100000;
		maxInterval = 100000;
		minSpawnsPerInterval = 1;
		maxSpawnsPerInterval = 1;
		duplicationTime = -1;
		debugLog = false;
	}
	
	public function reset(data:SpawnerData)
	{
		setDefaults();
		if (data != null)
			parseData(data);
	}
	
	public function parseData(data:SpawnerData):Void {
		
		setDefaults();
		
		debugLog = data.debugLog == true;
		
		if (data.max != null) {
			
			if (data.max < 0) {
				
				finite = false;
				max = 0;
			} else
				max = data.max;
		}
		
		if (data.interval != null) {
			
			minInterval = data.interval;
			maxInterval = data.interval;
			
		} else {
			minInterval = data.minInterval;
			maxInterval = data.maxInterval;
		}
		
		if (data.delay != null)
			nextSpawn = delay = data.delay;
		
		if (data.duration != null)
			end = delay + data.duration;
		else if (data.end != null)
			end = data.end;
		
		if (data.spawnsPerInterval != null)
			minSpawnsPerInterval = maxSpawnsPerInterval = data.spawnsPerInterval;
		else if (data.minSpawnsPerInterval != null && data.maxSpawnsPerInterval != null) {
			minSpawnsPerInterval = data.minSpawnsPerInterval;
			maxSpawnsPerInterval = data.maxSpawnsPerInterval;
		}
		
		if (data.duplicationTime != null)
			duplicationTime = data.duplicationTime;
	}
	
	override function update(elapsed:Float):Void {
		super.update(elapsed);
		
		if (timer >= end && end >= 0)
			return;
		
		// Increase time faster for every duplication
		if (duplicationTime >= 0 && timer > delay)
			elapsed *= Std.int(1 + (timer - delay) / duplicationTime);
		
		var logInterval = Std.int(timer) != Std.int(timer + elapsed);
		
		timer += elapsed;
		if (timer > nextSpawn) {
			
			var spawns = FlxG.random.int(minSpawnsPerInterval, maxSpawnsPerInterval);
			var logMsg = 'Spawning\n\t- max:$max\n\t- current:${debugCountLiving()}\n\t- num:$spawns';
			spawns = trimSpawns(spawns);
			logMsg += '\n\t- trimmed:$spawns';
			
			if (logInterval)
				logMsg += "\n\t- timer" + Std.int(timer) + "->" + Std.int(timer + elapsed);
			
			if (spawns > 0) {
				
				nextSpawn += FlxG.random.float(minInterval, maxInterval);
				logMsg += '\n\t- next:$nextSpawn';
				
				for (i in 0...spawns)
					spawn();
				
				log(logMsg);
				
			} else if (logInterval)
				log(logMsg);
		}
		
		if (timer >= end)
			onEnd.dispatch();
	}
	
	function spawn():T {
		
		var obj = type != null ? recycle(type) : null;
		onSpawn.dispatch(obj);
		return obj;
	}
	
	inline function trimSpawns(numNew:Int):Int {
		
		if (max == 0)
			return finite ? 0 : numNew;
		
		var count = countLiving();
		// countliving returns -1 if empty, 0 if all members are dead
		if (count < 0)
			count = 0;
		
		if (count + numNew > max)
			numNew = max - count;
		
		return numNew;
	}
	
	inline function get_duration():Float { return end - delay; }
	
	public function stop():Void {
		
		end = 1;
	}
	
	override function destroy() {
		super.destroy();
		
		onSpawn.removeAll();
		onEnd.removeAll();
	}
	
	inline function debugCountLiving():Int {
		return #if debug countLiving() #else 0 #end;
	}
		
	inline function log(msg:String):Void {
		#if debug
		if (debugLog)
			trace(Type.getClassName(type) + " - " + msg);
		#end
	}
}
