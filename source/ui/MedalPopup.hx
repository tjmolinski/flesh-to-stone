package ui;

import utils.Sounds;
import io.newgrounds.NG;

import flixel.FlxG;
import flixel.FlxSprite;
import flixel.effects.FlxFlicker;
import flixel.group.FlxGroup;
import flixel.math.FlxRect;
import flixel.tweens.FlxEase;
import flixel.tweens.FlxTween;

abstract MedalPopup(FlxGroup) to FlxGroup
{
	inline public function new ()
	{
		this = new FlxGroup(2);
	}
	
	public function show(medalId:Int, ?y:Int, callback:()->Void):Float
	{
		var medal = NG.core.medals.get(medalId);
		var icon = new FlxSprite('assets/images/ui/medals_$medalId.png');
		this.add(icon);
		icon.x = (FlxG.width - icon.width) / 2;
		icon.y = -icon.height;
		var text = new BitmapText(0, y, "MEDAL GET!");
		this.add(text);
		text.centerXOnStage();
		if (y == null)
			text.centerYOnStage();
		var toY = text.y;
		text.y = icon.y - text.height;
		text.clipRect = new FlxRect(-1, -1, text.width + 1, text.height + 2);
		function updateClipRect(_){ text.clipRect = text.clipRect; }
		
		// Look, I get it, I need to learn coroutines and async/await stuff
		
		function tween(startDelay = 0.0, target:Dynamic, values:Dynamic, duration = 0.5, ?ease, ?onComplete, ?onUpdate)
		{
			FlxTween.tween
				( target
				, values
				, duration
				,   { startDelay:startDelay
					, ease:ease != null ? ease : FlxEase.backOut
					, onComplete:onComplete
					, onUpdate:onUpdate != null || target != text.clipRect ? onUpdate : updateClipRect
					}
				);
		}
		var totalDelay = 0.0;
		function queueTween(startDelay = 0.0, target:Dynamic, values:Dynamic, duration = 0.5, ?ease, ?onComplete, ?onUpdate)
		{
			totalDelay += startDelay;
			tween(totalDelay, target, values, duration, ease, onComplete);
			totalDelay += duration;
		}
		
		tween(icon, { y:toY + text.height });
		queueTween(text, { y:toY }, (_)->FlxFlicker.flicker(text, 0.5));
		final wipeDuration = 0.5;
		queueTween(0.5, text.clipRect, { x:text.width / 2, width:0 }, wipeDuration, FlxEase.circIn,
			function (_):Void
			{
				text.text = medal.name.toUpperCase();
				text.centerXOnStage();
				tween(text.clipRect, { x:-1, width:text.width }, wipeDuration, FlxEase.circOut, (_)->Sounds.play(CHEER));
			}
		);
		totalDelay += wipeDuration + 0.5;
		
		if (y == null)
			toY = FlxG.height;
		else
			toY = -text.height - icon.height;
		
		tween(totalDelay, icon, { y:toY + text.height }, FlxEase.backIn);
		queueTween(text, { y:toY }, FlxEase.backIn, 
			function (_)
			{
				this.remove(icon);
				this.remove(text);
				callback();
			}
		);
		
		return totalDelay;
	}
}