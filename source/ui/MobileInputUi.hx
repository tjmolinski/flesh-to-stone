package ui;

import flixel.tweens.FlxEase;
import flixel.tweens.FlxTween;
import flixel.system.FlxAssets;
import flixel.FlxG;
import flixel.math.FlxVector;

import openfl.events.Event;
import openfl.display.Bitmap;
import openfl.display.BitmapData;
import openfl.display.Sprite;
import openfl.geom.ColorTransform;
import openfl.geom.Rectangle;

class MobileInputUi extends Sprite {
	
	inline static var HANDLE_RANGE = 10;
	
	static var instance:MobileInputUi;
	
	var anchor:BitmapAnim;
	var handle:BitmapAnim;
	var tap:BitmapAnim;
	
	public function new () {
		super();
		
		instance = this;
		visible = false;
		
		scaleX = scaleY = 4;
		
		addChild(anchor = new BitmapAnim("assets/images/ui/joypad_base.png"));
		anchor.smoothing = false;
		
		addChild(handle = new BitmapAnim("assets/images/ui/joypad_handle.png"));
		handle.smoothing = false;
		
		addChild(tap = new BitmapAnim("assets/images/ui/tap.png"));
		
		addEventListener(Event.ENTER_FRAME, update);
	}
	
	inline function reset():Void {
		
		resetMove();
		resetTap();
	}
	
	inline function resetMove():Void {
		
		@:privateAccess
		anchor.x = RelativeTouchManager.anchorPos.x;
		@:privateAccess
		anchor.y = RelativeTouchManager.anchorPos.y;
	}
	
	inline function resetTap():Void {
		
		@:privateAccess
		tap.x = RelativeTouchManager.actionPos.x;
		@:privateAccess
		tap.y = RelativeTouchManager.actionPos.y;
	}
	
	function update(e:Event) {
		
		tap.update();
		handle.update();
		anchor.update();
		
		if (Inputs.anchor != null && !Inputs.anchor.isZero()) {
			
			anchor.x = Std.int(Inputs.anchor.x);
			anchor.y = Std.int(Inputs.anchor.y);
			
			var dir = FlxVector.get().copyFrom(Inputs.analogDir)
				.scale(HANDLE_RANGE);
				
			handle.x = Std.int(anchor.x + dir.x);
			handle.y = Std.int(anchor.y + dir.y);
		
		} else {
			
			handle.x = anchor.x;
			handle.y = anchor.y;
		}
		
		if (Inputs.justPressed.STONE)
			tap.flash();
	}
	
	function _introduceMove (duration = 1.0, frequency = 0.04):Void
	{
		resetMove();
		anchor.visible = true;
		handle.visible = true;
		anchor.scaleX = 0;
		anchor.scaleY = 0;
		handle.scaleX = 0;
		handle.scaleY = 0;
		FlxTween.tween(anchor, { scaleX:1, scaleY:1} , 0.25, { ease:FlxEase.backOut });
		FlxTween.tween(handle, { scaleX:1, scaleY:1} , 0.25, { ease:FlxEase.backOut, startDelay:0.3,
			onComplete: (_)->handle.flash(duration, frequency) });
	}
	
	function _introduceTap(duration = 1.0, frequency = 0.04):Void {
		
		resetTap();
		tap.visible = true;
		tap.scaleX = 0;
		tap.scaleY = 0;
		FlxTween.tween(tap, { scaleX:1, scaleY:1} , 0.25, { ease:FlxEase.backOut,
			onComplete: (_)->tap.flash(duration, frequency) });
	}
	
	static public function disable():Void {
		
		instance.visible = false;
	}
	
	static public function enable():Void {
		
		instance.visible = true;
		instance.reset();
	}
	
	static public function enableTap():Void {
		
		instance.tap.visible = true;
	}
	
	static public function enableMove():Void {
		
		instance.anchor.visible = true;
		instance.handle.visible = true;
	}
	
	static public function disableTap():Void {
		
		instance.tap.visible = false;
	}
	
	static public function disableMove():Void {
		
		instance.anchor.visible = false;
		instance.handle.visible = false;
	}
	
	inline static public function introduceTap(duration = 1.0, frequency = 0.04):Void {
		
		instance._introduceTap(duration, frequency);
	}
	
	inline static public function introduceMove(duration = 1.0, frequency = 0.04):Void {
		
		instance._introduceMove(duration, frequency);
	}
}

class PixelBitmap extends Bitmap {
	
	override function set_x(value:Float):Float {
		return super.set_x(Std.int(value));
	}
	
	override function set_y(value:Float):Float {
		return super.set_y(Std.int(value));
	}
}

typedef Animation = { fps:Float, frames:Array<Int> };
class BitmapAnim extends Sprite {
	
	public var finishedCallback:Void->Void;
	public var loop = false;
	public var playing(default, null) = true;
	public var frameWidth(default, null):Int;
	public var frameHeight(default, null):Int;
	public var frame(default, null):Int;
	public var smoothing(get, set):Bool;
	inline function get_smoothing() return bitmap.smoothing;
	inline function set_smoothing(value) return bitmap.smoothing = value;
	
	var framesTotal:Int;
	var time = 0.0;
	var flashingTime = 0.0;
	var flashingFrequency = 1.0;
	
	final animations:Map<String, Animation> = new Map();
	var currentAnimName:String = "default_secret_key";
	var currentAnim(get, never):Animation;
	
	var bitmap:Bitmap;
	var bitmapData(get, never):BitmapData;
	inline function get_bitmapData() { return bitmap.bitmapData; }
	
	inline function get_currentAnim() { return animations[currentAnimName]; }
	
	public function new(assetKey:String, frameWidth = 0, frameHeight = 0, center = true) {
		super();
		
		addChild(bitmap = new Bitmap(FlxAssets.getBitmapData(assetKey)));
		
		if (frameWidth  == 0) frameWidth  = bitmapData.width;
		if (frameHeight == 0) frameHeight = bitmapData.height;
		
		if (center) {
			
			bitmap.x -= Std.int(frameWidth / 2);
			bitmap.y -= Std.int(frameHeight / 2);
		}
		
		framesTotal = Math.ceil(bitmapData.width / frameWidth);
		bitmap.scrollRect = new Rectangle(0, 0, frameWidth, frameHeight);
		addAnimation(currentAnimName, [0]);
		frame = 0;
	}
	
	function addAnimation(name:String, frames:Array<Int>, fps = 60.0):Void {
		
		animations[name] = { frames:frames, fps:fps };
	}
	
	function setFrame(frame:Int):Void {
		
		var rect = bitmap.scrollRect;
		rect.x = currentAnim.frames[frame] * frameWidth;
		bitmap.scrollRect = null;
		bitmap.scrollRect = rect;
		this.frame = frame;
	}
	
	public function update():Void {
		
		if (flashingTime > 0) {
			
			flashingTime -= 1 / stage.frameRate;
			if (flashingTime <= 0) {
				
				transform.colorTransform.redOffset
					= transform.colorTransform.greenOffset
					= transform.colorTransform.blueOffset
					= 0;
				
				flashingTime = 0;
				
			} else {
				
				transform.colorTransform.redOffset
					= transform.colorTransform.greenOffset
					= transform.colorTransform.blueOffset
					= Std.int((flashingTime / flashingFrequency) % 2) == 1 ? 0 : 0x80;
			}
		}
		
		if (playing) {
			
			time += 1 / stage.frameRate;
			var spf = 1 / currentAnim.fps;
			if (time > spf) {
				time -= spf;
				
				var frames = currentAnim.frames;
				setFrame(frame == frames.length - 1 ? 0 : frame + 1);
				
				if (frame == frames.length - 1) {
					
					if (!loop)
						playing = false;
					
					if (finishedCallback != null)
						finishedCallback();
				}
			}
		}
	}
	
	public function play(frame = -1):Void {
		
		if (frame >= 0)
			setFrame(frame);
		
		playing = true;
	}
	
	public function stop(frame = -1):Void {
		
		if (frame >= 0)
			setFrame(frame);
		
		playing = false;
	}
	
	public function flash(duration:Float = 1.0, frequency = 0.04):Void {
		
		flashingTime = duration;
		flashingFrequency = frequency;
	}
}