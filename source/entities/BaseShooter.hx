package entities;

import vfx.Warning;
import utils.Sounds;

import openfl.geom.ColorTransform;

import flixel.group.FlxGroup;
import flixel.math.FlxPoint;

enum ShooterAction
{
	Shoot;
	Leave;
}

class BaseShooter extends flixel.FlxSprite
{
	inline static var SPAWN_TIME:Float = 1.00;
	inline static var FLASH_TIME:Float = 0.35;
	
	private var bullets:FlxTypedGroup<Bullet>;
	private var spawnTimeBuffer:Float = 0.0;
	private var level:Int = 0;
	private var halfWidth:Float = 0;
	private var halfHeight:Float = 0;
	private var finishedLaunch:Bool = false;
	private var nextAction:ShooterAction = Shoot;
	
	public function new(x = 0.0, y = 0.0) 
	{
		super(x, y);
		immovable = true;
		moves = false;
		
		loadGraphic("assets/images/enemies/turret.png", true, 8, 8);
		animation.add("launch", [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13], 10, false);
		animation.add("dismiss", [13, 12, 11, 10, 9, 8, 7, 6, 5, 4, 3, 2, 1, 0], 10, false);
		animation.finishCallback = function(anim:String){
			if(anim == "launch")
			{
				finishedLaunch = true;
			}
			else if(anim == "dismiss")
			{
				kill();
			}
		};
		
		halfWidth = width / 2;
		halfHeight = height / 2;
	}
	
	function spawn(pos:FlxPoint, bullets:FlxTypedGroup<Bullet>):Void
	{
		reset(pos.x, pos.y);
		
		level = 0;
		spawnTimeBuffer = 0;
		finishedLaunch = false;
		this.bullets = bullets;
		nextAction = Shoot;
		
		Sounds.play(TURRET);
		animation.play("launch");
	}
	
	public function exit(warnings:FlxTypedGroup<Warning>):Void
	{
		animation.play("dismiss");
		finishedLaunch = true;
		
		bullets.forEachAlive((bullet)->bullet.exit(warnings.recycle(Warning)));
	}
	
	override public function update(elapsed:Float):Void
	{
		super.update(elapsed);
		
		if (!finishedLaunch) {
			return;
		}
		
		spawnTimeBuffer += elapsed;
		if (spawnTimeBuffer > SPAWN_TIME) {
			spawnTimeBuffer -= SPAWN_TIME;
			
			switch(nextAction) {
				case Shoot:
					setColorTransform();
					shootBullets();
					level += 1;
				case Leave:
					animation.play("dismiss");
			}
		} else if (nextAction == Shoot && spawnTimeBuffer > SPAWN_TIME - FLASH_TIME) {
			
			var flash = 0xFF * Std.int(spawnTimeBuffer / 0.04) % 2;
			setColorTransform(1, 1, 1, 1, flash, flash, flash);
		}
	}
	
	function shootBullets():Void {}
	
	inline function playRandomSound():Void
	{
		Sounds.play(SHOOT);
	}
}