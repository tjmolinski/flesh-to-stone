package ui;

import flixel.FlxG;
import flixel.FlxSprite;
import flixel.graphics.frames.FlxBitmapFont;
import flixel.tweens.FlxEase;
import flixel.tweens.FlxTween;
import flixel.ui.FlxButton;

import ui.BitmapText.NokiaText;
import ui.ButtonGroup.ITransitionable;
import utils.Sounds;

enum ButtonSize {
	Big;
	Medium;
	Small;
	//special
	Play;
}

enum ButtonType {
	
	None(graphic:String, ?frames:Int);
	Text(text:String, size:ButtonSize, ?graphic:String, ?font:FlxBitmapFont);
	Icon(key:String, ?graphic:String);
}

class Button extends FlxTypedButton<FlxSprite> implements ITransitionable {
	
	public function new(x, y, type:ButtonType, ?onClick) {
		super(x, y, onClick);
		
		init(type);
		
		onOver.callback = Sounds.play.bind(MENU_NAV);
		scrollFactor.set(1, 1);
		labelAlphas = [1.0, 1.0, 1.0];
		statusAnimations[FlxButton.HIGHLIGHT] = "normal";
		allowSwiping = false;
	}
	
	override function onUpHandler() {
		super.onUpHandler();
		
		// call this because we set active to false to disable buttons for transitions
		updateButton();
		updateStatusAnimation();
		updateLabelPosition();
	}
	
	function init(type:ButtonType):Void
	{
		switch type {
			case None(graphic, frames):
				setGraphic(graphic, frames == null ? 2 : frames);
				label = new FlxSprite(x, y);
				label.makeGraphic(1, 1, 0);
				
			case Icon(icon, graphic):
				if (graphic == null) graphic = "button_19";
				loadGraphic('assets/images/buttons/$graphic.png', true, 19, 21);
				setLabel(new FlxSprite(x, y, 'assets/images/buttons/$icon.png'));
				
			case Text(text, size, graphic, font):
				createTextLabel(text, size, graphic, font);
		}
	}
	
	function createTextLabel(text:String, size:ButtonSize, ?graphic:String, ?font:FlxBitmapFont):BitmapText
	{
		var width = getWidth(size);
		if (graphic == null) graphic = 'button_$width';
		loadGraphic('assets/images/buttons/$graphic.png', true, width);
		var label = new BitmapText(x, y, text, 0, font);
		setLabel(label);
		// ad-hoc hack
		if (font == NokiaText.font && graphic != "musicButton")
			label.offset.y--;
		label.offset.subtract(1, 1);
		return label;
	}
	
	inline function getWidth(size:ButtonSize):Int
	{
		return switch size { case Big: 81; case Medium: 60; case Small: 28; case Play:15; }
	}
	
	public function setGraphic(key:String = null, frames = 2):Void
	{
		loadGraphic('assets/images/buttons/$key.png');
		loadGraphic
			( key == null ? graphic : 'assets/images/buttons/$key.png'
			, true
			, Std.int(graphic.width / frames)
			, graphic.height
			);
	}
	
	inline function setLabel(label:FlxSprite):FlxSprite
	{
		label.offset.x = -Std.int((width - label.width) / 2);
		label.offset.y = -Std.int((height - 2 - label.height) / 2);
		labelOffsets[2].y = 2;
		return this.label = label;
	}
	
	inline public function startIntro(delay = 0.0, ?callback:()->Void):Void
	{
		var options:TweenOptions = { ease:FlxEase.backOut, startDelay: delay };
		if (callback != null)
			options.onComplete = (_)-> callback();
		
		scale.set();
		FlxTween.num(0, 1, 0.25, options,
			(num)->
			{
				scale.set(num, num);
				label.scale.set(num, num);
			}
		);
	}
	
	inline public function startOutro(delay = 0.0, ?callback:()->Void):Void
	{
		var options:TweenOptions = { ease:FlxEase.backIn, startDelay: delay };
		if (callback != null)
			options.onComplete = (_)-> callback();
		
		FlxTween.num(1, 0, 0.25, options,
			(num)->
			{
				scale.set(num, num);
				label.scale.set(num, num);
			}
		);
	}
}

@:forward
abstract SimpleToggleButton(ToggleButton) to ToggleButton
{
	inline public function new(x, y, name:String, onToggle:(Bool)->Void, defaultValue = false)
	{
		@:privateAccess
		this = new ToggleButton
			( x
			, y
			, Icon(name + "_on")
			, Icon(name + "_off")
			, onToggle
			, defaultValue
			);
	}
}

class ToggleButton extends Button
{
	var onType:ButtonType;
	var offType:ButtonType;
	
	var _toggled:Bool; 
	public var toggled(get, set):Bool;
	
	public function new(x, y, onType:ButtonType, offType:ButtonType, onToggle:(Bool)->Void, defaultValue = false)
	{
		_toggled = defaultValue;
		this.onType = onType;
		this.offType = offType;
		
		super(x, y, toggled ? onType : offType, toggle.bind(onToggle));
	}
	
	function toggle(callback:(Bool)->Void):Void
	{
		_toggled = !_toggled;
		init(toggled ? onType : offType);
		
		callback(toggled);
	}
	
	inline function get_toggled() return _toggled;
	
	function set_toggled(value:Bool)
	{
		if (value != toggled)
			onUp.fire();
		
		return value;
	}
}

@:forward
abstract SoundButton(ToggleButton) to Button
{
	inline public function new(x:Float, y:Float)
	{
		this = new SimpleToggleButton
			( x
			, y
			, "sound"
			, (toggled)->{ FlxG.sound.muted = !toggled; }
			, !FlxG.sound.muted
			);
	}
}

@:forward
abstract FullscreenButton(ToggleButton) to Button
{
	inline public function new(x:Float, y:Float)
	{
		this = new SimpleToggleButton
			( x
			, y
			, "fullscreen"
			, (toggled)->{ FlxG.fullscreen = toggled; }
			);
	}
}