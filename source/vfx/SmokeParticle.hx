package vfx;

import flixel.FlxSprite;

/**
 * ...
 * @author TJ
 */
class SmokeParticle extends FlxSprite 
{
	@:keep
	public function new()
	{
		super();
		
		loadGraphic("assets/images/fx/smoke.png", true, 24, 24);
		
		animation.add("life", [0, 1, 2, 3, 4, 5, 6, 7], 15, false);
		animation.finishCallback = function(anim:String) {
			if(anim == "life")
			{
				kill();
			}
	    };
		centerOffsets();
	}
	
	public function spawn(newX:Float, newY:Float, _angle:Float, xSpeed:Float, ySpeed:Float)
	{
		super.reset(newX - (width/2), newY - (height/2));
		angle = _angle;
		velocity.set(xSpeed, ySpeed);
		moves = false;
		animation.play("life");
	}
}