package ui;

import flixel.math.FlxPoint;
import flixel.FlxG;
import flixel.input.touch.FlxTouch;
import flixel.math.FlxVector;

class SwipeManager {
	
	inline static var MIN_SWIPE_DISTANCE = 16;
	
	static public function pressed(input:Input):Bool {
		
		return switch(input) {
			case UP | DOWN | LEFT | RIGHT: checkDir(input);
			case BACK | PAUSE : false;
			case STONE | ACCEPT: checkAction();
		}
	}
	
	
	static function checkDir(input:Input):Bool {
		
		for (swipe in FlxG.swipes) {
			
			if (swipe.distance > MIN_SWIPE_DISTANCE) {
				
				var p = FlxPoint.get()
					.copyFrom(swipe.endPosition)
					.subtractPoint(swipe.startPosition);
				
				return switch(input) {
					case UP    : p.y < -0;
					case DOWN  : p.y >  0;
					case LEFT  : p.x < -0;
					case RIGHT : p.x >  0;
					default:false;
				}
			}
		}
		
		return false;
	}
	
	static function checkAction():Bool {
		
		for (swipe in FlxG.swipes)
			if (swipe.distance < MIN_SWIPE_DISTANCE)
				return true;
		
		return false;
	}
}