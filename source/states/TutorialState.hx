package states;

import flixel.FlxG;
import flixel.math.FlxVector;
import flixel.util.FlxTimer;

import data.Save;
import data.Levels;
import data.TutorialStepData;
import entities.Floater;
import entities.GoalGroup.GoalDifficulty;
import ui.Inputs;
#if FLX_TOUCH
import ui.MobileInputUi;
#end

typedef GoalMapDef = { difficulty:GoalDifficulty, index:Int };

class TutorialState extends PlayState
{
	var keySteps:Array<TutorialStepData>;
	var padSteps:Array<TutorialStepData>;
	var steps:Array<TutorialStepData>;
	var currentStep = -1;
	var stepActive = false;
	var stepsComplete = false;
	var usingKeyboard = true;
	
	public function new (showUiIntro = false):Void
	{
		super(Story(0), showUiIntro);
		
		showBottomControls = false;
	}
	
	override function create()
	{
		super.create();
		
		parseSteps();
		
		#if FLX_TOUCH
		if (FlxG.onMobile)
		{
			MobileInputUi.disableMove();
			MobileInputUi.disableTap();
		}
		#end
	}
	
	override function startIntro()
	{
		super.startIntro();
		
		floaters.exists = true;
		floaters.max = 0;
	}
	
	override function onIntroComplete()
	{
		super.onIntroComplete();
		
		startNextStep();
	}
	
	function parseSteps():Void
	{
		#if FLX_TOUCH
		if (FlxG.onMobile)
			steps = TutorialStepData.arrayFromXml(getSteps("Ftue_Mobile"));
		else
		{
			keySteps = TutorialStepData.arrayFromXml(getSteps("Ftue_Keyboard"));
			padSteps = TutorialStepData.arrayFromXml(getSteps("Ftue_Gamepad"));
			steps = keySteps;
		}
		#else
		keySteps = TutorialStepData.arrayFromXml(getSteps("Ftue_Keyboard"));
		padSteps = TutorialStepData.arrayFromXml(getSteps("Ftue_Gamepad"));
		steps = keySteps;
		#end
	}
	
	function getSteps(name:String):Xml
	{
		var data = null;
		for(group in  Xml.parse(openfl.Assets.getText("assets/data/level.tmx"))
			.elementsNamed("map").next()
			.elementsNamed("group"))
		{
			if (group.get("name") == name)
				data = group;
		}
		
		if (data == null)
			throw 'Missing $name group in level.tmx';
		
		return data;
	}
	
	function startNextStep():Void
	{
		if(currentStep > -1)
		{
			remove(steps[currentStep]);
			steps[currentStep].kill();
		}
		
		Levels.seenFtueStep = currentStep;
		currentStep++;
		
		if (currentStep < steps.length)
		{
			if (!FlxG.onMobile)
				steps = Inputs.usingPad() ? padSteps : keySteps;
			
			var step = steps[currentStep];
			add(step);
			step.startIntro(onStepIntroComplete);
			
			if (step.stoneEnabled != null)
				pbjMouse.stoneEnabled = step.stoneEnabled;
			
			step.forEachOfType(FollowSprite, (sprite)->{ sprite.target = pbjMouse; });
		}
		else 
		{
			if (!hasMetWinConditions())
			{
				// set ftue seen
				var levelNum = switch(level)
				{
					case Story(num): num;
					case _:throw "Expected ftue on level 1";
				}
				Save.postScore(levelNum, score);
				
				goals.endTutorial();
				goals.spawn();
			}
		}
	}
	
	function onStepIntroComplete():Void
	{
		var step = steps[currentStep];
		
		#if FLX_TOUCH
		switch (step.mobileHilite)
		{
			case null:
			case "move": MobileInputUi.introduceMove();
			case "stone": MobileInputUi.introduceTap();
			case string: throw 'unexpected mobileHilite: $string';
		}
		#end
		
		switch (step.enemyConfig)
		{
			case null:
			case 0:
				for (i in 0...16)
				{
					@:privateAccess
					var floater:TutorialFloater = floaters.spawn();
					floater.setupRadialPath((i >= 8 ? i + 4 : i) / 8 * Math.PI * 2, 3 + i * 0.5);
				}
			case 1:
				for (i in 0...14)
				{
					@:privateAccess
					var floater:TutorialFloater = floaters.spawn();
					floater.setupRedRover(i / 14);
				}
			case num:
				throw 'unexpected enemyConfig: $num';
		}
		
		stepActive = true;
		
		if (step.enemiesEnabled == true)
		{
			floaters.max = 50;
			@:privateAccess
			floaters.timer = 0;
			enableEnemySpawners();
		}
		
		if (step.goalGroupName != null && step.goalGroupName != "")
			goals.spawn(step.goalGroupName);
		
		switch (step.waitAction)
		{
			case "goals" if (step.goalGroupName != null):
				goals.onClear.addOnce(endCurrentStep.bind(0.5));
			case time if (Std.parseFloat(time) > 0): 
				endCurrentStep(Std.parseFloat(time));
			// case "move":
		}
	}
	
	function endCurrentStep(delay = 0.5):Void
	{
		if (!stepActive)
			throw 'step already ending';
		stepActive = false;
		if (delay > 0)
			new FlxTimer().start(delay, (_)->steps[currentStep].startOutro(startNextStep));
		else
			steps[currentStep].startOutro(startNextStep);
	}
	
	override function update(elapsed:Float)
	{
		super.update(elapsed);
		
		if (stepActive)
		{
			switch (steps[currentStep].waitAction)
			{
				case "move":
					if (pbjMouse.velocity.x != 0 || pbjMouse.velocity.y != 0)
						endCurrentStep(0.5);
				case "stone":
					if (pbjMouse.isStatue())
						endCurrentStep(0.5);
				case "enemies":
					if (floaters.countLiving() == 0)
						endCurrentStep(0);
				case "goals" if (steps[currentStep].goalGroupName == null):
					if (goals.countLiving() == 0)
						endCurrentStep(0);
			}
		}
	}
}

@:forward
abstract TutorialFloater(Floater) to Floater from Floater
{
	public function setupRadialPath(angle:Float, secondsToCenter:Float):Void
	{
		(this.velocity:FlxVector).radians = Math.PI + angle;
		this.x = FlxG.width / 2 - this.velocity.x * secondsToCenter;
		this.y = FlxG.height / 2 - this.velocity.y * secondsToCenter;
	}
	
	public function setupRedRover(heightPercent:Float, rightToLeft = true):Void
	{
		this.velocity.x = (this.velocity:FlxVector).length;
		this.velocity.y = 0;
		if (rightToLeft)
			this.velocity.x *= -1;
		
		this.x = FlxG.width + this.width;
		this.y = 4 * 8 + (FlxG.height - 6 * 8) * heightPercent;
	}
}
