package vfx;

import ui.BitmapText;
import utils.Sounds;

import flixel.FlxG;
import flixel.util.FlxTimer;
import flixel.math.FlxPoint;
import flixel.tweens.FlxEase;
import flixel.tweens.FlxTween;
import flixel.group.FlxSpriteGroup.FlxTypedSpriteGroup;

typedef Letter = KOText;

@:forward
abstract KO(FlxTypedSpriteGroup<Letter>) to FlxTypedSpriteGroup<Letter> {
	
	public var k(get, never):Letter;
	inline function get_k():Letter { return this.members[0]; }
	public var o(get, never):Letter;
	inline function get_o():Letter { return this.members[1]; }
	
	inline public function new (?center:FlxPoint, animate:Bool = false, ?callback:Void->Void) {
		
		this = new FlxTypedSpriteGroup<Letter>(2);
		
		var k = new Letter("K");
		var o = new Letter("O");
		this.add(k);
		this.add(o);
		
		if (center == null)
			center = FlxPoint.weak(FlxG.width * 0.5, FlxG.height * 0.5);
		
		this.x = center.x;
		this.y = center.y;
		center.putWeak();
		
		k.x = this.x - k.width * k.scale.x - 1;
		k.y = this.y - k.height / 2;
		o.x = this.x;
		o.y = this.y - o.height / 2;
		
		if (animate || callback != null)
		{
			var destY = k.y;
			k.y = -k.height;
			o.y = FlxG.height;
			
			FlxTween.tween(k, { y:destY }, .25, 
				{ ease:FlxEase.quintOut
				, onComplete: function(_) {
						FlxG.camera.shake(0.05, 0.125);
						Sounds.play(DEATH);
					}
				}
			).then(
				FlxTween.tween(o, { y:destY }, .25,
					{ ease:FlxEase.quintOut
					, startDelay: 0.25
					, onComplete: function(_) { 
							FlxG.camera.shake(0.05, 0.125);
							Sounds.play(DEATH);
							new FlxTimer().start(0.5, function (_) { callback(); });
						}
					}
				)
			);
		}
	}
}