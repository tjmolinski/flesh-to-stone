package ui;

class PauseScreen extends flixel.group.FlxGroup {
	
	public function new () {
		super();
		
		var text = new BitmapText(0, 0, "PAUSED");
		text.centerXOnStage();
		text.centerYOnStage();
		add(text);
	}
}