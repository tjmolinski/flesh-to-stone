package vfx;

import flixel.FlxG;
import flixel.FlxSprite;

/**
 * ...
 * @author TJ
 */
class DustParticle extends FlxSprite 
{
	@:keep
	public function new()
	{
		super();
		
		loadGraphic("assets/images/fx/dust.png", true, 8, 8);
		
		animation.add("life", [0, 1, 2, 3, 4, 5], 15, false);
		animation.finishCallback = function(anim:String) {
			if(anim == "life")
			{
				kill();
			}
	    };
		centerOffsets();
	}
	
	public function spawn(newX:Float, newY:Float, flipped:Bool)
	{
		super.reset(newX, newY);
		velocity.set(FlxG.random.float(-20, 20), FlxG.random.float(-20, 20));
		flipX = flipped;
		// moves = false;
		animation.play("life");

		return this;
	}
}