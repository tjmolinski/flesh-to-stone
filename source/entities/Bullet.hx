package entities;

import utils.Sounds;
import vfx.Warning;
using flixel.util.FlxSpriteUtil;

import flixel.FlxG;
import flixel.FlxSprite;
import flixel.math.FlxPoint;
import flixel.util.FlxColor;

/**
 * ...
 * @author TJ
 */
class Bullet extends FlxSprite implements IMurderable
{
	inline static var SPEED = 25;

	@:keep
	public function new()
	{
		super();
		loadGraphic("assets/images/enemies/bullet.png", true, 8, 8);
		animation.add("life", [0, 1, 2, 3, 4, 5], 10, true);
		animation.play("life", true, false, -1);
		width  -= 2;
		height -= 2;
		centerOffsets();
	}

	public function spawn(newX:Float, newY:Float, angle:Float, newSpeed:Float = SPEED)
	{
		super.reset(newX - (width/2), newY - (height/2));
		solid = true;
		velocity.set(newSpeed, 0);
		velocity.rotate(FlxPoint.weak(0, 0), angle);
	}
	
	public function exit(?sprite:Warning, ?callback:()->Void):Void
	{
		if (sprite != null)
			sprite.exit(this, FlxG.random.float(0, 0.5), callback);
	}

	override public function update(elapsed:Float):Void
	{
		if (!alive)
		{
			exists = false;	
		}
		else if (!isOnScreen())
		{
			kill();
		}
		super.update(elapsed);
	}

	public function murder():Void
	{
		kill();
	}
	
}