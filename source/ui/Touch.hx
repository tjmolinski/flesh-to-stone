package ui;

import flixel.FlxG;
import flixel.input.touch.FlxTouch;
import flixel.math.FlxPoint;
import flixel.math.FlxVector;

@:forward
abstract Touch(FlxTouch) from FlxTouch to FlxTouch {
	
	static var vec1 = new FlxVector();
	
	public var ticks(get, never):Int;
	inline function get_ticks() {
		
		return FlxG.game.ticks - this.justPressedTimeInTicks;
	}
	
	public var duration(get, never):Float;
	inline function get_duration() {
		
		return ticks / 1000;
	}
	
	public var distanceX(get, never):Float;
	inline function get_distanceX() {
		
		return this.screenX - this.justPressedPosition.x;
	}
	
	public var distanceY(get, never):Float;
	inline function get_distanceY() {
		
		return this.screenY - this.justPressedPosition.y;
	}
	
	inline public function getDistance(?v:FlxVector):FlxVector {
		
		if (v == null)
			v = FlxVector.get();
		
		return v.set(distanceX, distanceY);
	}
	
	inline public function getDistanceFrom(origin:FlxPoint, ?v:FlxVector):FlxVector {
		
		if (v == null)
			v = FlxVector.get();
		
		return v.set(this.screenX - origin.x, this.screenY - origin.y);
	}
	
	public var lengthSquared(get, never):Float;
	inline function get_lengthSquared() {
		
		return distanceX * distanceX + distanceY * distanceY;
	}
	
	public var length(get, never):Float;
	inline function get_length() {
		
		return Math.sqrt(lengthSquared);
	}
	
	public var degrees(get, never):Float;
	inline function get_degrees() {
		
		return getDistance(vec1).degrees;
	}
	
	public var radians(get, never):Float;
	inline function get_radians() {
		
		return getDistance(vec1).radians;
	}
}