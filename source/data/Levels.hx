package data;

import data.LevelConfig;

class Levels {
	
	inline static public var LEVELS = 12;
	inline static public var STARTING_SCORE_GOAL = 8;
	inline static public var NEW_GAME_PLUS_SCORE_INCREASE = 4;
	
	static public var endlessBarSize(get, never):Int;
	inline static function get_endlessBarSize():Int
	{
		return STARTING_SCORE_GOAL;
	}
	static public var scoreGoal(get, never):Int;
	inline static function get_scoreGoal():Int
	{
		return STARTING_SCORE_GOAL + Save.newGamePlusLevel * NEW_GAME_PLUS_SCORE_INCREASE;
	}
	static public var unlockScoreGoal(get, never):Int;
	inline static function get_unlockScoreGoal():Int
	{
		return Std.int(scoreGoal / 2);
	}
	
	/** First time user experience */
	static public var seenFtueStep:Int = -1;
	static public var seenFtue(get, never):Bool;
	static function get_seenFtue() { return getScore(0) > 0 || Save.newGamePlusLevel > 0 || Main.SKIP_MENU; }
	
	static public function getConfig(num:Int):LevelConfig {
		
		return numbered[num >= numbered.length ? numbered.length : num];
	}
	
	inline static public function updateSeenScores():Void
	{
		Save.updateSeenScores();
	}
	
	inline static public function getSeenScore(level:Int):Int {
		
		return Save.getSeenScore(level);
	}
	
	inline static public function getSeenScoreRatio(level:Int):Float {
		
		var score = getSeenScore(level);
		if (score >= scoreGoal)
			return 1.0;
		return score / scoreGoal;
	}
	
	static public function getScore(level:Int):Int {
		
		return Save.getScore(level);
	}
	
	static public function getScoreRatio(level:Int):Float {
		
		var score = getScore(level);
		if (score >= scoreGoal)
			return 1.0;
		return score / scoreGoal;
	}
	
	static public function getIsUnlocked(level:Int):Bool {
		
		return Save.getIsUnlocked(level);
	}
	
	static public function getIsSeenUnlocked(level:Int):Bool {
		
		return Save.getIsSeenUnlocked(level);
	}
	
	static public function getUnlockedLevelCount():Int {
		
		return Save.getUnlockedLevelCount();
	}
	
	static public function hasUnlockedPlus():Bool {
		
		return Save.getUnseenCompletedLevelCount() == (Save.newGamePlusLevel + 1) * LEVELS;
	}
	
	static public function getHighestPlayedLevel():Int
	{
		for (i in 1...LEVELS)
		{
			if (getScore(i) == 0)
				return i - 1;
		}
		return 0;
	}
	
	static public function getUnseenProgressLevel():Int
	{
		for (i in 0...LEVELS)
		{
			if (getScore(i) > getSeenScore(i))
				return i;
		}
		return -1;
	}
	
	static public function getUnseenUnlockedLevel():Int
	{
		for (i in 1...LEVELS)
		{
			if (getIsUnlocked(i) && !getIsSeenUnlocked(i))
				return i;
		}
		return -1;
	}
	
	static var numbered:Array<LevelConfig> = 
	[ new LevelConfig()//1-1
		.add(FLOATER  , { max:50, interval:1 })
	, new LevelConfig()//1-2
		.add(FLOATER  , { max:50, interval:0.5 })
		.add(HOMER    , { max:10, interval:5 })
	, new LevelConfig()//1-3
		.add(FLOATER  , { max:50, interval:0.5 })
		.add(HOMER    , { max:10, interval:  5 })
		.add(BEAM     , { max: 3, interval:10, delay:30 })
	, new LevelConfig()//2-1
		.add(HOMER    , { max:10, interval:5 })
		.add(SHOOTER  , { max:-1, interval:15, delay:10 })
		.add(BEAM     , { max: 3, interval:10 })
	, new LevelConfig()//2-2
		.add(SHOOTER  , { max:-1, interval:5 })
		.add(BEAM     , { max:3 , interval:5, delay:10 })
		.add(HOMER    , { max:10, interval:5, delay:20 })
	, new LevelConfig()//2-3
		.add(SHOOTER  , { max:-1, interval:10 })
		.add(HOMER    , { max:10, interval:5, delay:5, duplicationTime: 45 })
		.add(BEAM     , { max:3 , interval:5, delay:10 })
	, new LevelConfig()//3-1
		.add(LATCHER  , { max:20, interval:2 })
		.add(SKYLASER , { max:1 , interval:10, delay: 30 })
		.add(SEEKER   , { max:5 , interval:10 , duplicationTime:15 })
	, new LevelConfig()//3-2
		.add(LATCHER  , { max:20, interval:2 })
		.add(SKYLASER , { max:1, interval:10 })
		.add(BEAM     , { max:3, interval:10, duplicationTime:40 })
	, new LevelConfig()//3-3
		.add(LATCHER  , { max:10, interval:5 })
		.add(SKYLASER , { max:1 , interval:15, delay:15 })
		.add(BEAM     , { max:2, interval:20, duplicationTime:45 })
		.add(FLOATER  , { max:50, interval:1, duplicationTime:30 })
	, new LevelConfig()//10
		.add(LUNGER   , { max:1, interval:5, duplicationTime:20 })
		.add(LATCHER  , { max:20, interval:2 })
	, new LevelConfig()//11
		.add(LUNGER   , { max:2, interval:5, duplicationTime:20 })
		.add(LATCHER  , { max:20, interval:2 })
		.add(HOMER    , { max:10, interval:5 })
	, new LevelConfig()//12
		.add(LUNGER   , { max:3, interval:5, duplicationTime:40 })
		.add(HOMER    , { max:10, interval:5 })
		.add(SKYLASER , { max:1 , interval:20, delay:15 })
	];
	
	static public function getEndlessConfig(difficulty:Int):LevelConfig
	{
		return numbered[endlessConfigOrder[difficulty % endlessConfigOrder.length]];
	}
	
	static var endlessConfigOrder:Array<Int> =
	[ 3, 6, 9
	, 1, 4, 7
	, 5, 8, 11
	, 2, 10
	];
	
	var endlessConfig = new LevelConfig()
		.add(HOMER   , { max:-1, interval:5, delay:30 })
		.add(BEAM    , { max:3 , interval:10, delay:20, duplicationTime:20 })
		.add(FLOATER , { max:20, interval:5.0, duplicationTime:10 })
		.add(SEEKER  , { max:-1 , interval:15 })
		.add(LATCHER , { max:20, interval:20, delay:5, spawnsPerInterval:10 })
		.add(LUNGER  , { max:-1 , interval:20, delay:0 , duplicationTime:100 })
		.add(SHOOTER , { max:-1, interval:10, delay:20 })
		.add(SKYLASER, { max:1, interval:20, delay:60 });
}