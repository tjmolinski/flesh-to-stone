package vfx;

import flixel.FlxCamera;
import flixel.FlxG;
import flixel.FlxSprite;
import flixel.effects.particles.FlxEmitter;
import flixel.group.FlxGroup;
import flixel.group.FlxSpriteGroup.FlxTypedSpriteGroup;
import flixel.tweens.FlxEase;
import flixel.tweens.FlxTween;
import flixel.util.FlxTimer;
import flixel.util.FlxColor;

import openfl.filters.ShaderFilter;

import utils.Sounds;

class Title extends FlxGroup
{
	inline static var Y = 7;
	
	var sprites:FlxTypedSpriteGroup<Word>;
	
	public function new ()
	{
		super();
		
		add(sprites = new FlxTypedSpriteGroup(0, Y));
		sprites.add(new Word(23,  0, "flesh"));
		sprites.add(new Word(50, 33, "to"   ));
		sprites.add(new Word(22, 42, "stone"));
	}
	
	public function startIntro(delay = 0.0, ?onComplete:()->Void):Void
	{
		FlxCamera.defaultCameras = [FlxG.camera];
		var shaderCam = new FlxCamera();
		shaderCam.setFilters([new ShaderFilter(new PixelPerfectSmoke())]);
		shaderCam.bgColor = 0;
		FlxG.cameras.add(shaderCam);
		
		var emitters = new FlxTypedGroup<Emitter>();
		insert(0, emitters);
		var count = 0;
		function show(args:ExplodeArgs, timer:FlxTimer)
		{
			var emitter = sprites.members[count].explode(count, args);
			emitter.camera = shaderCam;
			emitters.add(emitter);
			count++;
		}
		new FlxTimer().start(delay + 0.0, show.bind({ assetKey:"flesh" }));
		new FlxTimer().start(delay + 0.5, show.bind({ assetKey:"to", strength: 0.5 }));
		new FlxTimer().start(delay + 1.0, show.bind({ assetKey:"stone" }));
		new FlxTimer().start(delay + 1.5,
			function(_)
			{
				emitters.kill();
				remove(emitters);
				FlxG.cameras.remove(shaderCam);
				
				if (onComplete != null)
					onComplete();
			}
		);
	}
	
	public function startOutro(delay = 0.0, ?onComplete:()->Void):Void
	{
		var options:TweenOptions = { ease:FlxEase.backIn, startDelay:delay };
		if (onComplete != null)
			options.onComplete = (_) -> onComplete();
		
		var delta = sprites.y + sprites.height;
		FlxTween.tween(sprites, { y:sprites.y - delta }, 0.75, options);
	}
	
	
	public function startQuickIntro(delay = 0.0, ?onComplete:()->Void):Void
	{
		var options:TweenOptions = { ease:FlxEase.backOut, startDelay:delay };
		if (onComplete != null)
			options.onComplete = (_) -> onComplete();
		
		FlxTween.tween(sprites, { y:Y }, 0.75, options);
	}
	
	#if debug
	override function update(elapsed:Float)
	{
		super.update(elapsed);
		
		if (FlxG.keys.justPressed.R)
		{
			sprites.members[0].visible = false;
			sprites.members[1].visible = false;
			sprites.members[2].visible = false;
			startIntro();
		}
	}
	#end
}

typedef ExplodeArgs = { ?assetKey:String, ?color:FlxColor, ?strength:Float };

class Word extends FlxSprite
{
	inline public function new(x:Float, y:Float, asset:String)
	{
		super(x, y, 'assets/images/text/title_$asset.png');
		
		visible = false;
	}
	
	inline public function explode(soundIndex:Int, args:ExplodeArgs):Emitter
	{
		FlxG.cameras.shake((args.strength != null ? args.strength : 1.0) * 0.05, 0.125);
		Sounds.play(DEATH, soundIndex);
		
		visible = true;
		return new Emitter(this, args);
	}
}

@:forward
abstract Emitter(FlxEmitter) to FlxEmitter
{
	inline static var SIZE = 2;
	inline static var NUM = 35;
	inline static var TIME = .5;
	inline static var DISTANCE = 25;
	inline static var BASE_SPEED = DISTANCE / TIME;
	inline static var DRAG = BASE_SPEED / TIME;
	
	inline static var USE_SPAWN_POINT = false;
	
	inline public function new (target:FlxSprite, args:ExplodeArgs):Void
	{
		var strength = args.strength == null ? 1.0 : args.strength;
		var num = Std.int(NUM * strength);
		
		if (USE_SPAWN_POINT)
			this = new FlxEmitter
				( target.x + target.width / 2
				, target.y + target.height / 2
				, num
				);
		else
		{
			this = new FlxEmitter(target.x, target.y, num);
			this.width = target.width;
			this.height = target.height;
		}
		
		if (args.assetKey == null)
			this.makeParticles(1, 1, args.color == null ? FlxColor.WHITE : args.color, num);
		else
			this.loadParticles('assets/images/fx/${args.assetKey}_particle.png', num, 0, true);
		
		this.launchMode = SQUARE;
		this.lifespan.set(0, TIME);
		var speed = BASE_SPEED * strength;
		this.velocity.set(-2 * speed, -speed, 2 * speed, speed);
		this.drag.set(DRAG);
		this.acceleration.set(0, 0, 0, 0, 0, -500, 0, -500);
		// this.alpha.set(1, 1, 0, .5);
		this.scale.set
			( SIZE * 1, SIZE * 1, SIZE * 4, SIZE * 4
			, SIZE / 4, SIZE / 4, SIZE / 2, SIZE / 2
			);
		this.start();
	}
}