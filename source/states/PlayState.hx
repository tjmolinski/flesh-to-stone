package states;

#if FLX_TOUCH
import ui.MobileInputUi;
#end
import entities.GoalGroup.GoalProgression;
import entities.GoalGroup.GoalDifficulty;
import data.ApiData;
import data.Levels;
import data.LevelConfig;
import data.Save;
import entities.*;
import states.PlayStateSuccessOutros.SuccessOutro;
import ui.ControlInstructions;
import ui.HealthHud;
import ui.Inputs;
import ui.PauseScreen;
import ui.ScoreBar;
import utils.Sounds;
import utils.Spawner;
import utils.Transitions;
import vfx.Explosion;
import vfx.KO;
import vfx.Warning;
import vfx.SmokeParticle;
import vfx.DustParticle;

import flixel.FlxG;
import flixel.FlxSprite;
import flixel.group.FlxGroup;
import flixel.math.FlxPoint;
import flixel.tweens.FlxEase;
import flixel.tweens.FlxTween;
import flixel.util.FlxSignal;

enum GameStates {
	INTRO;
	PLAYING;
	DEAD;
	WIN;
}

enum LevelType {
	Story(num:Int);
	Endless;
	Empty(goals:GoalDifficulty);
}

class PlayState extends MapState
{
	inline static var USE_EMPTY_LEVEL = 
		// #if debug !/* uncomment to toggle */ #end
		false;
	
	private var pbjMouse:PBJMouse;
	private var currentState:GameStates;
	private var healthHud:HealthHud;
	private var onFtueComplete:FlxSignal = new FlxSignal();
	
	private var levelConfig:LevelConfig;
	private var spawners:LevelSpawner;
	
	private var bullets:FlxTypedGroup<Bullet>;
	private var explosions:FlxTypedGroup<Explosion>;
	private var warnings:FlxTypedGroup<Warning>;
	private var beamEmitters:FlxTypedGroup<BeamEmitter>;
	private var smokeParticles:FlxTypedGroup<SmokeParticle>;
	private var dustParticles:FlxTypedGroup<DustParticle>;
	
	private var homers:Spawner<Homer>;
	private var beams:Spawner<Beam>;
	private var latchers:Spawner<Latcher>;
	private var floaters:Spawner<Floater>;
	private var seekers:Spawner<Seeker>;
	private var lungers:Spawner<Lunger>;
	private var shooters:Spawner<BaseShooter>;
	private var goals:GoalGroup;
	private var skyLasers:Spawner<SkyLaser>;
	
	private var skyLaserWarnings:FlxTypedGroup<FlxSprite>;
	private var pbjKillers:FlxGroup;
	private var skyLaserKillables:FlxGroup;
	private var collidingEnemies:FlxGroup;
	private var lungerTargets:FlxGroup;
	private var pauseScreen:PauseScreen;
	private var showBottomControls = true;
	
	private var scoreBar:ScoreBar;
	private var score = 0;
	private var showUiIntro:Bool;
	
	public var level:LevelType;
	public var goalPoints:Float;
	
	public function new (level:LevelType, showUiIntro = false)
	{
		if (USE_EMPTY_LEVEL)
			level = Empty(EASY);
		
		this.level = level;
		this.showUiIntro = showUiIntro;
		levelConfig = switch(this.level)
		{
			case Empty(_): new LevelConfig();
			case Story(num): Levels.getConfig(num);
			case Endless: Levels.getEndlessConfig(0);
		}
		
		super();
		#if debug
		enableMapSwitching = false;
		#end
	}
	
	override public function create():Void
	{
		super.create();
		
		FlxG.camera.pixelPerfectRender = true;
		
		Sounds.init();
		#if FLX_TOUCH
		Inputs.touchMode = Inputs.actionTouchMode;
		if (FlxG.onMobile)
			MobileInputUi.enable();
		#end
		
		currentState = INTRO;
		
		add(skyLaserWarnings = new FlxTypedGroup());
		
		bullets = new FlxTypedGroup<Bullet>();
		explosions = new FlxTypedGroup<Explosion>();
		warnings = new FlxTypedGroup<Warning>();
		beamEmitters = new FlxTypedGroup<BeamEmitter>(20);
		smokeParticles = new FlxTypedGroup<SmokeParticle>(100);
		dustParticles = new FlxTypedGroup<DustParticle>();
		
		createSpawners();
		
		lungerTargets = new FlxGroup();
		lungerTargets.add(homers);
		lungerTargets.add(floaters);
		lungerTargets.add(seekers);
		lungerTargets.add(lungers);
		
		pbjKillers = new FlxGroup();
		pbjKillers.add(bullets);
		pbjKillers.add(homers);
		pbjKillers.add(beams);
		pbjKillers.add(floaters);
		pbjKillers.add(seekers);
		
		skyLaserKillables = new FlxGroup();
		skyLaserKillables.add(homers);
		skyLaserKillables.add(bullets);
		skyLaserKillables.add(floaters);
		skyLaserKillables.add(seekers);
		skyLaserKillables.add(latchers);
		skyLaserKillables.add(lungers);
		
		collidingEnemies = new FlxGroup();
		// skyLaserKillables.add(floaters);
		collidingEnemies.add(homers);
		collidingEnemies.add(seekers);
		collidingEnemies.add(latchers);
		collidingEnemies.add(lungers);
		collidingEnemies.add(shooters);
		
		pbjMouse = new PBJMouse(FlxG.width / 2, -200, dustParticles);
		pbjMouse.onMurdered.add(onPlayerDie);
		add(dustParticles);
		add(pbjMouse);
		
		createUi();
		
		startIntro();
	}
	
	function createSpawners():Void
	{
		spawners = levelConfig.createSpawners();
		homers = spawners.get(HOMER);
		beams = spawners.get(BEAM);
		floaters = spawners.get(FLOATER);
		seekers = spawners.get(SEEKER);
		latchers = spawners.get(LATCHER);
		lungers = spawners.get(LUNGER);
		shooters = spawners.get(SHOOTER);
		skyLasers = spawners.get(SKYLASER);

		add(warnings);
		
		final progression:GoalProgression = switch level
		{
			case Empty(goals): STEADY(goals);
			case Endless     : CYCLE(Levels.endlessBarSize);
			case Story(num)  : RETRO_FIT(GoalGroup.diffList[num % 3]);
		}
		add(goals = new GoalGroup(Levels.seenFtue ? progression : TUTORIAL));
		add(shooters);
		add(homers);
		add(lungers);
		add(latchers);
		add(explosions);
		add(beams);
		add(beamEmitters);
		add(floaters);
		add(seekers);
		add(bullets);
		add(smokeParticles);
		add(skyLasers);
		
		homers.onSpawn.add(function(homer)
		{
			homer.spawn(
				getRandomSpotOnFloor(),
				pbjMouse,
				explosions.recycle(Explosion),
				warnings.recycle(Warning)
			);
		});
		beams.onSpawn.add(function(beam) { beam.init(beamEmitters); });
		floaters.onSpawn.add(function(floater) { floater.init(); });
		seekers.onSpawn.add(function(seeker)
		{
			seeker.init(
				getRandomSpotOnFloor(),
				pbjMouse,
				explosions.recycle(Explosion),
				warnings.recycle(Warning)
			);
		});
		latchers.onSpawn.add(function(latcher) { latcher.init(pbjMouse); });
		lungers.onSpawn.add(function(lunger)
		{
			lunger.spawn(
				getRandomSpotOnFloor(),
				pbjMouse,
				explosions.recycle(Explosion),
				warnings.recycle(Warning)
			);
		});
		skyLasers.onSpawn.add(function(skyLaser) { skyLaser.spawn(skyLaserWarnings, smokeParticles); });
		shooters.onSpawn.add(function(_) 
		{
			if(FlxG.random.bool())
			{
				(cast shooters.recycle(BulletSpawner):BulletSpawner)
					.init(getRandomSpotOnFloor(), bullets);
			}
			else
			{
				(cast shooters.recycle(Shooter):Shooter)
					.init(getRandomSpotOnFloor(), pbjMouse, bullets);
			}
			
		});
	}
	
	function createUi():Void {
		
		add(healthHud = new HealthHud());
		add(pauseScreen = new PauseScreen())
			.visible = false;
		
		if (!FlxG.onMobile && showBottomControls)
			add(new ControlInstructions(Play, true));
		
		createScoreBar();
	}
	
	function createScoreBar():Void
	{
		add(scoreBar = new ScoreBar(level));
		
		goals.autoSpawn = false;
		goals.onSpawn.add(function () {
			scoreBar.setMaxPellets(goals.total);
		});
		goals.onClear.add(function () {
			
			score++;
			if (hasMetWinConditions())
			{
				currentState = WIN;
				onSuccess();
			}
			else if (level == Endless && score % scoreBar.maxScore == 0)
			{
				increaseEndlessDifficulty(Std.int(score / scoreBar.maxScore));
			}
			
			scoreBar.addPoint(currentState == WIN ? null : goals.spawn.bind());
		});
	}
	
	function increaseEndlessDifficulty(diff:Int):Void
	{
		levelConfig = Levels.getEndlessConfig(diff);
		spawners.reset(levelConfig);
	}
	
	function startIntro():Void
	{
		homers.exists = false;
		beams.exists = false;
		floaters.exists = false;
		seekers.exists = false;
		latchers.exists = false;
		lungers.exists = false;
		goals.exists = false;
		shooters.exists = false;
		skyLasers.exists = false;
		
		if (FlxG.sound.music != null)
			FlxG.sound.music.stop();
		Sounds.playLevelMusic("ldsong", level);
		
		if (showUiIntro)
			scoreBar.startIntro(.25);
		
		pbjMouse.showIntroAnim(onIntroComplete);
	}
	
	function onIntroComplete():Void
	{
		goals.exists = true;
		scoreBar.setMaxPellets(goals.total);
		
		if (Levels.seenFtue)
		{
			enableEnemySpawners();
		}
		
		currentState = PLAYING;
	}
	
	inline function enableEnemySpawners():Void
	{
		homers.exists = true;
		beams.exists = true;
		floaters.exists = true;
		seekers.exists = true;
		latchers.exists = true;
		lungers.exists = true;
		shooters.exists = true;
		skyLasers.exists = true;
	}
	
	override public function update(elapsed:Float):Void
	{
		if (currentState == PLAYING && Inputs.justPressed.PAUSE)
		{
			pauseScreen.visible = !pauseScreen.visible;
			FlxG.timeScale = pauseScreen.visible ? 0 : 1;
			
			if(!pauseScreen.visible)
				return;
		}
		else if(pauseScreen.visible)
			return;
		
		super.update(elapsed);
		
		#if debug
		if (FlxG.keys.justPressed.ONE)
			FlxG.debugger.drawDebug = !FlxG.debugger.drawDebug;
			
		if (FlxG.keys.justPressed.TWO)
			pbjMouse.godMode = !pbjMouse.godMode;
			
		if (FlxG.keys.pressed.FIVE && currentState != WIN)
		{
			score = Levels.scoreGoal;
			
			currentState = WIN;
			onSuccess();
		}
		
		if (FlxG.keys.pressed.NINE)
			pbjMouse.murdered();
		#end
		
		switch(currentState)
		{
			case INTRO:
			case PLAYING:
				handlePlayingState(elapsed);
			case DEAD:
				handleCollisions();
			case WIN:
		}
	}
	
	function handlePlayingState(elapsed:Float):Void
	{
		healthHud.set(pbjMouse.health);
		handleCollisions();
	}
	
	function hasMetWinConditions():Bool
	{
		// check spawners for remaining enemies
		// return spawners != null && spawners.hasMetWinConditions();
		
		// goal limit
		return switch(level)
		{
			case Story(_): score >= Levels.scoreGoal;
			default: false;
		}
	}
	
	public function getRandomSpotOnFloor():FlxPoint {
		
		return FlxPoint.get(
			FlxG.random.int(2, Std.int(FlxG.width  / 8) - 3) * 8,
			FlxG.random.int(4, Std.int(FlxG.height / 8) - 3) * 8
		);
	}
	
	private function handleCollisions():Void
	{
		FlxG.overlap(lungers, lungerTargets, function(obj1: Lunger, obj2:IMurderable) 
		{
			if (obj1.isLunging)
			{
				obj2.murder();
			}
		});
		FlxG.collide(seekers, seekers);
		FlxG.collide(latchers, latchers);
		
		FlxG.overlap(floaters, floaters, function(f1:Floater, f2:Floater) { f1.onCollide(f2); });
		
		if (currentState == DEAD)
			return;
		
		var heroRect = pbjMouse.getRect();
		FlxG.collide(heroRect, map);
		pbjMouse.setFromRect(heroRect);
		
		FlxG.overlap(heroRect, goals, function(_, goal: Goal) {
			
			pbjMouse.collectedGoal();
			
			if (scoreBar != null && scoreBar.visible)
				goal.collectAndGo(scoreBar.getPelletDestination(), this, scoreBar.setDisplayNumPellets);
			else
				goal.collect();
			
			if (score == scoreBar.maxScore - 1 && level != Endless && goals.countUncollected() <= 0)
				pbjMouse.active = false;
			
			if (!Levels.seenFtue)
				onFtueComplete.dispatch();
		});
		
		FlxG.overlap(heroRect, pbjKillers, function(_, killer:FlxSprite) {
			
			if (Std.is(killer, IMurderable))
				(cast killer:IMurderable).murder();
			pbjMouse.hit();
		});

		FlxG.overlap(heroRect, latchers, function(_, latcher:Latcher) {
			pbjMouse.latch(latcher);
		});

		if (FlxG.overlap(heroRect, skyLasers))
		{
			pbjMouse.hit();
		}

		FlxG.overlap(heroRect, lungers, function(_, obj2: Lunger) {
			if (obj2.isLunging)
			{
				if (pbjMouse.isStatue())
				{
					obj2.stagger();
				}
				else
				{
					pbjMouse.hit();
				}
			}
		});
		
		function checkOffLand(enemy:Enemy) { enemy.checkOffLand(map); }
		homers.forEachAlive(checkOffLand);
		seekers.forEachAlive(checkOffLand);
		lungers.forEachAlive(checkOffLand);
		
		if (!pbjMouse.isStatue())
		{
			FlxG.overlap(heroRect, beams, function(_, beam:Beam) { pbjMouse.murdered(); });
		}
		
		FlxG.overlap(skyLasers, skyLaserKillables, function(_, enemy:IMurderable) { enemy.murder(); } );
		
		FlxG.collide(collidingEnemies, collidingEnemies);
	}
	
	function onPlayerDie():Void
	{
		currentState = DEAD;
		var unlockedLevel = false;
		var unlockedMedal:Null<Int> = null;
		var newBest = false;
		if (Levels.seenFtue)
		{
			switch (level)
			{
				case Story(num):
					unlockedLevel = !Save.getIsUnlocked(num + 1);
					Save.postScore(num, score);
					unlockedLevel = unlockedLevel && Save.getIsUnlocked(num + 1);
				case Endless:
					newBest = Save.endlessScore < score;
					if (Save.endlessScore < 24 && score >= 24)
						unlockedMedal = ApiData.medal_endless24;
					Save.postEndlessScore(score);
				default:
			}
		}
		
		var ko:KO;
		function transitionDown()
		{
			#if FLX_TOUCH
			if (FlxG.onMobile)
				MobileInputUi.disable();
			#end
			
			scoreBar.setNoScrollMode();
			ko.scrollFactor.set();
			#if FLX_TOUCH
			if (FlxG.onMobile)
				FlxTween.tween(ko, { y:ko.y - 16 }, 0.5, { ease:FlxEase.sineInOut });
			#end
			Transitions.panToInTime(0, 1, 0.5,
				function ()
				{
					FlxG.switchState(new EndState(
						{ prevLevel:level
						, score: score
						, unlockedLevel:unlockedLevel
						, unlockedMedal:unlockedMedal
						, newBest:newBest
						, koX:ko.x
						, koY:ko.y
						}
					));
				}
			);
		}
		add(ko = new KO(transitionDown));
	}
	
	function onSuccess():Void
	{
		pbjMouse.active = false;
		var levelNum = switch(level)
		{
			case Story(num): num;
			case _: -1;
		}
		
		var alreadyBeaten = Levels.getScore(levelNum) >= score;
		
		Sounds.play(SUCCESS);
		Save.postScore(levelNum, score);
		
		var outroType:SuccessOutro = 
			if (alreadyBeaten) Normal;
			else if (Save.getUnseenCompletedLevelCount() == Levels.LEVELS)
				Final;
			else switch(levelNum % 3)
			{
				case 2: Epic;
				case 1: Cool;
				case 0,_: Normal;
			};
		
		// really hacky way of moving all the outro logic to another file
		(this:PlayStateSuccessOutros).playOutro(outroType, toLevelSelect.bind(levelNum + 1));
	}
	
	function toLevelSelect(autoSelectLevel:Int):Void
	{
		#if FLX_TOUCH
		if (FlxG.onMobile)
			MobileInputUi.disable();
		#end
		
		Transitions.panToStateInTime(1, 0, 0.25, new LevelSelectState(autoSelectLevel));
	}
}