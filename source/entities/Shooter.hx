package entities;

import flixel.FlxG;
import flixel.math.FlxPoint;
import flixel.math.FlxVector;
import flixel.group.FlxGroup;

/**
 * ...
 * @author TJ
 */
class Shooter extends BaseShooter 
{
	inline static var BULLET_SPEED = 50;
	
	private var target:PBJMouse;
	
	public function new(x = 0.0, y = 0.0) { super(x, y); }
	
	public function init(pos:FlxPoint, target:PBJMouse, bullets:FlxTypedGroup<Bullet>):Void {
		
		spawn(pos, bullets);
		
		this.target = target;
	}
	
	override function shootBullets():Void
	{
		fireAtPlayer();
		// This is the logic to handle multiple shootings before going away
		switch(level)
		{
			case 0:
				if (FlxG.random.bool(75))
					nextAction = Leave;
			case 1:
				if (FlxG.random.bool(90))
					nextAction = Leave;
			case 2:
				nextAction = Leave;
		}
	}
	
	private function fireAtPlayer():Void
	{
		if (target != null)
		{
			var targetVector = new FlxVector((target.x + target.halfWidth) - (x + halfWidth), (target.y + target.halfHeight) - (y + halfHeight));
			targetVector = targetVector.normalize();
			var angle = targetVector.degrees;
			
			bullets.recycle(Bullet).spawn(x + halfWidth, y + halfHeight, angle, BULLET_SPEED);
			if (FlxG.random.bool())
			{
				bullets.recycle(Bullet).spawn(x + halfWidth, y + halfHeight, angle-15, BULLET_SPEED);
				bullets.recycle(Bullet).spawn(x + halfWidth, y + halfHeight, angle+15, BULLET_SPEED);
			}
		}
	}
}