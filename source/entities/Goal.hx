package entities;

import flixel.tweens.FlxTween;
import flixel.group.FlxGroup;
import flixel.addons.effects.FlxTrail;
import flixel.FlxG;
import flixel.FlxSprite;
import flixel.math.FlxPoint;
import flixel.util.FlxColor;

/**
 * ...
 * @author TJ
 */
class Goal extends FlxSprite
{
	public function new(?X:Float=0, ?Y:Float=0) 
	{
		super(X, Y);
		
		loadGraphic("assets/images/collectable.png", true, 10, 10);
		animation.add("spawn", [0, 1, 2, 3], 10, false);
		animation.add("idle", [4, 5, 6, 7, 8], 10, true);
		animation.add("collected", [9, 10, 11, 12, 13], 10, false);
		animation.finishCallback = function(anim:String){
			if(anim == "spawn")
			{
				solid = true;
				animation.play("idle", false, false, 0);
			}
			if(anim == "collected")
			{
				kill();
			}
		};
	}
	
	public function spawn(pos:FlxPoint):Void
	{
		reset(pos.x, pos.y);
		solid = false;
		animation.play("spawn", true, false, 0);
	}
	
	public function collect():Void
	{
		solid = false;
		alive = false;
		animation.play("collected", true, false, 0);
	}
	
	public function collectAndGo(dest:FlxPoint, ?trailParent:FlxGroup, ?onComplete:Void->Void):Void
	{
		solid = false;
		
		var trail:Null<FlxTrail>;
		// if (trailParent != null)
		// 	trail = new FlxTrail(this);
		
		FlxTween.tween
			( this
			, { x:dest.x - width / 2, y:dest.y - height / 2 }
			, 0.25
			,   { onComplete: function (_)
					{
						if (trail != null)
							trail.destroy();
						
						animation.play("collected", true, false, 0);
						alive = false;
						
						if (onComplete != null)
							onComplete();
					}
				}
			);
	}
}