package ui;

import data.Save;
import flixel.FlxBasic;
import flixel.FlxG;
import flixel.effects.FlxFlicker;

import data.Levels;
import ui.ButtonGroup.ITransitionable;
import utils.Sounds;

@:forward
abstract MainMenuButtons(FlxBasic) to FlxBasic
{
	var thisAsButtons(get, never):ITransitionable;
	inline function get_thisAsButtons() return cast this;
	
	inline public function new(onPlay, onEndless, onSettings, onCredits)
	{
		#if FLX_TOUCH
		if (FlxG.onMobile)
			@:privateAccess
			this = new MainMenuMobileButtons(onPlay, onEndless, onSettings, onCredits);
		else
		#end
			@:privateAccess
			this = new MainMenuDesktopButtons(onPlay, onEndless, onSettings, onCredits);
	}
	
	inline public function startIntro(delay = 0.0, ?callback:() -> Void):Void { thisAsButtons.startIntro(delay, callback); }
	inline public function startOutro(delay = 0.0, ?callback:() -> Void):Void { thisAsButtons.startOutro(delay, callback); }
}

#if FLX_TOUCH
class MainMenuMobileButtons extends MobileButtonGroup
{
	function new(onPlay:()->Void, onEndless:()->Void, onSettings:()->Void, onCredits:()->Void)
	{
		super(0);
		
		var buttonY = 84;
		addTextButton(40, buttonY, "STORY", onPlay);
		buttonY += 19;
		addTextButton(40, buttonY, "ENDLESS", onEndless);
		buttonY += 19;
		addTextButton(40, buttonY, "SETTINGS", onSettings);
		buttonY += 19;
		addTextButton(40, buttonY, "CREDITS", onCredits);
	}
	
	override function startIntro(delay:Float = 0.0, ?callback:() -> Void)
	{
		super.startIntro(delay,
			function ()
			{
				if (!Levels.seenFtue)
				{
					final button = members[1];
					button.setGraphic("locked_81", 1);
					button.labelOffsets[2].y = 0;
					button.onUp.callback = null;
				}
				
				if (callback != null)
					callback();
			}
		);
	}
}
#end

class MainMenuDesktopButtons extends ButtonGroup
{
	function new(onPlay:()->Void, onEndless:()->Void, onSettings:()->Void, onCredits:()->Void)
	{
		super(0);
		
		var button;
		var buttonY = 94;
		button = addNewButton(0, buttonY, (Levels.seenFtue ? "CONTINUE" : "BEGIN") + " STORY", onPlay);
		button.x = -button.width;
		buttonY += 10;
		button = addNewButton(0, buttonY, "ENDLESS RUSH", onEndless);
		button.x = -button.width;
		buttonY += 10;
		button = addNewButton(0, buttonY, "[F]ULLSCREEN", toggleFullscreenPadSelect);
		button.x = -button.width;
		buttonY += 10;
		button = addNewButton(0, buttonY, "SETTINGS", onSettings);
		button.x = -button.width;
		buttonY += 10;
		button = addNewButton(0, buttonY, "CREDITS", onCredits);
		button.x = -button.width;
		
		if (!Levels.seenFtue)
			disableButon(members[1]);
		
		active = false;
		
		if (Levels.seenFtue)
			Save.onDataClear.add(resetForDataClear);
	}
	
	function resetForDataClear():Void
	{
		members[0].text = "BEGIN STORY";
		
		if (!Levels.seenFtue)
			disableButon(members[1]);
	}
	
	function toggleFullScreen():Void
	{
		FlxG.fullscreen = !FlxG.fullscreen;
	}
	
	function onSelectEvent():Void
	{
		if (!active || !exists)
			return;
		
		//no prompt is needed if full screen is toggled via keyboard event
		if (!FlxG.fullscreen && callbacks[members[selected]] == toggleFullscreenPadSelect)
		{
			// Call toggle now rather than waiting flickering first (in onSelect)
			toggleFullScreen();
			Sounds.play(MENU_SELECT);
			
			active = false;
			FlxFlicker.flicker(members[selected], 0.5, 0.05, true, true,
				function(_) { active = true; }
			);
			
		}
		else
			onSelect();
	}
	
	function toggleFullscreenPadSelect():Void
	{
		if (FlxG.fullscreen) {
			
			toggleFullScreen();
			return;
		}
		
		// show mouse prompt, since gamepad events
		var prompt = new Prompt(true);
		var parent = FlxG.state;
		parent.add(prompt);
		active = false;
		prompt.setup(
			" GO FULLSCREEN?\n[MOUSE REQUIRED]",
			toggleFullScreen,
			null,
			function () {
				
				parent.remove(prompt);
				active = true;
			}
		);
	}
	
	#if (newgrounds && debug)
	public function addDebugNgButton(logInOutHandler:()->Void):Void
	{
		var button = addNewButton(0, members[members.length - 1].y + 10, "DEBUG LOG IN", logInOutHandler);
		button.x = -button.width;
	}
	
	public function setNgText(text:String):Void
	{
		var button = members[members.length - 1];
		button.text = text;
		button.centerXOnStage();
	}
	#end
	
	override function update(elapsed:Float)
	{
		super.update(elapsed);
		
		// handle gamepad events normally
		if (Inputs.padJustPressed.ACCEPT)
			onSelect();
	}
	
	override function destroy():Void
	{
		super.destroy();
		
		Inputs.acceptGesture.remove(onSelectEvent);
		Save.onDataClear.remove(resetForDataClear);
	}
}