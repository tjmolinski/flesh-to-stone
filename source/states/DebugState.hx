package states;

import flixel.FlxG;

import entities.GoalGroup;
import ui.BitmapText;

class DebugState extends flixel.FlxState
{
	var text:NokiaMonoText;
	
	override public function create()
	{
		super.create();
		
		add(text = new NokiaMonoText());
		
		displayGoalMath();
	}
	
	function displayGoalMath()
	{
		GoalGroup.parseData();
		@:privateAccess
		var levels = GoalGroup.levels;
		
		function str(num:Float):String
		{
			return Std.string(Std.int(num * 100) / 100);
		}
		
		var output = "";
		for (diff=>maps in levels)
		{
			var mapCount = maps.length;
			var goalCount = 0;
			for (map in maps)
				goalCount += map.total;
			
			var avgGoalPerGroup = goalCount / mapCount;
			var avgPtsPerGroup = 100 + 10 * avgGoalPerGroup;
			var avgGroupPer2000 = 2000 / avgPtsPerGroup;
			var avgGoalPer2000 = avgGroupPer2000 * avgGoalPerGroup;
			var avgGoalPerNewLevel = 8 * avgGoalPerGroup;
			var diffGoals = avgGoalPerNewLevel - avgGoalPer2000;
			var diffGroups = diffGoals / avgGoalPerGroup;
			
			output += diffToString(diff)
				+ '\n\tGoal [M]aps   : $mapCount'
				+ '\n\t[G]oals       : $goalCount'
				+ '\n\tAvg G|M       : ${str(avgGoalPerGroup)}'
				+ '\n\tAvg points|M  : ${str(avgPtsPerGroup)}'
				+ '\n\tAvg M|Win old : ${str(avgGroupPer2000)}'
				+ '\n\tAvg G|Win old : ${str(avgGoalPer2000)}'
				+ '\n\tAvg G|Win new : ${str(avgGoalPerNewLevel)}'
				+ '\n\tDifference    : ${str(diffGoals)} (~${str(diffGroups)}M)'
				;
			
			avgPtsPerGroup = 100 + 5 * avgGoalPerGroup;
			avgGroupPer2000 = 2000 / avgPtsPerGroup;
			avgGoalPer2000 = avgGroupPer2000 * avgGoalPerGroup;
			avgGoalPerNewLevel = 12 * avgGoalPerGroup;
			diffGoals = avgGoalPerNewLevel - avgGoalPer2000;
			diffGroups = diffGoals / avgGoalPerGroup;
			output += '\n\t==New game Plus=='
				+ '\n\tAvg Points|M  : ${str(avgPtsPerGroup)}'
				+ '\n\tAvg M|Win old : ${str(avgGroupPer2000)}'
				+ '\n\tAvg G|WIN old : ${str(avgGoalPer2000)}'
				+ '\n\tAvg G|WIN new : ${str(avgGoalPerNewLevel)}'
				+ '\n\tDifference    : ${str(diffGoals)} (~${str(diffGroups)}M)'
				+ '\n'
				;
		}
		
		text.text = output;
	}
	
	function diffToString(diff:GoalDifficulty):String
	{
		return switch diff
		{
			case EASY  : "Easy";
			case MEDIUM: "Medium";
			case HARD  : "Hard";
			case NONE  : "None";
		}
	}
	
	override function update(elapsed:Float)
	{
		super.update(elapsed);
		
		if (FlxG.keys.justPressed.C)
			trace(text.text);
	}
} 