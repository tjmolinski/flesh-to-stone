package data;

import flixel.FlxSprite;
import flixel.FlxG;
import flixel.group.FlxGroup;
import flixel.util.FlxSignal;

import utils.Spawner;

enum LevelEntity {
	HOMER;
	BEAM;
	FLOATER;
	SEEKER;
	LATCHER;
	LUNGER;
	SHOOTER;
	SKYLASER;
}

typedef TypedSpawnerData = { > SpawnerData, type:LevelEntity }

class LevelConfig {
	
	var spawners:Array<TypedSpawnerData> = [];
	
	public function new() { }
	
	public function add(type:LevelEntity, data:SpawnerData):LevelConfig {
		
		// Hacky conversion, I'm dumb
		(data:Dynamic).type = type;
		spawners.push((cast data:TypedSpawnerData));
		return this;
	}
	
	public function createSpawners():LevelSpawner {
		
		spawners.sort(sort);
		return new LevelSpawner(spawners);
	}
	
	function sort(a:TypedSpawnerData, b:TypedSpawnerData):Int {
		
		if (a.delay < b.delay) return -1;
		if (a.delay > b.delay) return 1;
		return 0;
	}
}

class LevelSpawner {
	
	var spawners:Map<LevelEntity, Spawner<FlxSprite>> = new Map();
	var map:Map<LevelEntity, Array<SpawnerData>>;
	
	public function new(list:Array<TypedSpawnerData>) {
		
		createMap(list);
	}
	
	function createMap(list:Array<TypedSpawnerData>) {
		
		map = new Map();
		for(spawner in list) {
			
			if (!map.exists(spawner.type))
				map[spawner.type] = [];
			else if (spawner.delay < map[spawner.type][0].end)
				FlxG.log.warn('Overlapping duration - type:${spawner.type} ${spawner.delay} < ${map[spawner.type][0].end}');
			
			// Store in reverse order for easy access of last added
			map[spawner.type].unshift(spawner);
		}
	}
	
	@:generic
	public function get<T:FlxSprite>(entityType:LevelEntity):Spawner<T> {
		
		var type:Null<Class<T>> = cast switch(entityType) {
			case HOMER   : cast entities.Homer;
			case BEAM    : cast entities.Beam;
			case FLOATER : cast entities.Floater;
			case SEEKER  : cast entities.Seeker;
			case LATCHER : cast entities.Latcher;
			case LUNGER  : cast entities.Lunger;
			case SHOOTER : cast null;
			case SKYLASER: cast entities.SkyLaser;
		}
		
		if (!map.exists(entityType))
			// No spawners for this type
			map[entityType] = [{ max:0 }];
		
		var spawner = new Spawner<T>(type, map[entityType].pop());
		spawners[entityType] = cast spawner;
		return spawner;
	}
	
	public function reset<T:FlxSprite>(config:LevelConfig) {
		
		@:privateAccess
		createMap(config.spawners);
		function resetorCreateSpawner(type:LevelEntity)
		{
			if (spawners.exists(type))
			{
				if (!map.exists(type))
					// No spawners for this type
					map[type] = [{ max:0 }];
				
				spawners[type].reset(map[type].pop());
			}
			else
				throw 'invalid type:$type';
		}
		
		resetorCreateSpawner(HOMER   );
		resetorCreateSpawner(BEAM    );
		resetorCreateSpawner(FLOATER );
		resetorCreateSpawner(SEEKER  );
		resetorCreateSpawner(LATCHER );
		resetorCreateSpawner(LUNGER  );
		resetorCreateSpawner(SHOOTER );
		resetorCreateSpawner(SKYLASER);
	}
}