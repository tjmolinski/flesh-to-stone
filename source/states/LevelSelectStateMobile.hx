package states;

import data.Save;
import data.Levels;
import states.LevelSelectState.LevelSelectStateBase;
import ui.Button;
import ui.Button.FullscreenButton;
import ui.LevelIcon;
import ui.Prompt;
import utils.Sounds;
import utils.Transitions;

import flixel.FlxG;
import flixel.effects.FlxFlicker;
import flixel.group.FlxSpriteGroup;
import flixel.group.FlxGroup;
import flixel.ui.FlxButton;
import flixel.util.FlxSignal;
import flixel.util.FlxTimer;

class LevelSelectStateMobile extends LevelSelectStateBase
{
	var buttonsGroup(get, never):LevelSelectMobile;
	function get_buttonsGroup() return cast buttons;
	
	public function new (isFromPlay:Bool) { super(isFromPlay); }
	
	override function create() 
	{
		super.create();
		
		startIntro();
	}
	
	override function createLevelIcons()
	{
		super.createLevelIcons();
		
		add(buttons = new LevelSelectMobile(transtitionToLevel));
		
		buttonsGroup.onNewGamePlusEnabled.add(onNewGamePlusEnabled.dispatch);
	}
	
	override function startButtonIntro()
	{
		super.startButtonIntro();
		
		buttonsGroup.startIntro();
		
		var button:Button;
		add(button = new Button(4, 4, Icon("back"), transitionToMainMenu));
		button.startIntro(1.0);
		add(button = new SoundButton(4, 160 - 21 - 4));
		button.startIntro(1.0);
		add(button = new FullscreenButton(160 - 19 - 4 - 8/*8 px error!WTF*/, 160 - 21 - 4));
		button.startIntro(1.0);
	}
}

enum IconType
{
	Level(num:Int);
	NewGamePlus;
}

class LevelIconButton extends FlxSpriteGroup
{
	public var button(default, null):EmptyButton;
	public var icon  (default, null):LevelIconBase;
	
	public function new(type:IconType, onClick:(LevelIconBase)->Void)
	{
		super(0, 0, 2);
		
		icon = switch(type)
		{
			case Level(num): new LevelIcon(num);
			case NewGamePlus: new LevelIconNewGamePlus();
		}
		x = icon.x;
		y = icon.y;
		icon.x = 0;
		icon.y = 0;
		add(button = new EmptyButton(icon, onClick.bind(icon)));
		add(icon);
		members.reverse();
	}
	
	override function update(elapsed:Float)
	{
		super.update(elapsed);
	}
}

class EmptyButton extends flixel.ui.FlxButton.FlxTypedButton<flixel.FlxSprite>
{
	var target:LevelIconBase;
	public function new(target:LevelIconBase, onClick:()->Void):Void
	{
		this.target = target;
		super(target.x, target.y, onClick);
		allowSwiping = false;
		
		makeGraphic(target.boxWidth * 3, target.boxHeight, 0x00ff0000);
		loadGraphic(graphic, true, Std.int(graphic.width/3), graphic.height);
	}
	
	override function updateStatusAnimation() {
		super.updateStatusAnimation();
		
		status == FlxButton.PRESSED ? target.select() : target.deselect();
	}
}

class LevelSelectMobile extends FlxTypedGroup<LevelIconButton>
{
	var showingNGP = false;
	
	public var onNewGamePlusEnabled(default, null) = new FlxSignal();
	
	public function new(callback:(Int)->Void)
	{
		super(Levels.LEVELS);
		
		for (i in 0...Levels.LEVELS)
		{
			var button = new LevelIconButton
				( Level(i)
				, onSelect.bind(_, callback.bind(i))
				);
			add(button);
		}
	}
	
	function onSelect(icon:LevelIconBase, callback:()->Void):Void
	{
		Sounds.play(MENU_SELECT);
		active = false;
		
		FlxFlicker.flicker(icon, 0.5, 0.05, true, true,
			function(_)
			{
				active = true;
				callback();
			}
		);
	}
	
	public function startIntro(?callback:Void->Void):Void
	{
		active = false;
		callback = endIntro.bind(callback);
		
		var duration = 0.0;
		for (button in members)
		{
			var iconDuration = button.icon.showIntro();
			if (iconDuration > duration)
				duration = iconDuration;
		};
		
		new FlxTimer().start(duration, (_)->checkEndIntro(callback));
	}
	
	function checkEndIntro(callback:Void->Void):Void
	{
		inline function getLevelIcon(level):LevelIcon { return cast members[level].icon; }
		
		var unlock = Levels.getUnseenUnlockedLevel();
		if (unlock > -1)
		{
			getLevelIcon(unlock - 1).showNewProgress(
				getLevelIcon(unlock).showUnlock.bind(checkEndIntro.bind(callback))
			);
			
			Levels.updateSeenScores();
		}
		else if (Levels.getUnseenProgressLevel() > -1)
		{
			getLevelIcon(Levels.getUnseenProgressLevel())
				.showNewProgress(checkEndIntro.bind(callback));
			
			Levels.updateSeenScores();
		}
		else if (Levels.hasUnlockedPlus() && !showingNGP)
			showNewGamePlusButton(checkEndIntro.bind(callback));
		else
			callback();
	}
	
	function endIntro(callback:Null<Void->Void>):Void {
		
		active = true;
		
		if (callback != null)
			callback();
	}
	
	function showNewGamePlusButton(callback:Void->Void):Void
	{
		maxSize = Levels.LEVELS + 1;
		showingNGP = true;
		
		var button:LevelIconButton;
		button = new LevelIconButton
			( NewGamePlus
			, onSelect.bind(_, ()->{ onClickPlus(cast button.icon); })
			);
		add(button);
		
		var duration = button.icon.showIntro();
		new FlxTimer().start(duration, (_)->callback());
	}
	
	function onClickPlus(plusButton:LevelIconNewGamePlus):Void
	{
		active = false;
		plusButton.deselect();
		var prompt = new Prompt();
		var parent = FlxG.state;
		prompt.setup
		(
			"START NEW GAME PLUS?",
			function onYes()
			{
				showingNGP = false;
				plusButton.hide();
				startNewGamePlusTransition();
			},
			plusButton.select,
			function removePrompt()
			{
				parent.remove(prompt);
				active = true;
			}
		);
		parent.add(prompt);
	}
	
	function startNewGamePlusTransition():Void
	{
		onNewGamePlusEnabled.dispatch();
		Save.startNewGamePlus();
		active = false;
		
		final totalStagger = 1.0;
		var duration = 0.0;
		for (i in 0...members.length)
		{
			var iconDuration = members[Levels.LEVELS - i].icon.resetProgress(i / Levels.LEVELS * totalStagger);
			if (iconDuration > duration)
				duration = iconDuration;
		}
		
		new FlxTimer().start(duration, (_)->{ active = true; });
	}
}