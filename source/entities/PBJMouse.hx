package entities;


import flixel.math.FlxVector;
#if FLX_TOUCH
import ui.TouchManager;
#end
import ui.Inputs;
import utils.Sounds;
import vfx.DustParticle;

import flixel.FlxG;
import flixel.FlxSprite;
import flixel.animation.FlxAnimationController;
import flixel.effects.FlxFlicker;
import flixel.math.FlxPoint;
import flixel.tweens.FlxTween;
import flixel.tweens.FlxEase;
import flixel.util.FlxSignal;
import flixel.util.FlxColor;
import flixel.util.FlxTimer;
import flixel.group.FlxGroup;

/**
 * ...
 * @author TJ
 */

 enum PBJMouseStates {
	INTRO;
	REGULAR;
	STATUE;
	DYING;
	OUTRO;
 }

class PBJMouse extends GroupEntity
{
	// 0 will disable stone hp
	inline static public var MAX_STONE_HEALTH = 0;
	inline static public var HEALTH_REGEN_TIME = 10;
	
	inline static var LATCHER_KILL_TIME = 0.25;
	inline static var MAX_LATCH = 8;
	inline static var DESKTOP_MAX_SPEED = 100;
	inline static var DESKTOP_ACCEL_TIME = 1/16;
	inline static var TOUCH_MAX_SPEED = 75;
	inline static var TOUCH_ACCEL_TIME = 1/8;
	inline static var MIN_SPEED = .0001;// must be > 0
	static var MAX_SPEED(get, never):Float;
	inline static function get_MAX_SPEED():Float
	{
		return FlxG.onMobile ? TOUCH_MAX_SPEED : DESKTOP_MAX_SPEED;
	}
	static var ACCEL_TIME(get, never):Float;
	inline static function get_ACCEL_TIME():Float
	{
		return FlxG.onMobile ? TOUCH_ACCEL_TIME : DESKTOP_ACCEL_TIME;
	}
	
	inline static public var DUST_TIME = 1/8;
	
	private var _currentState:PBJMouseStates;
	public var _life:Float = 5.0;
	public var _maxLife:Float = 5.0;
	private var _goalLifeIncrease:Float = 1.0;
	public var halfWidth:Float = 0.0;
	public var halfHeight:Float = 0.0;
	public var power:Float = 0.0;
	public var onMurdered(default, null) = new FlxSignal();
	
	@:allow(states.TutorialState)
	var stoneEnabled = true;
	
	var maxSpeed:Float = MAX_SPEED;
	
	#if debug
	public var godMode(default, set) = false;
	function set_godMode(value) {
		if (mainSprite != null)
		{
			if (value)
				mainSprite.setColorTransform(0,0,0);
			else
				mainSprite.setColorTransform();
		}
		return godMode = value;
	}
	#end
	
	private var mainSprite:MainSprite;
	private var mainAnim(get, never):FlxAnimationController;

	private var introTween:FlxTween;
	
	private var latchers:Array<Latcher>;
	private var latcherKillTimer:Float = 0.0;
	
	private var lowHealthBeepBuffer:Float = 0.0;
	private var lowHealthBeepTime:Float = 0.5;

	private var stoneLimitTimer:FlxTimer;
	private var stoneCooldownTime:Float = 1.0;
	private var stoneCooldownBlinkRate:Int = 15; // Lower is slower, Higher is faster
	private var isStoneCoolingDown:Bool = false;

	private var dustParticles:FlxTypedGroup<DustParticle>;
	private var dustTime = 0.0;
	
	override public function new(?X:Float=0, ?Y:Float=0, dust:FlxTypedGroup<DustParticle>) 
	{
		super(X, Y);

		dustParticles = dust;

		stoneLimitTimer = new FlxTimer();
		
		health = MAX_STONE_HEALTH;
		
		latchers = new Array();
		
		_currentState = INTRO;
		mainSprite = new MainSprite();
		
		halfWidth = width / 2;
		halfHeight = height / 2;

		maxSpeed = MAX_SPEED;
		drag.set(MAX_SPEED / ACCEL_TIME, MAX_SPEED / ACCEL_TIME);

		add(mainSprite);
		
		proxyMember = mainSprite;
	}
	
	public function showIntroAnim(onComplete:Void->Void):Void
	{
		var lastY = y;
		var falling = true;
		var shakes = [
			{ intensity:0.03, duration:0.5  },
			{ intensity:0.01, duration:0.25 },
			{ intensity:0.01, duration:0.25 }
		];
		FlxTween.tween(
			this,
			{ y: FlxG.height / 2},
			2,
			{
				ease: FlxEase.bounceOut,
				onComplete: function(tween) {
					mainAnim.play("down_idle");
					_currentState = REGULAR;
					onComplete();
				},
				onUpdate: function(tween) {
					
					if (y < lastY && falling && shakes.length > 0) {
						
						falling = false;
						FlxG.camera.shake(shakes[0].intensity, shakes[0].duration);
						shakes.shift();
						Sounds.play(HIT, 0);
						
					} else if (y > lastY)
						falling = true;
					
					lastY = y;
				}
			}
		);
	}
	
	inline public function showOutroRiseAndExit(onComplete:()->Void):Void
	{
		showOutroRise(showOutroExit.bind(onComplete));
	}
	public function showOutroRise(center = false, ?onComplete:()->Void):Void
	{
		_currentState = OUTRO;
		mainAnim.play("down_idle");
		
		FlxTween.tween
			( this
			, { y: center ? FlxG.height * 0.4 : y - 16 }
			, 1
			, { ease:FlxEase.backOut, onComplete: onComplete == null ? null : (_)->onComplete() }
			);
		
		if (center)
			FlxTween.tween(this, { x:(FlxG.width - width) / 2 }, 3, { ease:FlxEase.elasticInOut });
	}
	
	public function showOutroExit(?onComplete:()->Void):Void
	{
		var args:TweenOptions = { ease:FlxEase.backIn };
		if(onComplete != null)
			args.onComplete = (_)->onComplete();
		
		FlxTween.tween(this, { y: y-FlxG.height }, 0.5, args);
	}
	
	inline public function blink(blinkTime = 1.0, blinkRate = 0.04, ?onComplete:()->Void):FlxTimer
	{
		return new FlxTimer().start(blinkRate, 
			function (timer) {
				if(timer.loopsLeft % 2 == 0)
					mainSprite.setColorTransform();
				else
					mainSprite.setColorTransform(1,1,1,1,255,255,255,255);
				
				if (timer.loopsLeft == 0 && onComplete != null)
					onComplete();
			},
			Std.int(blinkTime / blinkRate)
		);
	}
	
	override public function update(elapsed:Float):Void
	{
		super.update(elapsed);
		
		switch(_currentState)
		{
			case INTRO:
			case OUTRO:
			case REGULAR:
				handleRegularState(elapsed);
			case STATUE:
				handleStatueState(elapsed);
			case DYING:
		}
	}

	public function latch(latcher: Latcher):Void
	{
		latcher.latched();
		if (isStatue())
		{
			latcher.murder();
		}
		else
		{
			latchers.push(latcher);
			updateSpeed();
		}
	}
	
	function startKillingLatchers():Void
	{
		killLatcher();
		latcherKillTimer = -LATCHER_KILL_TIME;
	}
	
	function updateKillLatchers(elapsed:Float):Void
	{
		latcherKillTimer += elapsed;
		if (latcherKillTimer > 0)
		{
			latcherKillTimer -= LATCHER_KILL_TIME;
			killLatcher();
		}
	}
	
	function killLatcher():Void
	{
		if(latchers.length > 0)
		{
			latchers.pop().murder();
			updateSpeed();
		}
	}
	
	function handleRegularState(elapsed:Float):Void
	{
		#if FLX_TOUCH
		if (FlxG.onMobile)
			switch (Inputs.actionTouchMode)
			{
				case SWIPE   : updateSwipeControls(elapsed);
				case JOYSTICK: updateAnalogControls(elapsed, Inputs.analogDir);
				case RELATIVE: updateAnalogControls(elapsed, Inputs.analogDir);
				case MENU    : updateDigitalControls(elapsed);
				case NONE    :
			}
		else
			updateDigitalControls(elapsed);
		#else
		updateDigitalControls(elapsed);
		#end
		
		updateMainAnimation(elapsed);
		
		if (health < MAX_STONE_HEALTH && (velocity.x != 0 || velocity.y != 0))
		{
			health += elapsed / HEALTH_REGEN_TIME;
			
			if (health > MAX_STONE_HEALTH)
				health = MAX_STONE_HEALTH;
		}
	}
	
	#if FLX_TOUCH
	inline static var SWIPE_ACCEL_TIME = 0.35;
	inline static var SWIPE_MAX_TIME = 0.25;
	inline static var MIN_SWIPE_DISTANCE = 16;
	inline static var MIN_TOUCH_HOLD = 0.125;
	
	var swipeTimer = 0.0;
	
	function updateSwipeControls(elapsed:Float):Void
	{
		if (FlxG.swipes.length > 0)
		{
			for (swipe in FlxG.swipes)
			{
				if (swipe.duration < SWIPE_MAX_TIME)
				{
					if (swipe.distance > MIN_SWIPE_DISTANCE)
					{
						(acceleration:FlxVector).set(MAX_SPEED / ACCEL_TIME, 0).degrees = swipe.angle - 90;
						swipeTimer = 0;
					}
					else
					{
						acceleration.set(0, 0);
					}
				}
			}
		}
		
		if (FlxG.touches.list.length > 1 && !isStoneCoolingDown)
		{
			var numHold = 0;
			for(touch in FlxG.touches.list)
			{
				if (FlxG.game.ticks - touch.justPressedTimeInTicks >= MIN_TOUCH_HOLD * 1000)
					numHold++;
			}
			
			if (numHold >= 2)
				startStatue();
		}
		else if (FlxG.touches.list.length == 1)
		{
			var touch = FlxG.touches.getFirst();
			var distance:FlxVector = touch.getPosition().subtractPoint(touch.justPressedPosition);
			var duration = FlxG.game.ticks - touch.justPressedTimeInTicks;
			if (duration >= MIN_TOUCH_HOLD && distance.lengthSquared < MIN_SWIPE_DISTANCE * MIN_SWIPE_DISTANCE)
				acceleration.set(0, 0);
		}
		
		swipeTimer += elapsed;
		if (swipeTimer >= SWIPE_ACCEL_TIME)
			acceleration.set(0, 0);
	}
	#end
	
	function updateAnalogControls(elapsed:Float, dirUnit:FlxPoint):Void
	{
		acceleration.copyFrom(dirUnit)
			.scale(MAX_SPEED / ACCEL_TIME);
		
		checkStatueInput(elapsed);
	}
	
	function updateSpeed():Void
	{
		var num = latchers.length;
		if (num > MAX_LATCH)
			num = MAX_LATCH;
		
		maxSpeed = MAX_SPEED - (MAX_SPEED - MIN_SPEED) * (num / MAX_LATCH);
	}
	
	function updateDigitalControls(elapsed:Float):Void
	{
		var pad = FlxG.gamepads.getFirstActiveGamepad();
		if (pad != null)
		{
			var dir = FlxVector.get(
				pad.analog.value.LEFT_STICK_X,
				pad.analog.value.LEFT_STICK_Y
			);
			
			if (!dir.isZero())
			{
				updateAnalogControls(elapsed, dir);
				return;
			}
		}
		
		var up    = Inputs.pressed.UP;
		var down  = Inputs.pressed.DOWN;
		var left  = Inputs.pressed.LEFT;
		var right = Inputs.pressed.RIGHT;
		
		acceleration.set(0, 0);
		if(up != down || left != right)
		{
			acceleration.x = (right ? 1 : 0) - (left ? 1 : 0);
			acceleration.y = (down ? 1 : 0) - (up ? 1 : 0);
			acceleration.scale(MAX_SPEED / ACCEL_TIME);
		}
		
		checkStatueInput(elapsed);
	}
	
	function checkStatueInput(elapsed:Float):Void
	{
		if (Inputs.justPressed.STONE && !isStoneCoolingDown && stoneEnabled)
			startStatue();
	}
	
	inline function startStatue():Void {
		
		_currentState = STATUE;
		acceleration.set(0, 0);
		randomStoneSound();
		startKillingLatchers();
		stoneLimitTimer.start(2, kickOutOfStone);
	}
	
	function updateMainAnimation(elapsed:Float):Void
	{
		if (_currentState == STATUE)
		{
			mainAnim.play(
				switch(mainAnim.name)
				{
					case "up_run"  , "up_idle"  : "up_statue";
					case "down_run", "down_idle": "down_statue";
					case "side_run", "side_idle": "side_statue";
					default: null;
				}
			);
			return;
		}
		
		if (acceleration.x != 0 || acceleration.y != 0)
		{
			var nextAnim = mainAnim.name;
			
			flipX = acceleration.x > 0;
			if (acceleration.x * acceleration.x > acceleration.y * acceleration.y)
				nextAnim = "side_run";
			else if (acceleration.y > 0) nextAnim = "down_run";
			else if (acceleration.y < 0) nextAnim = "up_run";
			
			if (mainAnim.name != nextAnim)
				mainAnim.play(nextAnim);
			
			dustTime += elapsed;
			if (dustTime > DUST_TIME){
				
				dustTime -= DUST_TIME;
				dustParticles.recycle(DustParticle).spawn(x + (flipX ? -1 : 1) * frameWidth/2, y + frameHeight, flipX);
			}
		}
		else
		{
			var nextAnim =
				switch(mainAnim.name)
				{
					case "down_run": "down_idle";
					case "up_run"  : "up_idle";
					case "side_run": "side_idle";
					default: mainAnim.name;
				};
			
			
			if (mainAnim.name != nextAnim)
				mainAnim.play(nextAnim);
		}
	}

	private function endStoneCooldown(timer:FlxTimer):Void
	{
		isStoneCoolingDown = false;
	}

	private function kickOutOfStone(timer:FlxTimer):Void
	{
		new FlxTimer().start(stoneCooldownTime, endStoneCooldown);
		isStoneCoolingDown = true;
		
		Sounds.play(POWER);
		blink(stoneCooldownTime, stoneCooldownTime / stoneCooldownBlinkRate);

		FlxFlicker.stopFlickering(mainSprite);
		color = FlxColor.WHITE;
		randomStoneSound();
		_currentState = REGULAR;
			
		if (mainAnim.name == "up_statue")
		{
			mainAnim.play("up_idle");
		}
		else if (mainAnim.name == "down_statue")
		{
			mainAnim.play("down_idle");
		}
		else if (mainAnim.name == "side_statue")
		{
			mainAnim.play("side_idle");
		}
	}
	
	private function handleStatueState(elapsed:Float):Void
	{
		if (_life <= 0)
		{
			murdered();
			return;
		}
		
		updateKillLatchers(elapsed);
		
		if (stoneLimitTimer.progress > 0.5 && !FlxFlicker.isFlickering(mainSprite))
		{
			FlxFlicker.flicker(mainSprite, stoneLimitTimer.timeLeft, 0.05);
		}
		
		if (!Inputs.pressed.STONE)
		{
			stoneLimitTimer.cancel();
			kickOutOfStone(stoneLimitTimer);
		}
	}
	
	inline function randomStoneSound():Void
	{
		Sounds.play(STATUE);
	}
	
	function showDeathAnim(onComplete:Void->Void):Void
	{
		mainSprite.showDeathAnim(onComplete);
	}
	
	public function isStatue():Bool
	{
		return _currentState == STATUE;
	}
	
	public function collectedGoal():Void
	{
		_life = _life + _goalLifeIncrease > _maxLife ? _maxLife : _life + _goalLifeIncrease;
		Sounds.play(GOAL);
	}
	
	public function hit():Void
	{
		#if debug
		if(godMode) return;
		#end
		
		if (isStatue())
		{
			if (MAX_STONE_HEALTH > 0)
			{
				health--;
				if (health == 0)
					murdered();
				else
					FlxFlicker.flicker(mainSprite, 1, 0.05);
			}
		}
		else
			murdered();
	}
	
	public function murdered():Void
	{
		
		#if debug
		if(godMode) return;
		#end
		
		if (_currentState == DYING)
			return;
		
		_currentState = DYING;
		solid = false;
		showDeathAnim(onMurdered.dispatch);
		Sounds.play(DEATH);
	}
	
	public function isDead():Bool
	{
		return _currentState == DYING;
	}
	
	inline function get_mainAnim():FlxAnimationController
	{
		return mainSprite.animation;
	}
	
	override function updateMotion(elapsed:Float) {
		//super.updateMotion(elapsed);
		
		if (maxSpeed > 0 && (velocity:FlxVector).lengthSquared > maxSpeed * maxSpeed)
			(velocity:FlxVector).length = maxSpeed;
			
		BaseSprite.updateMotionSkidDrag(this, elapsed);
	}
}

class MainSprite extends FlxSprite {
	
	var deathTween:FlxTween;
	
	public function new () {
		super();
		
		
		vfx.SpriteUtil.loadAnimationWithSpaces(
			this,
			"assets/images/hero.png",
			FlxPoint.get(8, 10),
			FlxPoint.get(0, 1)
		);
		animation.add("up_run", [0, 1, 2, 3], 5, true);
		animation.add("up_idle", [4, 5], 2, true);
		animation.add("up_statue", [6], 0, false);
		animation.add("side_run", [ 7, 8, 9, 10], 5, true);
		animation.add("side_idle", [11, 12], 2, true);
		animation.add("side_statue", [13], 0, false);
		animation.add("down_run", [14, 15, 16, 17], 5, true);
		animation.add("down_idle", [18, 19], 2, true);
		animation.add("down_statue", [20], 0, false);
		width = 6;
		height = 7;
		offset.set(1, 2);
		
		animation.frameIndex = 19;
	}
	
	public function showDeathAnim(onComplete:Void->Void):Void
	{
		FlxFlicker.flicker(this, 1.0, 0.05, false);
		FlxTween.tween(this, { x: getRandomNear(x, 50), y: getRandomNear(y, 50) }, 1.0);
		FlxTween.tween(scale, { x: 1.3, y: 1.3 }, 1.0, { ease:FlxEase.cubeIn } );
		FlxTween.tween(FlxG, { timeScale: 0.01 }, 0.5)
			.onComplete = function (_)
			{
				FlxG.timeScale = 1.0;
				if (onComplete != null)
					onComplete();
			};
		
	}
	
	inline function getRandomNear(base:Float, adjust:Float):Float
	{
		return base + FlxG.random.float(0, adjust * 2) - adjust;
	}
}