package vfx;

class PixelPerfectSmoke extends flixel.system.FlxAssets.FlxShader
{
	@:glFragmentSource('
		#pragma header
		
		const float scale = 4.0;
		
		float checker(vec2 coord)
		{
			return mod(floor(coord.x / scale) + floor(coord.y / scale), 2.0);
		}
		
		vec4 pixelColor(vec2 coord)
		{
			return texture2D(bitmap, coord / openfl_TextureSize);
		}
		
		float pixelAlpha(vec2 coord)
		{
			return pixelColor(coord).w;
		}
		
		vec4 bigPixelCenterColor(vec2 coord)
		{
			return pixelColor(coord + vec2(scale / 2.0, scale / 2.0));
		}
		
		float bigPixelBlendAlpha(vec2 coord)
		{
			return floor
				(   ( pixelAlpha(coord)
					+ pixelAlpha(coord + vec2(scale, 0    ))
					+ pixelAlpha(coord + vec2(0    , scale))
					+ pixelAlpha(coord + vec2(scale, scale))
					) / 4.0 + 0.5
				);
		}
		
		vec2 bigPixelTopLeft(vec2 coord)
		{
			return vec2(coord.x - mod(coord.x, scale), coord.y - mod(coord.y, scale));
		}
		
		float neighboringBigPixelsCenterAlpha(vec2 coord)
		{
			return floor
				(	( bigPixelCenterColor(coord + vec2(0,  scale)).w
					+ bigPixelCenterColor(coord + vec2(0, -scale)).w
					+ bigPixelCenterColor(coord + vec2( scale, 0)).w
					+ bigPixelCenterColor(coord + vec2(-scale, 0)).w
					) / 4.0
				);
		}
		
		void main()
		{
			vec2 topLeft = bigPixelTopLeft(openfl_TextureCoordv * openfl_TextureSize);
			gl_FragColor = bigPixelCenterColor(topLeft);
			gl_FragColor.w = floor(gl_FragColor.w) * mix(checker(topLeft), 1.0, neighboringBigPixelsCenterAlpha(topLeft));
		}
	')
	
	public function new() { super(); }
}