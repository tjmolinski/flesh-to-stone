package entities;

import data.Levels;
import flixel.util.FlxSignal;
import flixel.util.FlxTimer;
import flixel.FlxG;
import flixel.math.FlxPoint;

class GoalGroup extends flixel.group.FlxGroup.FlxTypedGroup<Goal> {
	
	static public var diffList = [EASY, MEDIUM, HARD];
	static var levels:Map<GoalDifficulty, Array<GoalMap>>;
	static var levelsByName:Map<String, GoalMap>;

	private var progression:GoalProgression;
	
	public var autoSpawn:Bool = true;
	public var onClear(default, null) = new FlxSignal();
	public var onSpawn(default, null) = new FlxSignal();
	public var total(default, null) = 0;
	
	var groupsCollected:Int = 0;
	#if debug
	var totalSpawned = 0;
	public var logSpawned = false;
	#end
	
	public function new(progression:GoalProgression) {
		super();

		this.progression = progression;
		
		parseData();
		spawn();
	}
	
	public function spawn(?name:String):Void {
		
		total = 0;
		
		var map;
		if (name != null)
			map = levelsByName[name];
		else
		{
			var difficulty = getDifficulty();
			if (difficulty == NONE)
				return;
			
			map = levels[difficulty][FlxG.random.int(0, levels[difficulty].length - 1)];
			
			#if debug
			if (logSpawned)
			{
				totalSpawned += map.total;
				
				trace
					( '${groupsCollected + 1}/${Levels.scoreGoal}'
					,   switch difficulty
						{
							case NONE  : "none  ";
							case EASY  : "easy  ";
							case MEDIUM: "medium";
							case HARD  : "hard  ";
						}
					, '+${map.total} = $totalSpawned'
					);
			}
			#end
			
		}
		
		total = map.total;
		map.create(this);
		
		onSpawn.dispatch();
	}
	
	function getDifficulty():GoalDifficulty
	{
		var percentComplete = groupsCollected / Levels.scoreGoal;
		return switch progression
		{
			case STEADY(diff)   : diff;
			case CYCLE(period)  : diffList[Std.int(groupsCollected / period) % 3];
			case TUTORIAL       : NONE;
			case RETRO_FIT(NONE): NONE;
			case RETRO_FIT(EASY):
				if      (percentComplete < 6/12) EASY;
				else if (percentComplete <11/12) MEDIUM;
				else                             HARD;
			case RETRO_FIT(MEDIUM):
				if      (percentComplete < 1/12) EASY;
				else if (percentComplete < 9/12) MEDIUM;
				else                             HARD;
			case RETRO_FIT(HARD):
				if      (percentComplete < 1/12) EASY;
				else if (percentComplete < 3/12) MEDIUM;
				else                             HARD;
		}
	}
	
	public function endTutorial() {
		
		if (progression != TUTORIAL)
			throw "Attempted to end a non-existent tutorial";
		
		progression = STEADY(MEDIUM);
	}
	
	public static function parseData():Void {
		
		levels = new Map();
		levelsByName = new Map();
		
		var data = null;
		for(group in  Xml.parse(openfl.Assets.getText("assets/data/level.tmx"))
			.elementsNamed("map").next()
			.elementsNamed("group"))
		{
			if (group.get("name") == "Goals")
				data = group;
		}
		
		if (data == null)
			throw "Missing Goals group in level.tmx";
		
		for(group in data.elementsNamed("group")) {
			switch(group.get("name"))
			{
				case "Easy":
					levels[EASY] = [];
					for(layer in group.elementsNamed("layer"))
					{
						var group = GoalMap.fromXml(layer);
						levels[EASY].push(group);
						levelsByName[group.name] = group;
					}
				case "Medium":
					levels[MEDIUM] = [];
					for(layer in group.elementsNamed("layer"))
					{
						var group = GoalMap.fromXml(layer);
						levels[MEDIUM].push(group);
						levelsByName[group.name] = group;
					}
				case "Hard":
					levels[HARD] = [];
					for(layer in group.elementsNamed("layer"))
					{
						var group = GoalMap.fromXml(layer);
						levels[HARD].push(group);
						levelsByName[group.name] = group;
					}
			}
		}
	}
	
	override function update(elapsed:Float) {
		super.update(elapsed);
		
		if (total > 0 && countLiving() == 0) {
			
			onClear.dispatch();
			total = 0;
			groupsCollected++;
			if (autoSpawn)
				spawn();
		}
	}
	
	override function revive() {
		// super.revive();
		
		alive = true;
		exists = true;
	}
	
	public function countUncollected():Int
	{
		var i:Int = length;
		var count:Int = -1;

		while (i-- > 0)
		{
			if (members[i] != null)
			{
				if (count < 0)
					count = 0;
				if (members[i].solid)
					count++;
			}
		}

		return count;
	}
}

class GoalMap {
	
	static inline var TILE_SIZE = 8;
	static inline var WIDTH  = 14;
	static inline var HEIGHT = 13;
	static inline var STAGGER = 1 / (WIDTH + HEIGHT);
	
	public var name(default, null):String;
	public var total(default, null):Int;
	var level:Array<Array<FlxPoint>>;
	var offsetX:Float;
	var offsetY:Float;
	
	public function new(name:String, levelData:String, offsetX = 0.0, offsetY = 0.0) {
		
		this.name = name;
		this.offsetX = offsetX;
		this.offsetY = offsetY + 8;
		
		var tileData:Array<Bool> = [];
		var msg = "\n";
		var rows = levelData.split("\n");
		rows.shift();
		rows.pop();
		var width = rows[0].split(",").length;
		var height = rows.length;
		
		for (rowData in rows) {

			for (tile in rowData.split(",")){
				
				tileData.push(tile != "0" && tile != "");
				msg += tile != "0" && tile != "" ? "X" : " ";
			}
			msg += "\n";
		}
		// trace(name, msg);
		
		// order by delay
		
		level = [for (i in 0 ... width + height) []];
		for (i in 0...tileData.length) {
			
			if (tileData[i]) {
				
				var pos = FlxPoint.get(i % width, Std.int(i / width));
				level[Std.int(pos.x + pos.y)].push(pos);
				total++;
			}
		}
		
		while(level[0].length == 0)
		{
			level.shift();
			offsetY += 8;
		}
		
		while(level[level.length - 1].length == 0)
			level.pop();
	}
	
	public function create(group:GoalGroup):Void {
		
		// trace('creating $name');
		
		var pos = FlxPoint.get();
		function spawnStaggered(?timer:FlxTimer){
			
			var i = timer != null ? timer.elapsedLoops : 0;
			for (p in level[i]) {
				
				pos.set(offsetX + p.x * TILE_SIZE, offsetY + p.y * TILE_SIZE);
				group.recycle(Goal).spawn(pos);
			}
		}
		spawnStaggered();
		new FlxTimer().start(STAGGER, spawnStaggered, level.length - 1);
	}
	
	static public function fromXml(data:Xml):GoalMap
	{
		inline function parseFloat(name:String):Float
		{
			return Std.parseFloat(data.get("offsetx"));
		}
		
		return new GoalMap
			( data.get("name")
			, data.firstElement().firstChild().toString()
			, parseFloat("offsetx")
			, parseFloat("offsety")
			);
	}
}

enum GoalProgression {
	/**
	 * Maintains the same difficulty throughout the entire level.
	 */
	STEADY(diff:GoalDifficulty);
	/**
	 * Switches to a harder Difficulty after the specified number of groups,
	 * loops back to easy after hard
	 */
	CYCLE(period:Int);
	/**
	 * Attempts to recreate the number of collectibles each level had in
	 * the previous scoring by startiung at easy and increasing
	 */
	RETRO_FIT(oldDiff:GoalDifficulty);
	/**
	 * STEADY(NONE)
	 */
	TUTORIAL;
}

enum GoalDifficulty {
	
	EASY;
	MEDIUM;
	HARD;
	NONE;
}