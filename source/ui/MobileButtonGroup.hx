package ui;

import flixel.tweens.FlxTween;
import flixel.tweens.FlxEase;
import ui.Button.ButtonType;
import ui.ButtonGroup.ITransitionable;
import flixel.FlxSprite;
import flixel.group.FlxGroup.FlxTypedGroup;

class MobileButtonGroup extends TypedMobileButtonGroup<Button>
{
	inline public function addButton(x:Float, y:Float, type:ButtonType, ?onClick:()->Void):Void
	{
		add(new Button(x, y, type, onClick));
	}
	
	inline public function addSimpleButton(x:Float, y:Float, bgKey:String, ?onClick:()->Void):Void
	{
		addButton(x, y, None(bgKey), onClick);
	}
	
	inline public function addTextButton(x:Float, y:Float, text:String, ?onClick:()->Void):Void
	{
		addButton(x, y, Text(text, Big), onClick);
	}
	
	override function startIntro(delay = 0.0, ?callback:()->Void):Void
	{
		visible = true;
		
		function onIntroComplete()
		{
			active = true;
			
			if (callback != null)
				callback();
		}
		
		for (i in  0...members.length)
			members[i].startIntro(delay + i * 0.125, i == members.length - 1 ? onIntroComplete : null);
	}
	
	override function startOutro(delay = 0.0, ?callback:()->Void):Void
	{
		active = false;
		
		for (i in 0...members.length)
			members[i].startOutro(delay + i * 0.125, (i == members.length - 1) ? callback : null);
	}
}

class TypedMobileButtonGroup<T:FlxSprite> extends FlxTypedGroup<T> implements ITransitionable
{
	public function new (maxSize:Int)
	{
		super(maxSize);
		active = false;
		visible = false;
	}
	
	public function startIntro(delay = 0.0, ?callback:()->Void):Void
	{
		visible = true;
		
		function onIntroComplete(_)
		{
			active = true;
			
			if (callback != null)
				callback();
		}
		
		for (i in  0...members.length)
		{
			var button = members[i];
			button.scale.set();
			
			var options:TweenOptions = { ease:FlxEase.backOut, startDelay: delay + i * 0.125 };
			if (i == members.length - 1)
				options.onComplete = onIntroComplete;
			
			FlxTween.tween(members[i], { 'scale.x':1, 'scale.y':1 }, 0.25, options);
		}
	}
	
	public function startOutro(delay = 0.0, ?callback:()->Void):Void
	{
		active = false;
		
		for (i in  0...members.length)
		{
			var options:TweenOptions = { ease:FlxEase.backIn, startDelay: delay + i * 0.125 };
			if (i == members.length - 1 && callback != null)
				options.onComplete = (_)->callback();
			
			FlxTween.tween(members[i], { 'scale.x':0, 'scale.y':0 }, 0.25, options);
		}
	}
}