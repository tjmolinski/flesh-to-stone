package ui;

import flixel.FlxBasic;
import flixel.FlxG;
import flixel.FlxSprite;

import utils.Sounds;

using flixel.util.FlxSpriteUtil;

class Prompt extends flixel.group.FlxGroup {
	
	inline static var BUFFER = 6;
	
	var box:FlxSprite;
	var label:BitmapText;
	var yesMouse:Button;
	var noMouse:Button;
	var keyButtons:ButtonGroup;
	var yesKeys:BitmapText;
	var noKeys:BitmapText;
	
	var forceMouse:Bool;
	
	public function new (forceMouse = false, singleButton = false) {
		super();
		
		this.forceMouse = forceMouse || FlxG.onMobile;
		var oldQuality = FlxG.stage.quality;
		FlxG.stage.quality = LOW;
		add(box = new FlxSprite());
		box.makeGraphic(FlxG.width - BUFFER * 2, 48, 0, true, "prompt-bg");
		box.drawRoundRect(
			0,
			0,
			box.graphic.width,
			box.graphic.height,
			8,
			8,
			0xFF52294b,
			{ color:0xFF928fb8, thickness:1 },
			{ smoothing: false }
		);
		box.x = (FlxG.width  - box.width ) / 2;
		box.y = (FlxG.height - box.height) / 2;
		FlxG.stage.quality = oldQuality;
		
		add(label = new BitmapText(0, 0, ""));
		
		if (this.forceMouse) {
			
			if (singleButton)
				add(yesMouse = new Button((FlxG.width - 28) / 2, 0, Text("OK", Small)));
			else {
				add(yesMouse = new Button(FlxG.width / 2 - 28 - 4, 0, Text("YES", Small)));
				add(noMouse = new Button(FlxG.width / 2 + 4, 0, Text("NO", Small)));
			}
			
		} else {
			
			keyButtons = new ButtonGroup(0, false);
			keyButtons.keysNext = RIGHT;
			keyButtons.keysPrev = LEFT;
			if (singleButton) {
				keyButtons.addButton(yesKeys = new BitmapText(0, 0, "OK"), null);
				yesKeys.centerXOnStage();
			} else {
				keyButtons.addButton(yesKeys = new BitmapText(FlxG.width / 2 - 28 - 4, 0, "YES"), null);
				keyButtons.addButton(noKeys  = new BitmapText(FlxG.width / 2 + 4, 0, "NO"), null);
			}
			add(keyButtons);
		}
	}
	
	public function setup(text:String, onYes:Void->Void, ?onNo:Void->Void, ?onChoose:Void->Void):Void {
		
		label.text = text;
		label.x = (FlxG.width - label.width) / 2 + 1;
		label.y = box.y + 8;
		
		if (forceMouse) {
			
			if(!FlxG.onMobile)
				FlxG.mouse.visible = true;
			
			yesMouse.y = label.y + label.lineHeight * 2 + 2;
			yesMouse.onUp.callback = onLoginDecide.bind(onYes, onChoose);
			
			if (noMouse != null) {
				noMouse.y = label.y + label.lineHeight * 2 + 2;
				noMouse.onUp.callback = onLoginDecide.bind(onNo , onChoose);
			}
			
		} else {
			
			yesKeys.y = label.y + label.lineHeight * 3 + 2;
			keyButtons.setCallback(yesKeys, onLoginDecide.bind(onYes, onChoose));
			
			if (noKeys != null) {
				noKeys .y = label.y + label.lineHeight * 3 + 2;
				keyButtons.setCallback(noKeys , onLoginDecide.bind(onNo , onChoose));
			}
		}
	}
	
	function onLoginDecide(callback:Void->Void, onChoose:Void->Void) {
		
		if (forceMouse) {
			
			yesMouse.onUp.callback = null;
			if (noMouse != null)
				noMouse .onUp.callback = null;
			
			if(!FlxG.onMobile)
				FlxG.mouse.visible = false;
			
		} else {
			
			keyButtons.setCallback(yesKeys, null);
			if (noKeys != null)
				keyButtons.setCallback(noKeys , null);
		}
		
		Sounds.play(MENU_SELECT);
		
		if (callback != null)
			callback();
		
		if (onChoose != null)
			onChoose();
	}
	
	/**
	 * Shows a single-button prompt and enables/disables the specified button group
	 * @param text    the dialog messsage.
	 * @param buttons the active ui group being interrupted.
	 */
	inline static public function showOKInterrupt(text:String, buttons:FlxBasic):Void {
		
		var prompt = new Prompt(false, true);
		var parent = FlxG.state;
		parent.add(prompt);
		buttons.active = false;
		prompt.setup(text, null, null,
			function () {
				
				parent.remove(prompt);
				buttons.active = true;
			}
		);
	}
}